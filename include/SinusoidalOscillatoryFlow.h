/*
 * File: SinusoidalOscillatoryFlow.h
 * Desc: derived class from Flows, implementation of sinusoidal oscillatory flow
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 26 May, 2023
 * Last modified: 16 August, 2023
 */

#ifndef SINUSOIDAL_OSCILLATORY_FLOW_
#define SINUSOIDAL_OSCILLATORY_FLOW_

#include "Flows.h"

class SinusoidalOscillatoryFlow: public Flows
{
    public:
        SinusoidalOscillatoryFlow() = default;
        SinusoidalOscillatoryFlow(const PetscScalar period,
            const PetscScalar amplitude): omega(2.0 * PETSC_PI / period),
            A(amplitude) {};
        ~SinusoidalOscillatoryFlow() = default;
        // horizontal velocity
        PetscScalar u (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return A * omega * PetscSinScalar(omega * t); };
        // vertical velocity
        PetscScalar w (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // horizontal acceleration
        PetscScalar ax(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return A * omega * omega * PetscCosScalar(omega * t); };
        // vertical acceleration
        PetscScalar az(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of horizontal velocity with respect to x
        PetscScalar dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of horizontal velocity with respect to z
        PetscScalar dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of vertical velocity with respect to x
        PetscScalar dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of vertical velocity with respect to z
        PetscScalar dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
    private:
        const PetscScalar omega;        // frequency
        const PetscScalar A;            // amplitude
};

#endif