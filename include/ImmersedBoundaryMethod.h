/*
 * File: ImmersedBoundaryMethod.h
 * Desc: An immersed boundary method to couple hydrodynamics
 *       and the motion of flexible vegetation
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 4 September, 2022
 * Last modified: 4 September, 2023
 */

#ifndef IMMERSED_BOUNDARY_METHOD_
#define IMMERSED_BOUNDARY_METHOD_

#include <petsc.h>
#include "Truss.h"
#include "RigidBar.h"

class ImmersedBoundaryMethod
{
    public:
        ImmersedBoundaryMethod () = default;
        ImmersedBoundaryMethod(const DM &dm,
            const PetscScalar Dx, const PetscScalar Dz,
            const PetscScalar xMin, const PetscScalar xMax,
            const PetscScalar zMin, const PetscScalar zMax,
            const PetscInt N = 2);
        ~ImmersedBoundaryMethod() = default;
        PetscErrorCode load(Vec &dU, DM &ts,
            const Vec &Uf, const Vec &Af,
            const PetscScalar factor = 1.0) const;
        PetscErrorCode load(Vec &dU, RigidBar **bars,
            const PetscScalar nbars,
            const Vec &Uf, const Vec &Af,
            const PetscScalar factor = 1.0) const;
        PetscErrorCode whichCells(PetscInt &imin, PetscInt &imax,
            PetscInt &jmin, PetscInt &jmax,
            const PetscScalar x, const PetscScalar z,
            const PetscScalar dx, const PetscScalar dz) const;
        PetscErrorCode destroy();
    private:
        PetscScalar delta(const Vector3s &x,
            const PetscScalar dx,
            const PetscScalar dy,
            const PetscScalar dz) const;
        PetscScalar delta(const PetscScalar x) const;
        const DM *ptrDM;
        PetscScalar dx;
        PetscScalar dz;
        PetscScalar xmin, xmax;
        PetscScalar zmin, zmax;
        PetscScalar span;
        PetscInt Nx, Nz;
        Mat ele2lr;
        Mat ele2ud;
};

#endif