/*
 * File: TrussSystem.h
 * Desc: implementation of explicit truss spring model
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 24 October, 2022
 * Last Modified: 16 August, 2023
 */

#ifndef TRUSS_SYSTEM_
#define TRUSS_SYSTEM_

#include <petsc.h>
#include "Truss.h"

class TrussSystem
{
    public:
        TrussSystem (DM &dm) : ptrDM(&dm) {};
        ~TrussSystem() = default;
        PetscErrorCode setupFromOptions(PetscOptions &options);
        PetscErrorCode setupInFluidFromOptions(PetscOptions &options);
        PetscErrorCode initialize();
        PetscErrorCode update(const PetscScalar dt);
        PetscErrorCode update_RK2nd(const PetscScalar dt, const PetscInt step);
        PetscErrorCode update_RK2nd_1(const PetscScalar dt);
        PetscErrorCode update_RK2nd_2(const PetscScalar dt);
        PetscErrorCode destroy();
        PetscScalar    maxstrain() const;
        PetscInt       truss_num () const { return Nedge;   };
        PetscInt       node_num  () const { return Nnode;   };
        PetscInt       subnet_num() const { return Nsubnet; };
        Branch         *subnets() const { return branches; };
    private:
        PetscErrorCode setupTopologyFromOptions(PetscOptions &options);
        PetscErrorCode setupPositionFromOptions(PetscOptions &options);
        PetscErrorCode setupPropertyFromOptions(PetscOptions &options);
        PetscErrorCode bending();
        PetscErrorCode advection();
        DM             *ptrDM;      // pointer to the network
        PetscInt       *netnum;     // global indices of the subnetworks
        PetscInt       *nedge;      // list of truss numbers in each subnetwork
        PetscInt       Nsubnet;     // number of subnetworks
        PetscInt       Nedge;       // total number of trusses
        PetscInt       Nnode;       // total number of nodes
        Branch         *branches;   // list of branches
        PetscBool      flag = PETSC_FALSE;
};

#endif