/*
 * File: UniformCurrent.h
 * Desc: derived class from Flows, implementation of uniform current
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 26 May, 2023
 * Last modified: 16 August, 2023
 */

#ifndef UNIFORM_CURRENT_
#define UNIFORM_CURRENT_

#include "Flows.h"

class UniformCurrent: public Flows
{
    public:
        UniformCurrent() = default;
        UniformCurrent(const PetscScalar velocity) :
            U(velocity) {};
        ~UniformCurrent() = default;
        // horizontal velocity
        PetscScalar u(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return U; };
        // vertical velocity
        PetscScalar w(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // horizontal acceleration
        PetscScalar ax(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // vertical acceleration
        PetscScalar az(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of horizontal velocity with respect to x
        PetscScalar dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of horizontal velocity with respect to z
        PetscScalar dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of vertical velocity with respect to x
        PetscScalar dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
        // derivative of vertical velocity with respect to z
        PetscScalar dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override
            { return 0; };
    private:
        const PetscScalar U;    // current velocity
};

#endif