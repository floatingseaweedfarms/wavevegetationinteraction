/*
 * File: Projection.h
 * Desc: projection method to solve the pressure
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 13 April, 2022
 * Last Modified: 17 August, 2023
 */

#ifndef PROJECTION_
#define PROJECTION_

#include <petsc.h>

class Projection
{
    public:
        Projection() = default;
        Projection(const DM &dm, 
            const PetscScalar Dx, 
            const PetscScalar Dz,
            const PetscInt bc = 0);
        ~Projection() = default;
        PetscErrorCode solve(Vec &sol, const Vec &pbc,
            const PetscScalar dt) const;
        PetscErrorCode divergence(Vec &div, const Vec &sol) const;
        PetscErrorCode destroy();
    private:
        const DM    *ptrDM  ;
        PetscScalar dx, dz  ;  // grid cell sizes
        PetscInt    xbc     ;  // boundary condition, periodic or non-periodic
        DM          pDM     ;  // DMDA for pressure
        KSP         ksp     ;  // Poisson equation solver
        PC          prec    ;  // preconditioner
        Mat         A       ;  // coefficient matrix
        Mat         dU      ;  // matrix to return divergence of veloicty
        Mat         dP      ;  // matrix to return gradient of pressure
        Mat         U       ;  // matrix to return velocity from sol
        Mat         ddP1stBC;  // second derivative of pressure on the boundary
        Mat         dP1stBC ;  // gradient of pressure on the boundary
        
};

#endif