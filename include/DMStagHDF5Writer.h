/*
 * File: DMStagHDF5Writer.h
 * Desc: write velocities, pressure into a HDF5 file
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 24 May, 2022
 * Last modified: 17 August, 2023
 */

#ifndef DMSTAGHDF5WRITER_
#define DMSTAGHDF5WRITER_

#include <petsc.h>
#include "PotentialWaves.h"

class DMStagHDF5Writer
{
    public:
        DMStagHDF5Writer()  = default;
        DMStagHDF5Writer(const PetscViewer &viewer,
            const DM &dm);
        ~DMStagHDF5Writer() = default;
        PetscErrorCode pushGroup();
        PetscErrorCode writeSolutionToHDF5(const Vec &sol,
            const PetscScalar T);
        PetscErrorCode writeElevationToHDF5(const Vec &eta,
            const PetscScalar T);
        PetscErrorCode writeWaveToHDF5(const PotentialWaves &wave);
        PetscErrorCode popGroup();
        PetscErrorCode destroy();
    private:
        const PetscViewer *ptrViewer;
        const DM          *ptrDM;
        PetscInt          index_ele;
        PetscInt          index_sol;
        PetscBool         flag;
        Mat               pU;
        Mat               pW;
        IS                topEta;
};

#endif