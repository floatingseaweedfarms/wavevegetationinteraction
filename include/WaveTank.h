/*
 * File     : WaveTank.h
 * Desc     : numerical wave tank
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created : 8 September, 2022
 * Last modified: 16 August, 2023
 */

#ifndef WAVE_TANK_
#define WAVE_TANK_

#include <petsc.h>
#include "Projection.h"
#include "WaveMaker.h"


class WaveTank
{
    public:
        WaveTank() = default;
        WaveTank(const DM &dm,
            const Projection &proj,
            const WaveMaker  &maker,
            const PetscInt   &bc = 0);
        ~WaveTank() = default;
        PetscErrorCode update(Vec &sol, Vec &eta,
            const PetscScalar t, const PetscScalar dt);
        PetscErrorCode update(Vec &sol, Vec &eta,
            const Vec &source,
            const PetscScalar t, const PetscScalar dt);
        PetscErrorCode destroy();
    private:
        const Projection *ptrProj;
        const WaveMaker  *ptrMaker;
        PetscInt xbc;
        Mat topWMat;
        Vec topw;
        Vec topp;
};

#endif