/*
 * File: Constants.h
 * Desc: some common constants
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 8 April, 2022
 * Last modified: 16 August, 2023
 */

#ifndef CONSTANTS_
#define CONSTANTS_

#include <petscsys.h>

const PetscScalar g       =    9.81;        // gravitational acceleration
const PetscScalar rhow    = 1000.00;        // water density

#endif