/*
 * File     : WaveMaker.h
 * Desc     : Wave Relaxation Zones to generate and absorb waves
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 26 May, 2023
 * Last modified: 16 August, 2023
 */

#ifndef WAVE_MAKER_
#define WAVE_MAKER_

#include <petsc.h>
#include "PotentialWaves.h"

class WaveMaker
{
    public:
        WaveMaker() = default;
        WaveMaker(const DM &dm, const PotentialWaves &wave,
            const PetscScalar xMin, const PetscScalar xMax,
            const PetscScalar Gamma,
            const PetscScalar n     = 2.0,
            const PetscScalar nRamp = 3.0,
            const PetscScalar nIn   = 1.0,
            const PetscScalar nOut  = 2.0);
        ~WaveMaker() = default;
        PetscErrorCode force(Vec &sol, Vec &eta,
            const PetscScalar t,
            const PetscScalar dt) const;
        PetscErrorCode destroy();
    private:
        PetscScalar blend(const PetscScalar x,
            const PetscScalar xmin, const PetscScalar xmax,
            const PetscScalar n,
            const PetscScalar nin,
            const PetscScalar nout,
            const PetscScalar L) const;
        const DM             *ptrDM  ;  // pointer to the dmstag grid
        const PotentialWaves *ptrWave;  // pointer to the wave
        const PetscScalar    xmin    ;  // minimum x coordinate value       
        const PetscScalar    xmax    ;  // maximum x coordinate value
        const PetscScalar    gamma   ;  // smoothing parameter
        const PetscScalar    nramp   ;  // nramp periods to start
        const PetscScalar    nin     ;  // nin wave lengths of wave making zone
        const PetscScalar    nout    ;  // nout wave lengths of wave damping zone
        Vec                  blender ;  // blender for velocity and suface elevation
};

#endif