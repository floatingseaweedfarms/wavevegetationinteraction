/*
 * File: RigidBar.h
 * Desc: a suspended rigid bar
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 27 February, 2024
 */

#ifndef RIGID_BAR_
#define RIGID_BAR_

#include "Flows.h"
#include "Vector.h"

class RigidBar
{
    public:
        RigidBar();
        RigidBar(const PetscScalar x,
                 const PetscScalar z,
                 const PetscScalar length,
                 const PetscScalar width,
                 const PetscScalar bodymass,
                 const PetscScalar addedmass,
                 const PetscScalar massdiff,
                 const PetscInt segments,
                 const PetscScalar Cd);
        ~RigidBar() { PetscFree(F); };
        PetscErrorCode update_RK2nd(const PetscScalar dt, const PetscInt step);
        PetscErrorCode hydroload(Vector3s &load, PetscScalar &f,
            const PetscScalar s,
            const Vector3s &xf,
            const Vector3s &uf,
            const Vector3s &af) const;
        PetscScalar* loads() const { return F; };
        PetscScalar theta() const { return Theta; };
        PetscScalar dtheta() const { return dTheta; };
        PetscInt segments() const { return N; };
        PetscScalar length() const { return l; };
        PetscErrorCode hangingPoint(PetscScalar &x, PetscScalar &z) const
            { x = x0; z = z0; return 0; };
    private:
        const PetscScalar x0;   // suspended position, x
        const PetscScalar z0;   // suspended position, z
        const PetscScalar  l;   // length
        const PetscScalar  b;   // width
        const PetscScalar mu;   // body mass per unit length
        const PetscScalar ma;   // added mass per unit length
        const PetscScalar dm;   // mass difference in fluid per unit length
        const PetscScalar  I;   // moment of inertia
        const PetscInt     N;   // number of segments
        const PetscScalar ds;   // segment size
        const PetscScalar CD;   // drag coefficient
        PetscScalar    Theta;   // rotational angle
        PetscScalar   dTheta;   // angular velocity
        PetscScalar  ddTheta_0; 
        PetscScalar  dTheta_0 ;
        PetscScalar  Theta_0  ;
        PetscScalar  *F;
};

#endif