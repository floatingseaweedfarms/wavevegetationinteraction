/*
 * File: HydroLoadOnTrussSystem.h
 * Desc: hydrodynamic load directly applied on the truss
 *       one-way model
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 20 April, 2023
 * Last modified: 16 August, 2023
 */

#ifndef HYDRO_LOAD_ON_TRUSS_SYSTEM_
#define HYDRO_LOAD_ON_TRUSS_SYSTEM_

#include <petsc.h>
#include "Flows.h"
#include "PotentialWaves.h"

class HydroLoadOnTrussSystem
{
    public:
        HydroLoadOnTrussSystem() = default;
        HydroLoadOnTrussSystem(const DM &dm,
            const Flows &flow) : ptrDM(&dm),
            ptrFlow(&flow) {};
        ~HydroLoadOnTrussSystem() { ptrDM = NULL; ptrFlow = NULL; };
        PetscErrorCode load(const PetscScalar T,
            const PetscScalar factor = 1.0);
    private:
        const DM       *ptrDM;       // pointer to the network
        const Flows    *ptrFlow;     // pointer to the wave
};

#endif