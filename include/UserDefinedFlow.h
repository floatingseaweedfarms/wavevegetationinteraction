/*
 * File: UserDefinedFlow.h
 * Desc: derived class from Flows, implementation of any user-defined flow
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 18 August, 2023
 */

#ifndef USER_DEFINED_FLOW_
#define USER_DEFINED_FLOW_

#include "Flows.h"

class UserDefinedFlow: public Flows
{
    public:
        UserDefinedFlow () = default;
        ~UserDefinedFlow() = default;
        // horizontal velocity
        PetscScalar u   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // vertical velocity
        PetscScalar w   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // horizontal acceleration
        PetscScalar ax  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // vertical acceleration
        PetscScalar az  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of horizontal velocity with respect to x
        PetscScalar dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of horizontal velocity with respect to z
        PetscScalar dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of vertical velocity with respect to x
        PetscScalar dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of vertical velocity with respect to z
        PetscScalar dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
};

#endif