/*
 * File: HydroLoad.h
 * Desc: the hydrodynamic load consists of two parts
 *       1) the resistive drag
 *       2) the reactive force
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 22 May, 2023
 * Last modified: 18 August, 2023
 */

#ifndef HYDROLOAD_
#define HYDROLOAD_

#include "Truss.h"

class HydroLoad
{
    public:
        HydroLoad () = default;
        ~HydroLoad() = default;
        /*
         * Output
         *  - F, the hydrodynamic load on the truss
         * Input
         *  - t, the target truss
         *  - X, position vector at the truss center
         *  - U, velocity vector of the flow at truss center
         *  - A, acceleration vector of the flow at the truss center
         *  - du, gradient tensor of the flow velocity
         */
        PetscErrorCode load(Vector3s &F, Truss &t,
            const Vector3s &X,
            const Vector3s &U,
            const Vector3s &A,
            const Matrix3s &du) const;
};

#endif