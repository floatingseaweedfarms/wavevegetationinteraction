/*
 * File       : AiryWave.h
 * Description: drived class from PotentialWaves
 *              Airy wave theory, linear wave theory
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 28 May, 2023
 * Last modified: 16 August, 2023
 */

#ifndef AIRYWAVE_
#define AIRYWAVE_

#include "PotentialWaves.h"

class AiryWave: public PotentialWaves
{
    public:
        AiryWave() = default;
        AiryWave(const PetscScalar period, const PetscScalar length,
            const PetscScalar depth, const PetscScalar height) : 
            PotentialWaves(period, length, depth, height) {};
        ~AiryWave() = default;
        // horizontal velocity
        PetscScalar u   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // vertical velocity
        PetscScalar w   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // horizontal acceleration
        PetscScalar ax  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // vertical acceleration
        PetscScalar az  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of horizontal velocity with respect to x
        PetscScalar dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of horizontal velocity with respect to z
        PetscScalar dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of vertical velocity with respect to x
        PetscScalar dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of vertical velocity with respect to z
        PetscScalar dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // pressure
        PetscScalar p (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // elevation
        PetscScalar elevation(const PetscScalar x, const PetscScalar t) const override;
};

#endif