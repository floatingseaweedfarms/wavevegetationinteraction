/*
 * File: Truss.h
 * Desc: define struct Node, Truss and Branch
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created : 18 January, 2022
 * Last modified: 16 August, 2023
 */

#ifndef TRUSS_
#define TRUSS_

#include "Vector.h"

typedef struct
{
    PetscInt    id;
    PetscBool   fixed;
    PetscScalar damping;
    PetscScalar mass;   
    PetscScalar addedmass;
    Vector3s    x;          // position
    Vector3s    u;          // velocity
    Vector3s    a;          // acceleration
    Vector3s    x_0_2;      // position at middle step
    Vector3s    u_0_2;      // velocity at middle step
    Vector3s    a_0_2;      // acceleration at middle step
    PetscBool   set;        // whether the node has been set before
    Vector3s    M;          // moment
    Vector3s    shear;      // shear
    Vector3s    F;          // total load
    Vector3s    Fe;         // external load
    Vector3s    Fp;         // prescribed loads
    Vector3s    NB;         // net buoyancy
    Vector3s    Kappa;      // curvature
    PetscScalar Ut;         // tangential projection of relative velocity
} Node;

typedef struct
{
    PetscInt    id;         
    PetscScalar l0;         // length without tension
    PetscScalar l;          // instant length
    PetscScalar b;          // width
    PetscScalar d;          // thickness
    PetscScalar mass;       // mass
    PetscScalar addedmass;  // added mass
    PetscScalar dl;         // strain
    Vector3s    dX;         // position difference between ending node and starting node
    Vector3s    x;          // position of truss center
    Vector3s    u;          // velocity of truss center
    Vector3s    a;          // acceleration of truss center
    Vector3s    x_0_2;      // position at middle step of truss center
    Vector3s    u_0_2;      // velocity at middle step of truss center
    Vector3s    a_0_2;      // acceleration at middle step of truss center
    PetscScalar EA;         // axial stiffness
    PetscScalar EI;         // bending stiffness
    PetscScalar T;          // tension
    Vector3s    F;          // hydro load
    Vector3s    NB;         // net buoyancy
    PetscScalar Cd;         // drag coefficient
    PetscScalar Cm;         // mass coefficient
    Vector3s    Kappa;      // curvature
    PetscScalar Ut;         // tangential projection of relative velocity at truss center
} Truss;

/*
 * Generally, trusses in each branch have same properties, such as cross-section, stiffness, and hydrodynamic coefficients
 */

typedef struct
{
    PetscBool   stiff;      // whether the branch is stiff
    Vector3s    sym;        // symmetry about each plane, 1 for same and -1 for opposite
    Vector3s    load;       // load at the branch root, all external loads and inertia force
} Branch;

#endif