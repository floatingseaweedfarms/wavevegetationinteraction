/*
 * File: PotentialWaves.h
 * Desc: derived class from Flows, implementation of potential waves
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 26 May, 2023
 * Last modified: 16 August, 2023
 */

#ifndef POTENTIAL_WAVES_
#define POTENTIAL_WAVES_

#include "Flows.h"

class PotentialWaves: public Flows
{
    public:
        PotentialWaves() = default;
        PotentialWaves(const PetscScalar period, const PetscScalar length,
            const PetscScalar depth, const PetscScalar height,
            const PetscScalar tol = 1.0e-4);
        virtual ~PotentialWaves() = default;
        // horizontal velocity
        virtual PetscScalar u   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // vertical velocity
        virtual PetscScalar w   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // horizontal acceleration
        virtual PetscScalar ax  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // vertical acceleration
        virtual PetscScalar az  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of horizontal velocity with respect to x
        virtual PetscScalar dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of horizontal velocity with respect to z
        virtual PetscScalar dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of vertical velocity with respect to x
        virtual PetscScalar dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of vertical velocity with respect to z
        virtual PetscScalar dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // pressure
        virtual PetscScalar p(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // elevation
        virtual PetscScalar elevation(const PetscScalar x, const PetscScalar t) const = 0;
        // wave length
        PetscScalar length  () const { return L; };
        // wave period
        PetscScalar period  () const { return T; };
        // wave height
        PetscScalar height  () const { return H; };
        // wave depth
        PetscScalar depth   () const { return h; };
        // wave celerity
        PetscScalar celerity() const { return c; };
    protected:
        const PetscScalar T;       // wave period
        const PetscScalar L;       // wave length
        const PetscScalar h;       // water depth
        const PetscScalar H;       // wave height
        const PetscScalar k;       // wave number
        const PetscScalar omega;   // wave angular frequency
        const PetscScalar c;       // wave speed
};

#endif