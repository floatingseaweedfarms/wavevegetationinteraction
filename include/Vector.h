/*
 * File       : Vector.h
 * Description: 2D and 3D vectors
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 *              
 * Created : 26 August, 2022
 * Last modified: 16 August, 2023
 */

#ifndef EIGEN_VECTOR_PETSCSCALAR_
#define EIGEN_VECTOR_PETSCSCALAR_

#include <petscsys.h>
#include <eigen3/Eigen/Dense>

typedef Eigen::Matrix<PetscScalar, 2, 1> Vector2s;
typedef Eigen::Matrix<PetscScalar, 3, 1> Vector3s;
typedef Eigen::Matrix<PetscScalar, 3, 3> Matrix3s;

#endif