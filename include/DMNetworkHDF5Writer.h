/*
 * File: DMNetworkHDF5Writer.h
 * Desc:  write data from DMnetwork to hdf5 file
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 1 September, 2022
 * Last modified: 18 August, 2023
 */

#ifndef DMNETWORK_HDF5_WRITER_
#define DMNETWORK_HDF5_WRITER_

#include <petsc.h>
#include <petscdmnetwork.h>
#include <petscviewerhdf5.h>
#include "TrussSystem.h"

class DMNetworkHDF5Writer
{
    public:
        DMNetworkHDF5Writer() = default;
        DMNetworkHDF5Writer(const PetscViewer &viewer,
            const PetscInt num_node,
            const PetscInt num_truss,
            const PetscInt num_subnet,
            const TrussSystem &ts,
            const DM &dm);
        ~DMNetworkHDF5Writer() { ptrviewer = NULL; };
        PetscErrorCode pushGroup  ();
        PetscErrorCode popGroup   ();
        PetscErrorCode writeToHDF5(const DM &dm,
            const PetscScalar T);
    private:
        const PetscViewer   *ptrviewer;
        const TrussSystem   *ptrTS;
        PetscInt            nnode;
        PetscInt            ntruss;
        PetscInt            nsubnet;
        PetscInt            index;
        PetscBool           flag;
        const char          *groupname = "TrussSystem";
};

#endif