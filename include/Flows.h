/*
 * File: Flows.h
 * Desc: base class for flows
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 26 May, 2023
 * Last modified: 16 August, 2023
 */

#ifndef FLOWS_
#define FLOWS_

#include <petscsys.h>

class Flows
{
    public:
        Flows() = default;
        virtual ~Flows() = default;
        // horizontal velocity
        virtual PetscScalar u   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // vertical velocity
        virtual PetscScalar w   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // horizontal acceleration
        virtual PetscScalar ax  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // vertical acceleration
        virtual PetscScalar az  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of horizontal velocity with respect to x
        virtual PetscScalar dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of horizontal velocity with respect to z
        virtual PetscScalar dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of vertical velocity with respect to x
        virtual PetscScalar dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
        // derivative of vertical velocity with respect to z
        virtual PetscScalar dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const = 0;
};

#endif