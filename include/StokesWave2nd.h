/*
 * File       : StokesWave2nd.h
 * Description: drived class from PotentialWaves
 *              second order Stokes wave theory
 * 
 * Zhilong Wei <zhilwei@dtu.dk>
 * 
 * Created: 28 May, 2023
 * Last modified: 16 August, 2023
 */

#ifndef STOKESWAVE2ND_
#define STOKESWAVE2ND_

#include "PotentialWaves.h"

class StokesWave2nd: public PotentialWaves
{
    public:
        StokesWave2nd() = default;
        StokesWave2nd(const PetscScalar period, const PetscScalar length,
            const PetscScalar depth, const PetscScalar height) : 
            PotentialWaves(period, length, depth, height) {};
        ~StokesWave2nd() = default;
        // horizontal velocity
        PetscScalar u   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // vertical velocity
        PetscScalar w   (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // horizontal acceleration
        PetscScalar ax  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // vertical acceleration
        PetscScalar az  (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of horizontal velocity with respect to x
        PetscScalar dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of horizontal velocity with respect to z
        PetscScalar dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of vertical velocity with respect to x
        PetscScalar dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // derivative of vertical velocity with respect to z
        PetscScalar dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // pressure
        PetscScalar p (const PetscScalar x, const PetscScalar z, const PetscScalar t) const override;
        // elevation
        PetscScalar elevation(const PetscScalar x, const PetscScalar t) const override;
};

#endif