# Flexible Blades in Sinusoidal Flow

The details in the examples can be found in [Leclercq & de Langre (2018)](https://www.cambridge.org/core/journals/journal-of-fluid-mechanics/article/reconfiguration-of-elastic-blades-in-oscillatory-flow/93C943F932866F3DCAFE8C53F34540F7).

## Usage
NOTE that you may need to modify the path to `model` in the bash scripts.

* Enter the current directory

* Generate input files for all cases.

    ```shell
    ./setup.sh
    ```

* Run applications

    ```shell
    ./run.sh
    ```

* Extract data from output files. See more details in the provided python scripts under `tools`.

    - Tip spans in batch

        ```shell
        ./find_tip_span.sh
        ```
    
    - Trajectory. Replace `A` and `T` with your target amplitude and period

        ```shell
        python3 ./tools/extract_trajectory.py ./subcases/A_[A]_T_[T]/output/structure.h5 A_[A]_T_[T]
        ```

## References

1. [Leclercq, T., & de Langre, E. (2018). Reconfiguration of elastic blades in oscillatory flow. Journal of Fluid Mechanics, 838, 606-630.](https://doi.org/10.1017/jfm.2017.910)