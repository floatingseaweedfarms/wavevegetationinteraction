import numpy as np
import matplotlib.pyplot as plt
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

if len(sys.argv) < 2:
    print("You should give the input HDF5 file!\n")
    sys.exit()

input_file = sys.argv[1]
A          = float(sys.argv[2])
T          = float(sys.argv[3])

t     = read(input_file, "TrussSystem", "time")
Nt    = int(read(input_file, "TrussSystem", "number_truss"))
Nn    = int(read(input_file, "TrussSystem", "number_node"))
t     = t[:, 0]
steps = t.size

tip_xz = np.zeros((steps, 2))
for i in range(steps):
    pos = read(input_file, "TrussSystem", "XYZ_{:d}".format(i))
    tip_xz[i, 0] = pos[-3]
    tip_xz[i, 1] = pos[-2]

span = np.amax(tip_xz[:, 0]) - np.amin(tip_xz[:, 0])
l = 0.20
print("{:.4f},{:.4f},{:.4f}".format(A, T, span / l / 2))