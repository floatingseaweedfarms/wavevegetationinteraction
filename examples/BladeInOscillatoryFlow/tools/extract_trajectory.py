import numpy as np
import matplotlib.pyplot as plt
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

if len(sys.argv) < 2:
    print("You should give the input HDF5 file!\n")
    sys.exit()

input_file       = sys.argv[1]
output_file      = sys.argv[2]

for i in range(1):
    # check if the file given exists
    if not os.path.exists(sys.argv[i + 1]):
        print("The file " + sys.argv[i + 1] + " does not exist!\nPlease check again!\n")
        sys.exit()

t     = read(input_file, "TrussSystem", "time")
Nt    = int(read(input_file, "TrussSystem", "number_truss"))
Nn    = int(read(input_file, "TrussSystem", "number_node"))
t     = t[:, 0]
steps = t.size

N = int(steps / 2 + 0.5) + 1
tip_xz = np.zeros((N, 2))
for i in range(N):
    pos = read(input_file, "TrussSystem", "XYZ_{:d}".format(i+1))
    tip_xz[i, 0] = pos[-3]
    tip_xz[i, 1] = pos[-2]

fig = plt.figure(figsize=(8, 5))
plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.15)
ax = fig.add_subplot()

l = 0.20

xmin = -1.0
xmax =  1.0
ymin =  0.0
ymax =  1.0

ax.set_xlim(xmin, xmax)
ax.set_ylim(ymin, ymax)
ax.set_xlabel(r'$x/l$', fontsize=16)
ax.set_ylabel(r'$z/l$', fontsize=16)
ax.grid()

ax.plot(tip_xz[1:, 0] / l, tip_xz[1:, 1] / l, '-k')

np.savetxt(output_file + "_tip.dat", tip_xz / l, delimiter=',', header="x,z", fmt="%.4f", comments='')

with open(output_file + "_trajectory.dat", "a") as f:
    f.write("x,z\n")
    for i in range(1, N):
        pos = read(input_file, "TrussSystem", "XYZ_{:d}".format(i))
        pos = np.reshape(pos, (Nn, 3))
        ax.plot(pos[:, 0] / l, pos[:, 1] / l, '-b', alpha=0.6)

        np.savetxt(f, pos[:, :2] / l, fmt='%.5f', delimiter=',')
        f.write("\n")

plt.show()