#!/bin/bash

# current directory
path=$PWD

amplitudes=(0.054 0.130)
periods=(4.762 3.027 2.176 1.736 1.437 1.214 1.060 0.926)

for (( Ai=0; Ai<2; Ai++ )); do
    A=${amplitudes[$Ai]}
    for (( Ti=0; Ti<8; Ti++ )); do
        T=${periods[$Ti]}

        subpath=$path/subcases/A_${A}_T_${T}
        python3 ./tools/find_tip_span.py $subpath/output/structure.h5 $A $T
    done
done