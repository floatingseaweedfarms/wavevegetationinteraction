#!/bin/bash

# current directory
path=$PWD
model=~/DTUGitLab/WaveVegetationInteraction/

amplitudes=(0.054 0.130)
periods=(4.762 3.027 2.176 1.736 1.437 1.214 1.060 0.926)

# common variables
startTime=0
endTime=10
rampTime=1
deltaT=1.0e-5
recordStartTime=8
recordEndTime=10
recordFreq=40

damping=0
shape=rectangle
length=0.20
width=0.02
thickness=0.49e-3
ntruss=20
canopy=single
x0=0
y0=0
z0=0
x1=0
y1=0
z1=0.20
flexible=True
E=8.57e8
density=1000.00
sysx=-1
sysy=1
sysz=1
influid=True
rhow=1000
Cd=2.0
Cm=1.0

for (( Ai=0; Ai<2; Ai++ )); do
    A=${amplitudes[$Ai]}
    for (( Ti=0; Ti<8; Ti++ )); do
        T=${periods[$Ti]}

        subpath=$path/subcases/A_${A}_T_${T}
        echo $subpath

        if ! [ -d "$path/subcases" ]; then
            mkdir $path/subcases
        fi
        if ! [ -d "$subpath" ]; then
            mkdir $subpath
        fi
        if ! [ -d "$subpath/setup" ]; then
            mkdir $subpath/setup
        fi

        rm -rf $subpath/setup/*
        rm -rf $subpath/input/*
        rm -rf $subpath/output/*

        touch $subpath/setup/bladeinoscillatoryflow
        touch $subpath/setup/trusssystem_config
        touch $subpath/setup/init_velo.py

        config=$subpath/setup/bladeinoscillatoryflow
        truss_config=$subpath/setup/trusssystem_config
        init_velo=$subpath/setup/init_velo.py

        # config file
        echo $A >> $config
        echo $T >> $config
        echo $startTime >> $config
        echo $endTime >> $config
        echo $rampTime >> $config
        echo $deltaT >> $config
        echo $recordStartTime >> $config
        echo $recordEndTime >> $config
        echo $recordFreq >> $config

        # truss system config
        echo $damping >> $truss_config
        echo $shape >> $truss_config
        echo $width >> $truss_config
        echo $thickness >> $truss_config
        echo $length >> $truss_config
        echo $ntruss >> $truss_config
        echo $canopy >> $truss_config
        echo $x0 >> $truss_config
        echo $y0 >> $truss_config
        echo $z0 >> $truss_config
        echo $x1 >> $truss_config
        echo $y1 >> $truss_config
        echo $z1 >> $truss_config
        echo $flexible >> $truss_config
        echo $E >> $truss_config
        echo $density >> $truss_config
        echo $sysx >> $truss_config
        echo $sysy >> $truss_config
        echo $sysz >> $truss_config
        echo $influid >> $truss_config
        echo $rhow >> $truss_config
        echo $Cd >> $truss_config
        echo $Cm >> $truss_config

        # initial velocity
        echo "# initial velocities" >> $init_velo
        echo "def velo_func(xyz):" >> $init_velo
        echo -e "\tx, y, z = xyz" >> $init_velo
        echo -e "\treturn 0.0, 0.0, 0.0" >> $init_velo

        # generate input files
        cat $config | python3 $model/tools/setup_structure_in_sinusoidal_oscillatory_flow.py $subpath
    done
done