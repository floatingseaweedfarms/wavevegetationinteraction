import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq
from scipy.signal import find_peaks
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx

file = sys.argv[1]
# check if the file given exists
if not os.path.exists(file):
    print("The file " + file + " does not exist!\nPlease check again!\n")
    sys.exit()

t     = read(file, "Variables", "time_elevation")
x     = read(file, "Variables", "coordinates")[-1, :, 0]
eta   = read(file, "Variables", "elevation")
steps = t.size
t     = np.reshape(t, (steps, 1))

# wave
H   = read(file, "Wave", "height")[0]
h   = read(file, "Wave", "depth")[0]
L   = read(file, "Wave", "length")[0]
T   = read(file, "Wave", "period")[0]
k   = 2.0 * np.pi / L

# grid
Nx_L  = read(file, "Grid", "nodes_in_x")[0]
Nz_h  = read(file, "Grid", "nodes_in_z")[0]
Nx_L  = int(Nx_L)
Nz_h  = int(Nz_h)
steps = t.size

Nx = x.size
x = x + (x[1] - x[0]) / 2.0
xmin = x[ 0]
xmax = x[-1]

xmins = xmin
xmaxs = xmax - 2.0 * L

_, xmin_idx = find_nearest(x, xmins)
_, xmax_idx = find_nearest(x, xmaxs)

time = t
t = t[time != 0]
Nt = t.size
dt = t[1] - t[0]
eta = eta[-Nt:, :]

height_threshold = 0.05 * H

xx        = np.zeros(xmax_idx - xmin_idx)
amplitude = np.zeros(xmax_idx - xmin_idx)
freqs     = np.zeros(xmax_idx - xmin_idx)

for i in range(xmax_idx - xmin_idx):
    xx[i] = x[i + xmin_idx]
    elevation = eta[:, i + xmin_idx]
    yf=fft(elevation, norm='forward')[:Nt//2] * 2.0
    freq = fftfreq(Nt, d=dt)[:Nt//2]
    
    peaks_index, properties = find_peaks(np.abs(yf), height=height_threshold)
    freqs[i] = freq[peaks_index[0]]
    amplitude[i] = properties['peak_heights'][0]

heights = amplitude * 2.0

print("{:0.5e}".format(heights[-1] / H))