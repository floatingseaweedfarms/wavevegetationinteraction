import numpy as np
import matplotlib.pyplot as plt
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

if len(sys.argv) < 2:
    print("You should give the input HDF5 file!\n")
    sys.exit()

input_file       = sys.argv[1]

for i in range(1):
    # check if the file given exists
    if not os.path.exists(sys.argv[i + 1]):
        print("The file " + sys.argv[i + 1] + " does not exist!\nPlease check again!\n")
        sys.exit()

t     = read(input_file, "TrussSystem", "time")
Nt    = int(read(input_file, "TrussSystem", "number_truss"))
Nn    = int(read(input_file, "TrussSystem", "number_node"))
Nb    = int(read(input_file, "TrussSystem", "number_subnet"))
t     = t[1:, 0]
steps = t.size

loads = np.zeros((steps, 2))
for i in range(steps):
    load = read(input_file, "TrussSystem", "Load_{:d}".format(i))
    loads[i, 0] = load[0]
    loads[i, 1] = load[2]

t = t[1:]
loadx = loads[1:, 0]
loadz = loads[1:, 1]

Frms=np.sqrt(np.sum(np.square(loadx) / loadx.size)) * 31
print("{:0.5e}".format(Frms))