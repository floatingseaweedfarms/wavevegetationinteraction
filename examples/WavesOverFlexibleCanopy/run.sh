#!/bin/bash

# current directory
path=$PWD
model=~/DTUGitLab/WaveVegetationInteraction/

for (( j=0; j<14; ++j)); do
    case=$(printf "%02d" $((j+1)))
    subpath=$path/subcases/wave_${case}
    echo $subpath

    ${model}/build/apps/waveoverflexiblecanopy -input_path $subpath
done

for (( j=0; j<14; ++j)); do
    case=$(printf "%02d" $((j+1)))
    subpath=$path/subcases/xload_${case}
    echo $subpath

    ${model}/build/apps/waveoverflexiblecanopy -input_path $subpath
done