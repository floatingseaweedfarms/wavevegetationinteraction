#!/bin/bash

# current directory
path=$PWD
model=~/DTUGitLab/WaveVegetationInteraction/

depth=(0.40 0.30 0.40 0.30 0.40 0.40 0.40 0.40 0.40 0.40 0.40 0.30 0.40 0.40)
period=(1.40 2.00 1.40 1.40 1.00 2.00 2.00 2.00 1.40 1.40 1.00 0.80 0.80 0.80)
height=(0.018 0.029 0.024 0.029 0.031 0.035 0.038 0.037 0.032 0.032 0.036 0.032 0.032 0.032)
depth1=(-0.11 -0.06 -0.11 -0.06 -0.11 -0.11 -0.16 -0.11 -0.16 -0.11 -0.11 -0.06 -0.16 -0.11)
depth2=(-0.2066 -0.1566 -0.2066 -0.1566 -0.2066 -0.2066 -0.2566 -0.2066 -0.2566 -0.2066 -0.2066 -0.1566 -0.2566 -0.2066)
CD=(5.4 3.9 4.9 4.3 5.2 3.8 3.8 3.8 4.6 4.5 4.9 5.0 6.1 5.6)

# common variables
flowStartTime=0
flowEndTime=42
CFL=0.5
flowWriteStartTime=40
flowWriteEndTime=42
surfaceWriteStartTime=32
surfaceWriteEndTime=42
recordFreq=40
distance=3.8
gap_before=1
gap_after=1
startpoint=0
gamma=6.68
nexp=2.0
nramp=3
nin=1
nout=2
Nx_per_L=40
Nz_per_h=20
structureStartTime=20
structureEndTime=42
structureRamp=1
structureDeltaT=2.0e-4
structureWriteStartTime=40
structureWriteEndTime=42
structureRecordFreq=40

damping=0
shape=rectangle
width=9.5e-3
thickness=1.0e-3
length=0.0966
Nt=10
canopy=patch
stem_density_in_x=5
stem_density_in_y=105
x0=0
x1=3.8
flexible=True
E=2.04e6
rhos=1200
sysx=-1
sysy=1
sysz=1
influid=True
rhow=1000
Cm=1.00

for (( j=0; j<14; ++j)); do
    case=$(printf "%02d" $((j+1)))
    # echo $case

    subpath=$path/subcases/wave_${case}
    h=${depth[$j]}
    T=${period[$j]}
    H=${height[$j]}
    z0=${depth1[$j]}
    z1=${depth2[$j]}
    Cd=${CD[$j]}

    echo $h $T $H $z0 $z1 $Cd

    if ! [ -d "$path/subcases" ]; then
        mkdir $path/subcases
    fi
    if ! [ -d "$subpath" ]; then
        mkdir $subpath
    fi
    if ! [ -d "$subpath/setup" ]; then
        mkdir $subpath/setup
    fi

    rm -rf $subpath/setup/*
    rm -rf $subpath/input/*
    rm -rf $subpath/output/*

    config=$subpath/setup/waveoverflexiblecanopy
    truss_config=$subpath/setup/trusssystem_config
    init_velo=$subpath/setup/init_velo.py

    # config file
    echo $h >> $config
    echo $T >> $config
    echo $H >> $config
    echo $flowStartTime >> $config
    echo $flowEndTime >> $config
    echo $CFL >> $config
    echo $flowWriteStartTime >> $config
    echo $flowWriteEndTime >> $config
    echo $surfaceWriteStartTime >> $config
    echo $surfaceWriteEndTime >> $config
    echo $recordFreq >> $config
    echo $distance >> $config
    echo $gap_before >> $config
    echo $gap_after >> $config
    echo $startpoint >> $config
    echo $gamma >> $config
    echo $nexp >> $config
    echo $nramp >> $config
    echo $nin >> $config
    echo $nout >> $config
    echo $Nx_per_L >> $config
    echo $Nz_per_h >> $config
    echo $structureStartTime >> $config
    echo $structureEndTime >> $config
    echo $structureRamp >> $config
    echo $structureDeltaT >> $config
    echo $structureWriteStartTime >> $config
    echo $structureWriteEndTime >> $config
    echo $structureRecordFreq >> $config

    # truss system config
    echo $damping >> $truss_config
    echo $shape >> $truss_config
    echo $width >> $truss_config
    echo $thickness >> $truss_config
    echo $length >> $truss_config
    echo $Nt >> $truss_config
    echo $canopy >> $truss_config
    echo $stem_density_in_x >> $truss_config
    echo $stem_density_in_y >> $truss_config
    echo $x0 >> $truss_config
    echo $x1 >> $truss_config
    echo $z0 >> $truss_config
    echo $z1 >> $truss_config
    echo $flexible >> $truss_config
    echo $E >> $truss_config
    echo $rhos >> $truss_config
    echo $sysx >> $truss_config
    echo $sysy >> $truss_config
    echo $sysz >> $truss_config
    echo $influid >> $truss_config
    echo $rhow >> $truss_config
    echo $Cd >> $truss_config
    echo $Cm >> $truss_config

    # initial velocity
    echo "# initial velocities" >> $init_velo
    echo "def velo_func(xyz):" >> $init_velo
    echo -e "\tx, y, z = xyz" >> $init_velo
    echo -e "\treturn 0.0, 0.0, 0.0" >> $init_velo

    # generate input files
    cat $config | python3 $model/tools/setup_wave_over_flexible_canopy.py $subpath
done

for (( j=0; j<14; ++j)); do
    case=$(printf "%02d" $((j+1)))
    # echo $case

    subpath=$path/subcases/xload_${case}
    h=${depth[$j]}
    T=${period[$j]}
    H=${height[$j]}
    z0=${depth1[$j]}
    z1=${depth2[$j]}
    Cd=${CD[$j]}

    stem_density_in_x=1
    x1=0.2
    distance=0.2

    echo $h $T $H $z0 $z1 $Cd

    if ! [ -d "$path/subcases" ]; then
        mkdir $path/subcases
    fi
    if ! [ -d "$subpath" ]; then
        mkdir $subpath
    fi
    if ! [ -d "$subpath/setup" ]; then
        mkdir $subpath/setup
    fi

    rm -rf $subpath/setup/*
    rm -rf $subpath/input/*
    rm -rf $subpath/output/*

    config=$subpath/setup/waveoverflexiblecanopy
    truss_config=$subpath/setup/trusssystem_config
    init_velo=$subpath/setup/init_velo.py

    # config file
    echo $h >> $config
    echo $T >> $config
    echo $H >> $config
    echo $flowStartTime >> $config
    echo $flowEndTime >> $config
    echo $CFL >> $config
    echo $flowWriteStartTime >> $config
    echo $flowWriteEndTime >> $config
    echo $surfaceWriteStartTime >> $config
    echo $surfaceWriteEndTime >> $config
    echo $recordFreq >> $config
    echo $distance >> $config
    echo $gap_before >> $config
    echo $gap_after >> $config
    echo $startpoint >> $config
    echo $gamma >> $config
    echo $nexp >> $config
    echo $nramp >> $config
    echo $nin >> $config
    echo $nout >> $config
    echo $Nx_per_L >> $config
    echo $Nz_per_h >> $config
    echo $structureStartTime >> $config
    echo $structureEndTime >> $config
    echo $structureRamp >> $config
    echo $structureDeltaT >> $config
    echo $structureWriteStartTime >> $config
    echo $structureWriteEndTime >> $config
    echo $structureRecordFreq >> $config

    # truss system config
    echo $damping >> $truss_config
    echo $shape >> $truss_config
    echo $width >> $truss_config
    echo $thickness >> $truss_config
    echo $length >> $truss_config
    echo $Nt >> $truss_config
    echo $canopy >> $truss_config
    echo $stem_density_in_x >> $truss_config
    echo $stem_density_in_y >> $truss_config
    echo $x0 >> $truss_config
    echo $x1 >> $truss_config
    echo $z0 >> $truss_config
    echo $z1 >> $truss_config
    echo $flexible >> $truss_config
    echo $E >> $truss_config
    echo $rhos >> $truss_config
    echo $sysx >> $truss_config
    echo $sysy >> $truss_config
    echo $sysz >> $truss_config
    echo $influid >> $truss_config
    echo $rhow >> $truss_config
    echo $Cd >> $truss_config
    echo $Cm >> $truss_config

    # initial velocity
    echo "# initial velocities" >> $init_velo
    echo "def velo_func(xyz):" >> $init_velo
    echo -e "\tx, y, z = xyz" >> $init_velo
    echo -e "\treturn 0.0, 0.0, 0.0" >> $init_velo

    # generate input files
    cat $config | python3 $model/tools/setup_wave_over_flexible_canopy.py $subpath
done