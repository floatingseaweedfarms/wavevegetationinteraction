# Waves over a Suspended, Flexible Canopy

The details in the examples can be found in [Zhu et al. (2021)](https://doi.org/10.1016/j.coastaleng.2021.103947).

## Usage

NOTE that you may need to modify the path to `model` in the bash scripts.

* Enter the current directory

* Generate input files for all cases

    ```shell
    ./setup.sh
    ```

* Run applications

    ```shell
    ./run.sh
    ```

* Extract data from output files. Replace `[case]` with case number `01-14`. See more details in the provided python scripts under `tools`.

    - Wave transmission ratio

        ```shell
        python3 ./tools/wave_transmission_ratio.py ./subcases/wave_[case]/output/flow.h5
        ```

    - Horizontal load on one row
    
        ```shell
        python3 ./tools/xload_on_longline.py ./subcases/wave_[case]/output/structure.h5
        ```


## References

1. [Zhu, L., Lei, J., Huguenard, K., & Fredriksson, D. W. (2021). Wave attenuation by suspended canopies with cultivated kelp (Saccharina latissima). Coastal Engineering, 168, 103947.](https://doi.org/10.1016/j.coastaleng.2021.103947)