# Wave Induced Motion of Flexible Blades

The details in the examples can be found in [Luhar and Nepf (2016)](https://doi.org/10.1016/j.jfluidstructs.2015.11.007).

## Usage

* Enter the current directory

* Generate input files

    ```shell
    cat ./HDPE_N_20_dt_1.0e-5/setup/directwaveloadonstructure | python3 ../../tools/setup_structure_in_user_defined_flow.py ./HDPE_N_20_dt_1.0e-5/
    cat ./Silicon_N_20_dt_3.0e-4/setup/directwaveloadonstructure | python3 ../../tools/setup_structure_in_user_defined_flow.py ./Silicon_N_20_dt_3.0e-4/
    ```

* Run applications

    ```shell
    ../../build/apps/bladeinuserdefinedflow -input_path ./HDPE_N_20_dt_1.0e-5/
    ../../build/apps/bladeinuserdefinedflow -input_path ./Silicon_N_20_dt_3.0e-4/
    ```

* Extract data from output files. See more details in the provided python scripts under `tools`.

    - Load in x-direction

        ```shell
        python3 ./tools/extract_force_HDPE.py ./HDPE_N_20_dt_1.0e-5/output/structure.h5 HDPE_xload.ascii
        python3 ./tools/extract_force_Silicon.py ./Silicon_N_20_dt_3.0e-4/output/structure.h5 Silicon_xload.ascii
        ```

    - Trajectory

        ```shell
        python3 ./tools/extract_trajectory.py ./HDPE_N_20_dt_1.0e-5/output/structure.h5 HDPE
        python3 ./tools/extract_trajectory.py ./Silicon_N_20_dt_3.0e-4/output/structure.h5 Silicon
        ```

## References

1. [Luhar, M., & Nepf, H. M. (2016). Wave-induced dynamics of flexible blades. Journal of Fluids and Structures, 61, 20-41.](https://doi.org/10.1016/j.jfluidstructs.2015.11.007)