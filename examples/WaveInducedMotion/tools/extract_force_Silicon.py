import numpy as np
import matplotlib.pyplot as plt
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

if len(sys.argv) < 2:
    print("You should give the input HDF5 file!\n")
    sys.exit()

input_file       = sys.argv[1]
output_file      = sys.argv[2]

for i in range(1):
    # check if the file given exists
    if not os.path.exists(sys.argv[i + 1]):
        print("The file " + sys.argv[i + 1] + " does not exist!\nPlease check again!\n")
        sys.exit()

t     = read(input_file, "TrussSystem", "time")
Nt    = int(read(input_file, "TrussSystem", "number_truss"))
Nn    = int(read(input_file, "TrussSystem", "number_node"))
Nb    = int(read(input_file, "TrussSystem", "number_subnet"))
t     = t[:, 0]
steps = t.size

loads = np.zeros((steps, 3 * Nb))
for i in range(steps):
    loads[i, :] = read(input_file, "TrussSystem", "Load_{:d}".format(i))

Fx = loads[:, 0]
Fz = loads[:, 2]

b = 0.02
l = 0.20
Cd = 3.65
rho = 1000
U = 0.206
F0 = 0.5 * rho * Cd * b * l * U * U
print(F0)
T = 2.0

fig = plt.figure(figsize=(8, 5))
plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.15)
ax = fig.add_subplot()

xmin = 0
xmax = 2
ymin = -0.07
ymax =  0.07

ax.set_xlim(xmin, xmax)
ax.set_ylim(ymin, ymax)
ax.set_xlabel(r'$t (\mathrm{s})$', fontsize=16)
ax.set_ylabel(r'$F_x (\mathrm{N})$', fontsize=16)
ax.grid()

ax.plot(t - 36.45, Fx, '-b', lw=3.0)

data = np.zeros((steps - 1, 2))
data[:, 0] = (t[1:] - 36.45) / T
data[:, 1] = Fx[1:] / F0
np.savetxt(output_file, data, header='t, Fx', comments='', delimiter=',', fmt='%.5f')

plt.show()