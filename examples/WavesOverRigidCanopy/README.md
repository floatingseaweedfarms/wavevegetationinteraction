# Waves over a Rigid Canopy

The details in the examples can be found in Jacobsen & MaFall (2022).

## Usage

NOTE that you need to replace `[case]` with `R6`, `R7`, `R8`, or `R9`.

* Enter the current directory

* Generate input files

    ```shell
    cat ./[case]_CFL_1.00_Nt_10_Nx_40_Nz_20/setup/waveoverrigidcanopy | python3 ../../tools/setup_wave_over_rigid_canopy.py ./[case]_CFL_1.00_Nt_10_Nx_40_Nz_20/
    ```

* Run applications

    ```shell
    ../../build/apps/waveoverrigidcanopy -input_path ./[case]_CFL_1.00_Nt_10_Nx_40_Nz_20/
    ```

* Extract data. Giving an additional argument to the python scripts will write the results. See more details in the scripts.

    - Wave attenuation

        ```shell
        python3 ./tools/wave_attenuation.py ./[case]_CFL_1.00_Nt_10_Nx_40_Nz_20/output/flow.h5
        ```

    - Flow velocity profiles

        ```shell
        python3 ./tools/amplitude_over_lines.py ./[case]_CFL_1.00_Nt_10_Nx_40_Nz_20/output/flow.h5 ADV_locations.ascii 20
        ```

## References

1. [Jacobsen, N. G., & McFall, B. C. (2022). Wave-averaged properties for non-breaking waves in a canopy: Viscous boundary layer and vertical shear stress distribution. Coastal Engineering, 174, 104117.](https://doi.org/10.1016/j.coastaleng.2022.104117)