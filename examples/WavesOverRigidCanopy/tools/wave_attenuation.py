import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq
from scipy.signal import find_peaks
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx

file = sys.argv[1]

# check if the file given exists
if not os.path.exists(file):
    print("The file " + file + " does not exist!\nPlease check again!\n")
    sys.exit()

t     = read(file, "Variables", "time_elevation")
x     = read(file, "Variables", "coordinates")[-1, :, 0]
eta   = read(file, "Variables", "elevation")
steps = t.size
t     = np.reshape(t, (steps, 1))

# wave
H   = read(file, "Wave", "height")[0]
h   = read(file, "Wave", "depth")[0]
L   = read(file, "Wave", "length")[0]
T   = read(file, "Wave", "period")[0]
k   = 2.0 * np.pi / L

# grid
Nx_L  = read(file, "Grid", "nodes_in_x")[0]
Nz_h  = read(file, "Grid", "nodes_in_z")[0]
Nx_L  = int(Nx_L)
Nz_h  = int(Nz_h)
steps = t.size

Nx = x.size
x = x + (x[1] - x[0]) / 2.0
xmin = x[ 0]
xmax = x[-1]

xmins = xmin + 1.0 * L
xmaxs = xmax - 2.0 * L

_, xmin_idx = find_nearest(x, xmins)
_, xmax_idx = find_nearest(x, xmaxs)

time = t
t = t[time != 0]
Nt = t.size
dt = t[1] - t[0]
eta = eta[-Nt:, :]

height_threshold = 0.05 * H

xx        = np.zeros(xmax_idx - xmin_idx)
amplitude = np.zeros(xmax_idx - xmin_idx)
freqs     = np.zeros(xmax_idx - xmin_idx)

for i in range(xmax_idx - xmin_idx):
    xx[i] = x[i + xmin_idx]
    elevation = eta[:, i + xmin_idx]
    yf=fft(elevation, norm='forward')[:Nt//2] * 2.0
    freq = fftfreq(Nt, d=dt)[:Nt//2]
    
    peaks_index, properties = find_peaks(np.abs(yf), height=height_threshold)
    freqs[i] = freq[peaks_index[0]]
    amplitude[i] = properties['peak_heights'][0]

heights = amplitude * 2.0

# vegetation
Lv = 22.5
N = 625

# blade dimensions
l = 0.443       # length
b = 9.5e-3      # diameter

# layer thickness
d3 = 0
d2 = l
d1 = h - d2 - d3

# Dalrymple solution
x = np.linspace(0, 22.5, 100)

xmin_ax = -5.0 / Lv
xmax_ax = 25.0 / Lv
ymin_ax = 0.0
ymax_ax = 1.2

fig, ax = plt.subplots(1, 1, figsize=(10, 4))
plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.15)

ax.plot(xx / Lv, heights / H, '-k', label="numerical")
ax.set_xlim([xmin_ax, xmax_ax])
ax.set_ylim([ymin_ax, ymax_ax])
ax.set_xlabel("$x/L_v$", fontsize=12)
ax.set_ylabel("$H/H_0$", fontsize=12)

ax.plot(np.array([0.0, 0.0]), np.array([ymin_ax, ymax_ax]), '--k', alpha=0.5)
ax.plot(np.array([1.0, 1.0]), np.array([ymin_ax, ymax_ax]), '--k', alpha=0.5)
ax.annotate("Start of canopy", xy=(0.0, 1.1), xytext=(0.1, 1.1), arrowprops=dict(arrowstyle = "->"))
ax.annotate("End of canopy", xy=(1.0, 0.1), xytext=(0.7, 0.1), arrowprops=dict(arrowstyle = "->"))

lines = ['-b', '-r', '-g']
labels = ["Given ", "Calibrated ", "Calculated: "]

title = "Wave: $kh={:.3f}, kH={:0.3f}$; Grid: $N_x/L={:d},N_z/h={:d}$".format(k * h, k * H, Nx_L, Nz_h)
ax.set_title(title)

if len(sys.argv) > 3:
    # specify output figure name
    fname_fig = sys.argv[3]
    
    plt.savefig(fname_fig, bbox_inches='tight', dpi=300)
else:
    plt.show()

if len(sys.argv) > 4:
    output_file = sys.argv[4]
    num = np.zeros((xx.size, 2))
    num[:, 0] = xx / Lv
    num[:, 1] = heights / H
    np.savetxt(output_file, num, fmt='%.5e', delimiter=',')