import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])
    
def amplitude(x1, x2, z1, z2, N, file, x, z):
    xx = np.linspace(x1, x2, N)
    zz = np.linspace(z1, z2, N)

    steps = int(read(file, "Variables", "solution_time_steps"))

    umax = np.zeros(N)
    wmax = np.zeros(N)

    for i in range(steps):
        u = read(file, "Variables", "horizontal_velocity_{:d}".format(i))
        w = read(file, "Variables", "vertical_velocity_{:d}".format(i))

        fu = interpolate.interp2d(x, z, u, kind='linear')
        fw = interpolate.interp2d(x, z, w, kind='linear')

        uu = fu(xx, zz)[:, 0]
        ww = fw(xx, zz)[:, 0]

        umax = np.maximum(umax, uu)
        wmax = np.maximum(wmax, ww)
    return xx, zz, umax, wmax

input_file = sys.argv[1]
points     = np.loadtxt(sys.argv[2], skiprows=1)
N          = int(sys.argv[3])

num_points = points.shape[0]

# check if the file given exists
if not os.path.exists(input_file):
    print("The file " + input_file + " does not exist!\nPlease check again!\n")
    sys.exit()

# coordinates
with h5py.File(input_file, "r") as f:
    if "Variables/coordinates" in f:
        x = read(input_file, "Variables", "coordinates")[0, :, 0]
        z = read(input_file, "Variables", "coordinates")[:, 0, 1]
        x = x + (x[1] - x[0]) / 2.0
        z = z + (z[1] - z[0]) / 2.0

# wave
H   = read(input_file, "Wave", "height")[0]
h   = read(input_file, "Wave", "depth")[0]
L   = read(input_file, "Wave", "length")[0]
T   = read(input_file, "Wave", "period")[0]
k   = 2.0 * np.pi / L

# grid
Nx_L  = read(input_file, "Grid", "nodes_in_x")[0]
Nz_h  = read(input_file, "Grid", "nodes_in_z")[0]
Nx_L  = int(Nx_L)
Nz_h  = int(Nz_h)

# vegetation
Lv = 22.5
NN = 625
Cd = 1.75
Cm = 1.00

# blade dimensions
l = 0.443       # length
b = 9.5e-3      # diameter

# layer thickness
d3 = 0
d2 = l
d1 = h - d2 - d3

# nondimensionalization
kh = k * h
omega = 2 * np.pi / T
U0 = np.pi * H / T * np.cosh(kh) / np.sinh(kh)
W0 = H / 2 * omega

colors = ['navy', 'red', 'green', 'black', 'mediumpurple']
labels = ['ADV3', 'ADV5', 'ADV6', 'ADV7', 'ADV8']

fig, axes = plt.subplots(1, 2, figsize=(10, 4))
num = np.zeros((N, 11))
for i in range(num_points):
    x1 = points[i, 1]
    z1 = points[i, 2]
    x2 = points[i, 3]
    z2 = points[i, 4]

    X, Z, U, W = amplitude(x1, x2, z1, z2, N, input_file, x, z)
    axes[0].plot(U / U0, Z / h, '-', color=colors[i], label=labels[i])
    axes[1].plot(W / W0, Z / h, '-', color=colors[i])
    num[:, 0] = Z / h
    num[:, 2*i+1] = U / U0
    num[:, 2*i+2] = W / W0
    
# the interface
axes[0].plot(np.array([0, 1]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)
axes[1].plot(np.array([0, 1]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)

axes[0].plot(np.array([0, 1]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)
axes[1].plot(np.array([0, 1]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)

axes[0].set_xlim([ 0.0, 1.0])
axes[0].set_ylim([-1.0, 0.0])
axes[1].set_xlim([ 0.0, 1.0])
axes[1].set_ylim([-1.0, 0.0])
axes[0].set_xlabel("$U/U_0$")
axes[0].set_ylabel("$z/h$")
axes[1].set_xlabel("$W/W_0$")
axes[1].set_ylabel("$z/h$")
axes[0].grid()
axes[1].grid()
axes[0].legend()

title = "Wave: $kh={:.3f}, kH={:0.3f}$; Grid: $N_x/L={:d},N_z/h={:d}$".format(k * h, k * H, Nx_L, Nz_h)

if len(sys.argv) > 4:
    # specify output figure name
    fname_fig = sys.argv[4]

    # fname_fig = fname_fig + "_kh_{:.2f}_H_{:0.2f}_Nx_{:d}_Nz_{:d}".format(k * h, H, Nx_L, Nz_h) + ".png"
    plt.savefig(fname_fig, bbox_inches='tight', dpi=300)
else:
    plt.show()

if len(sys.argv) > 5:
    output_file = sys.argv[5]
    np.savetxt(output_file, num, fmt='%.5e', delimiter=',', header='z,ADV3_U,ADV3_W,ADV5_U,ADV5_W,ADV6_U,ADV6_W,ADV7_U,ADV7_W,ADV8_U,ADV8_W', comments='')