# change MODULE_PETSC_BASE_DIR to your local PETSC path
include ${MODULE_PETSC_BASE_DIR}/lib/petsc/conf/variables

BUILD    	 := ./build
OBJ_DIR  	 := $(BUILD)/objects
APP_DIR  	 := $(BUILD)/apps
INCLUDE  	 := -Iinclude -I${MODULE_PETSC_BASE_DIR}/include
SRC_OTHERS   := $(wildcard ./src/*.cc)

OBJS_OTHERS  := $(SRC_OTHERS:%.cc=$(OBJ_DIR)/%.o)
DEPENDENCIES := $(OBJS_APP:.o=.d) $(OBJS_OTHERS:.o=.d)

all: build wavetankperiodicbc wavetank bladeincurrent bladeinoscillatoryflow \
	 bladeinwaves bladeinuserdefinedflow waveoverrigidcanopy waveoverflexiblecanopy waveoverrigidbars

$(OBJ_DIR)/%.o: %.cc
	@mkdir -p $(@D)
	-${CXXLINKER} -c -Wall $< -MMD -o $@ ${INCLUDE}

wavetankperiodicbc: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/wavetankperiodicbc.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

wavetank: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/wavetank.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

bladeincurrent: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/bladeincurrent.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

bladeinoscillatoryflow: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/bladeinoscillatoryflow.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

bladeinwaves: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/bladeinwaves.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

bladeinuserdefinedflow: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/bladeinuserdefinedflow.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

waveoverrigidcanopy: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/waveoverrigidcanopy.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

waveoverflexiblecanopy: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/waveoverflexiblecanopy.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

waveoverrigidbars: $(OBJS_OTHERS) $(OBJ_DIR)/src/apps/waveoverrigidbars.o
	-${CXXLINKER} -o $(APP_DIR)/$@ $^ ${PETSC_LIB}

-include $(DEPENDENCIES)

build:
	@mkdir -p $(APP_DIR)
	@mkdir -p $(OBJ_DIR)

clean:
	-@rm -rvf $(OBJ_DIR)/**
	-@rm -rvf $(APP_DIR)/**

cleanall:
	-@rm -rvf $(BUILD)