import numpy as np
import os
import sys
import subprocess
from scipy.optimize import fsolve

# constants
g   = 9.81              # gravitational acceleration
tol = 1.0e-10           # numerical tolerance

# options
wave_regime_options    = ['Airy', 'SecondStokes']

# dispersion relation
def dispersion(k, *args):
    omega, h = args
    return omega * omega - k * g * np.tanh(k * h)

def read_input(options, prompt):
    while True:
        user_input = input(prompt)
        if not user_input in options:
            print("The input can only be ", end="")
            print(*options, sep=' or ')
            continue
        else:
            return user_input

if len(sys.argv) < 2:
    print("You should give the folder to be set up!\n")
    sys.exit()

# script path
script_path = os.path.abspath(os.path.dirname(__file__))

setup_path  = sys.argv[1]
input_path  = setup_path + "/input"
output_path = setup_path + "/output"

if not os.path.exists(setup_path):
    print("The setup path should be given!")
    sys.exit()

# check whether the input and output exist
# if not, create them
if not os.path.exists(input_path):
    os.makedirs(input_path)
if not os.path.exists(output_path):
    os.makedirs(output_path)

control_file = input_path + "/control.yaml"

# read wave characteristics
h = float(input("water depth (m): "))
T = float(input("wave period (s): "))
H = float(input("wave height (m): "))
# check wave characteristics
assert h > 0, "Water depth must be greater than 0"
assert T > 0, "Wave period must be greater than 0"
assert H > 0, "Wave height must be greater than 0"
wave_regime = read_input(wave_regime_options, "wave regime (Airy or SecondStokes): ")

# find wave length
omega = 2.0 * np.pi / T
k0 = 0.1
k = fsolve(dispersion, k0, (omega, h))[0]
L = 2.0 * np.pi / k
# check dispersion relation
assert np.abs(dispersion(k, omega, h)) < tol, "Please check the dispersion relation again!"

# read and write structure control info
# read control information for the structure
structureStartTime_by_period = float(input("start time of the structure simulation (by period): "))
structureEndTime_by_period = float(input("end time of the structure simulation (by period): "))
nStructureRamp = float(input("warmup time, usually 1 (by period): "))
structureDeltaT = float(input("update time step size for the structure (s): "))
structureWriteStartTime_by_period = float(input("start time to record the structure motion (by period): "))
structureWriteEndTime_by_period = float(input("end time to record the structure motion (by period): "))
structureWriteFPS_per_period = int(input("frequency of the record of the structure in one wave period: "))

# checkout inputs
assert structureStartTime_by_period >= 0, "The start time should be greater than or equal to zero"
assert structureEndTime_by_period   >= structureStartTime_by_period, "The end time should be greater than or equal to the start time"
assert nStructureRamp               >= 0, "The ramp time should be greater than or equal to zero"
assert structureStartTime_by_period >= structureStartTime_by_period and structureStartTime_by_period <= structureEndTime_by_period, "The record should start during the simulation"
assert structureEndTime_by_period   >= structureStartTime_by_period, "The record should end not before it starts"
assert structureDeltaT              >  0, "The update time step size should be greater than 0"
assert structureWriteFPS_per_period >= 0, "The record rate should be greater than or equal to zero"

structureStartTime          = structureStartTime_by_period * T
structureEndTime            = structureEndTime_by_period * T
structureRampTime           = nStructureRamp * T
structureWriteStartTime     = structureWriteStartTime_by_period * T
structureWriteEndTime       = structureWriteEndTime_by_period * T
structureWriteDeltaT        = T / structureWriteFPS_per_period

control_file = input_path + "/control.yaml"

with open(control_file, "w") as f:
    f.write("### structure\n")
    f.write("structureStartTime:\n")
    f.write("  - {:.5e}\n".format(structureStartTime))
    f.write("structureEndTime:\n")
    f.write("  - {:.5e}\n".format(structureEndTime))
    f.write("structureRampTime:\n")
    f.write("  - {:.5e}\n".format(structureRampTime))
    f.write("structureDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureDeltaT))
    f.write("structureWriteStartTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteStartTime))
    f.write("structureWriteEndTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteEndTime))
    f.write("structureWriteDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureWriteDeltaT))
    f.write("\n")

    f.write("### wave characteristics\n")
    f.write("waterdepth:\n")
    f.write("  - {:.5e}\n".format(h))
    f.write("waveperiod:\n")
    f.write("  - {:.5e}\n".format(T))
    f.write("wavelength:\n")
    f.write("  - {:.9e}\n".format(L))
    f.write("waveheight:\n")
    f.write("  - {:.5e}\n".format(H))
    f.write("waveregime:\n")
    if (wave_regime == 'Airy'):
        f.write("  - {:d}\n".format(1))
    if (wave_regime == 'SecondStokes'):
        f.write("  - {:d}\n".format(2))
    f.write("\n")

# check if the local trusssystem_setup.py exits, which if so should be executed
if os.path.isfile(setup_path + "/setup/trusssystem_setup.py"):
    cmd_str = "python3 {}/setup/trusssystem_setup.py".format(setup_path)
else:
    cmd_str = "cat {}/setup/trusssystem_config | python3 {}/trusssystem_fast_setup.py {}".format(setup_path, script_path, setup_path)
subprocess.run(cmd_str, shell=True)