import numpy as np
import os
import sys
import subprocess

if len(sys.argv) < 2:
    print("You should give the folder to be set up!\n")
    sys.exit()

# script path
script_path = os.path.abspath(os.path.dirname(__file__))

setup_path  = sys.argv[1]
input_path  = setup_path + "/input"
output_path = setup_path + "/output"

if not os.path.exists(setup_path):
    print("The setup path should be given!")
    sys.exit()

# check whether the input and output exist
# if not, create them
if not os.path.exists(input_path):
    os.makedirs(input_path)
if not os.path.exists(output_path):
    os.makedirs(output_path)

control_file = input_path + "/control.yaml"

# read flow information
U = float(input("flow speed (m/s): "))

# read and write structure control info
# read control information
structureStartTime           = float(input("start time of the structure simulation (s): "))
structureEndTime             = float(input("end time of the structure simulation (s): "))
structureRampTime            = float(input("ramp time of the structure simulation (s): "))
structureDeltaT              = float(input("update time step size for the structure (s): "))
structureWriteStartTime      = float(input("start time to record the structure motion (s): "))
structureWriteEndTime        = float(input("end time to record the structure motion (s): "))
structureWriteFPS_per_second = int(input("frequency of the record of the structure per second: "))

# check inputs
assert structureStartTime           >= 0, "The start time should be greater than or equal to zero"
assert structureEndTime             >= structureStartTime, "The end time should be greater than or equal to the start time"
assert structureRampTime            >= 0, "The ramp time should be greater than or equal to zero"
assert structureDeltaT              >  0, "The update time step size should be greater than 0"
assert structureWriteStartTime      >= structureStartTime and structureWriteStartTime <= structureEndTime, "The record should start during the simulation"
assert structureWriteEndTime        >= structureWriteStartTime, "The record should end not before it starts"
assert structureWriteFPS_per_second >= 0, "The record rate should be greater than or equal to zero"

structureWriteDeltaT = 1.0 / structureWriteFPS_per_second

control_file = input_path + "/control.yaml"

with open(control_file, "w") as f:
    f.write("### structure\n")
    f.write("structureStartTime:\n")
    f.write("  - {:.5e}\n".format(structureStartTime))
    f.write("structureEndTime:\n")
    f.write("  - {:.5e}\n".format(structureEndTime))
    f.write("structureRampTime:\n")
    f.write("  - {:.5e}\n".format(structureRampTime))
    f.write("structureDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureDeltaT))
    f.write("structureWriteStartTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteStartTime))
    f.write("structureWriteEndTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteEndTime))
    f.write("structureWriteDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureWriteDeltaT))
    f.write("\n")

    f.write("### current\n")
    f.write("U:\n")
    f.write("  - {:.5e}\n".format(U))
    f.write("\n")

# check if the local trusssystem_setup.py exits, which if so should be executed
if os.path.isfile(setup_path + "/setup/trusssystem_setup.py"):
    cmd_str = "python3 {}/setup/trusssystem_setup.py".format(setup_path)
else:
    cmd_str = "cat {}/setup/trusssystem_config | python3 {}/trusssystem_fast_setup.py {}".format(setup_path, script_path, setup_path)
subprocess.run(cmd_str, shell=True)