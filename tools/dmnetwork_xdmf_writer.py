import numpy as np
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

if len(sys.argv) < 2:
    print("You should give the input HDF5 file!\n")
    sys.exit()

input_file = sys.argv[1]

# check if the file given exists
if not os.path.exists(input_file):
    print("The file " + input_file + " does not exist!\nPlease check again!\n")
    sys.exit()

# The generated xdmf file is stored in the same directory
output_file = input_file.replace(".h5", ".xdmf")

# topology
with h5py.File(input_file, "r") as f:
    if "TrussSystem/topology" in f:
        topology = read(input_file, "TrussSystem", "topology")

t = read(input_file, "TrussSystem", "time")
steps = t.size
Nt = int(read(input_file, "TrussSystem", "number_truss"))
Nn = int(read(input_file, "TrussSystem", "number_node"))

with open(output_file, 'w') as f:
    f.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
    f.write("<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n")
    f.write("<Xdmf Version=\"3.0\">\n")
    f.write("\t<Domain>\n")
    f.write("\t\t<Grid Name=\"TrussSystem\" GridType=\"Collection\" CollectionType=\"Temporal\">\n")
    
    file = input_file.rsplit('/', 1)[-1]
    for i in range(steps):
        f.write("\n")
        f.write("\t\t\t<Grid Name=\"Truss System\">\n")
        f.write("\t\t\t\t<Time Value=\"{:f}\" />\n".format(t[i][0]))
        f.write("\t\t\t\t\t<Topology TopologyType=\"Polyline\" NodesPerElement=\"2\" NumberOfElements=\"{:d}\" NumberOfCells=\"0\">\n".format(Nt))
        f.write("\t\t\t\t\t\t<DataItem Format=\"HDF\" DataType=\"Int\" Dimensions=\"{:d} 2\">\n".format(Nt))
        f.write("\t\t\t\t\t\t\t{:s}:/TrussSystem/topology\n".format(file))
        f.write("\t\t\t\t\t\t</DataItem>\n")
        f.write("\t\t\t\t\t</Topology>\n")
        f.write("\t\t\t\t\t<Geometry GeometryType=\"XYZ\">\n")
        f.write("\t\t\t\t\t\t<DataItem Format=\"HDF\" Dimensions=\"{:d} 3\">\n".format(Nn))
        f.write("\t\t\t\t\t\t\t{:s}:/TrussSystem/XYZ_{:d}\n".format(file, i))
        f.write("\t\t\t\t\t\t</DataItem>\n")
        f.write("\t\t\t\t\t</Geometry>\n")
        f.write("\t\t\t</Grid>\n")
    f.write("\t\t</Grid>\n")
    f.write("\t</Domain>\n")
    f.write("</Xdmf>\n")