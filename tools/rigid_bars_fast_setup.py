import numpy as np
import sys

tol = 1.0e-12

if len(sys.argv) < 2:
    print("You should give the folder to be set up!\n")
    sys.exit()

setup_path = sys.argv[1]
input_path = setup_path + "/input"

sys.path.insert(0, setup_path + "/setup")

section_options     = ['rectangle', 'circle']

def read_input(options, prompt):
    while True:
        user_input = input(prompt)
        if not user_input in options:
            print("The input can only be ", end="")
            print(*options, sep=' or ')
            continue
        else:
            return user_input

# overall properties
damping = float(input("damping (kg/s): "))

# cross-section
cross_section = input("cross-section (circle or rectangle): ")
if cross_section == 'circle':
    b = float(input("cylinder diameter (m): "))
    assert b > 0, "The diameter should be greater than zero"
    d = b

if cross_section == 'rectangle':
    b = float(input("blade width (m): "))
    d = float(input("blade thickness (m): "))
    assert b > 0 and d > 0, "The width and thickness should be greater than zero"
    assert b >= d, "Generally the width should to greater than the thickness"

# blade length
l = float(input("structure length (m): "))

# number of segments on one bar
num_segments_in_one_bar = int(input("number of segments on one bar: "))

stems_per_meter_in_x = float(input("the stem density in x (1/m): "))
stems_per_meter_in_y = float(input("the stem density in y (1/m): "))

interval_in_x = 1.0 / stems_per_meter_in_x

## x-direction
### start position (m)
x0 = float(input("start of the patch (m): "))
### end position (m)
x1 = float(input("end of the patch (m): "))
### rows
rows_in_x = int(np.ceil((x1 - x0) / interval_in_x + 0.5))
### modify x1
x1 = x0 + interval_in_x * rows_in_x

## z-direction
### start position (m)
z0 = float(input("root of the stem (m): "))
### end position (m)
z1 = float(input("tip of the stem (m): "))

assert np.abs(np.abs(z0 - z1) - l) < tol, "Generally there should be no prestress"

## density (kg/m^3)
density = float(input("density (kg/m^3): "))

fluiddensity = float(input("fluid density (kg/m^3): "))
Cd = float(input("Cd: "))
Cm = float(input("Cm: "))
assert fluiddensity > 0, "The fluid density should be greater than zero"
assert Cd >= 0, "The drag coefficient should be greater than or equal to zero"
assert Cm >= 0, "The added mass coefficient should be greater than or equal to zero"

## other properties
### body mass and mass difference per unit length 
if cross_section == 'circle':
    mu = density * b * b * np.pi / 4
    md = (density - fluiddensity) * b * b * np.pi / 4
if cross_section == 'rectangle':
    mu = density * b * d
    md = (density - fluiddensity) * b * d
### added mass per unit length
ma = np.pi / 4 * fluiddensity * Cm * b * b

with open(setup_path + "/input/rigidbars.yaml", 'w') as f:
    f.write("### dimensions\n")
    f.write("length:\n")
    f.write("  - {:.5e}\n".format(l))
    f.write("width:\n")
    f.write("  - {:.5e}\n\n".format(b))

    f.write("### distribution\n")
    f.write("dx:    # distance between two adjacent bars\n")
    f.write("  - {:.5e}\n".format(interval_in_x))
    f.write("x0:    # where the canopy starts\n")
    f.write("  - {:.5e}\n".format(x0))
    f.write("x1:    # where the canopy ends\n")
    f.write("  - {:.5e}\n".format(x1))
    f.write("z0:    # where the bars hang\n")
    f.write("  - {:.5e}\n".format(z0))
    f.write("linedensity:\n")
    f.write("  - {:.5e}\n\n".format(stems_per_meter_in_y))
    
    f.write("### discretization\n")
    f.write("nsegments:     # number of segments in each bar\n")
    f.write("  - {:d}\n\n".format(num_segments_in_one_bar))

    f.write("### mass\n")
    f.write("mu:    # body mass per unit length\n")
    f.write("  - {:.5e}\n".format(mu))
    f.write("md:    # mass difference per unit length\n")
    f.write("  - {:.5e}\n".format(md))
    f.write("ma:    # added mass per unit length\n")
    f.write("  - {:.5e}\n".format(ma))

    f.write("### drag\n")
    f.write("Cd:    # drag coeffcient\n")
    f.write("  - {:.5e}\n".format(Cd))