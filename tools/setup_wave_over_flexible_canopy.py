import numpy as np
import os
import sys
import subprocess
from scipy.optimize import fsolve

# constants
g   = 9.81              # gravitational acceleration
tol = 1.0e-10           # numerical tolerance

# dispersion relation
def dispersion(k, *args):
    omega, h = args
    return omega * omega - k * g * np.tanh(k * h)

def read_input(options, prompt):
    while True:
        user_input = input(prompt)
        if not user_input in options:
            print("The input can only be ", end="")
            print(*options, sep=' or ')
            continue
        else:
            return user_input

if len(sys.argv) < 2:
    print("You should give the folder to be set up!\n")
    sys.exit()

# script path
script_path = os.path.abspath(os.path.dirname(__file__))

setup_path  = sys.argv[1]
input_path  = setup_path + "/input"
output_path = setup_path + "/output"

if not os.path.exists(setup_path):
    print("The setup path should be given!")
    sys.exit()

# check whether the input and output exist
# if not, create them
if not os.path.exists(input_path):
    os.makedirs(input_path)
if not os.path.exists(output_path):
    os.makedirs(output_path)

# read wave characteristics
h = float(input("water depth (m): "))
T = float(input("wave period (s): "))
H = float(input("wave height (m): "))
# check wave characteristics
assert h > 0, "Water depth must be greater than 0"
assert T > 0, "Wave period must be greater than 0"
assert H > 0, "Wave height must be greater than 0"

# find wave length
omega = 2.0 * np.pi / T
k0 = 0.1
k = fsolve(dispersion, k0, (omega, h))[0]
L = 2.0 * np.pi / k
# check dispersion relation
assert np.abs(dispersion(k, omega, h)) < tol, "Please check the dispersion relation again!"

# read control information for the flow
flowStartTime_by_period             = float(input("start time of the flow simulation (by period): "))
flowEndTime_by_period               = float(input("end time of the flow simulation (by period): "))
CFL                                 = float(input("CFL based on the wave celerity (can be greater than 1): "))
flowWriteStartTime_by_period        = float(input("start time to record the velocities and pressure (by period): "))
flowWriteEndTime_by_period          = float(input("end time to record the velocities and pressure (by period): "))
flowSurfaceWriteStartTime_by_period = float(input("start time to record the surface elevation (by period): "))
flowSurfaceWriteEndTime_by_period   = float(input("end time to record the surface elevation (by period): "))
flowWriteFPS_per_period             = int(input("frequency of the record of the flow in one wave period: "))

assert flowStartTime_by_period              >= 0, "The start time of flow simulation should be greater than or equal to 0"
assert flowEndTime_by_period                >= flowStartTime_by_period, "The end time of flow simulation should be greater than or equal to the start time"
assert CFL                                  >  0, "CFL should greater than 0"
assert flowWriteStartTime_by_period         >= flowStartTime_by_period and flowWriteStartTime_by_period <= flowEndTime_by_period, "The record of the flow should start during the simulation"
assert flowWriteEndTime_by_period           >= flowWriteStartTime_by_period, "The record of the flow should end after it starts"
assert flowSurfaceWriteStartTime_by_period  >= flowStartTime_by_period and flowSurfaceWriteStartTime_by_period <= flowEndTime_by_period, "The record of the surface elevation should start during the simulation"
assert flowSurfaceWriteEndTime_by_period    >= flowSurfaceWriteStartTime_by_period, "The record of the surface elevation should after it starts"
assert flowWriteFPS_per_period              >= 0, "The record rate should be greater than or equal to zero"

flowStartTime               = flowStartTime_by_period * T
flowEndTime                 = flowEndTime_by_period * T
flowWriteStartTime          = flowWriteStartTime_by_period * T
flowWriteEndTime            = flowWriteEndTime_by_period * T
flowSurfaceWriteStartTime   = flowSurfaceWriteStartTime_by_period * T
flowSurfaceWriteEndTime     = flowSurfaceWriteEndTime_by_period * T
flowWriteDeltaT             = T / flowWriteFPS_per_period

### wave tank
# length of domain of interest (m)
distance = float(input("length of domain of interest (m): "))

### wave tank setup
# gap between the wave making zone and the domain of interest (by wave length)
gap_after_in   = int(input("gap between the wave making zone and the domain of interest (by wave length): "))
# gap between the wave absorbing zone and the domain of interest (by wave length)
gap_before_out = int(input("gap between the wave absorbing zone and the domain of interest (by wave length): "))

# start of the interest domain, usually zero (m)
xstart = float(input("start of the interest domain, usually zero (m): "))

# wave making coefficients
gamma = float(input("gamma: "))
nexp  = float(input("exponential: "))
nRamp = int(input("warmup time (by period): "))
nIn   = int(input("length of wave making zone (by wave length): "))
nOut  = int(input("length of wave absorbing zone (by wave length): "))

### grid resolution
Nx_per_L = int(input("cell number within one wave length: "))
Nz_per_h = int(input("cell number over the depth: "))

# computational domain
distance_by_wave_length = int(np.ceil(distance / L))
xmin = (xstart - nIn - gap_after_in) * L
xmax = (xstart + distance_by_wave_length + gap_before_out + nOut) * L
zmin = -h
zmax = 0.0

print("The computation domain is [{:.2f}, {:.2f}]x[{:.2f}, {:.2f}] (m*m)".format(xmin, xmax, zmin, zmax))

# grid resolution
Nx = Nx_per_L * int(nIn + gap_after_in + distance_by_wave_length + gap_before_out + nOut)
Nz = Nz_per_h

print("The overall resolution is {:d}x{:d}, or {:d}x{:d} within one wave legnth.".format(Nx, Nz, Nx_per_L, Nz_per_h))

# read control information for the structure
structureStartTime_by_period = float(input("start time of the structure simulation (by period): "))
structureEndTime_by_period = float(input("end time of the structure simulation (by period): "))
nStructureRamp = float(input("warmup time, usually 1 (by period): "))
structureDeltaT = float(input("update time step size for the structure (s): "))
structureWriteStartTime_by_period = float(input("start time to record the structure motion (by period): "))
structureWriteEndTime_by_period = float(input("end time to record the structure motion (by period): "))
structureWriteFPS_per_period = int(input("frequency of the record of the structure in one wave period: "))

# checkout inputs
assert structureStartTime_by_period >= 0, "The start time should be greater than or equal to zero"
assert structureEndTime_by_period   >= structureStartTime_by_period, "The end time should be greater than or equal to the start time"
assert nStructureRamp               >= 0, "The ramp time should be greater than or equal to zero"
assert structureStartTime_by_period >= structureStartTime_by_period and structureStartTime_by_period <= structureEndTime_by_period, "The record should start during the simulation"
assert structureEndTime_by_period   >= structureStartTime_by_period, "The record should end not before it starts"
assert structureDeltaT              >  0, "The update time step size should be greater than 0"
assert structureWriteFPS_per_period >= 0, "The record rate should be greater than or equal to zero"

structureStartTime          = structureStartTime_by_period * T
structureEndTime            = structureEndTime_by_period * T
structureRampTime           = nStructureRamp * T
structureWriteStartTime     = structureWriteStartTime_by_period * T
structureWriteEndTime       = structureWriteEndTime_by_period * T
structureWriteDeltaT        = T / structureWriteFPS_per_period
writeDeltaT = np.minimum(flowWriteDeltaT, structureWriteDeltaT)

control_file = input_path + "/control.yaml"
with open(control_file, "w") as f:
    f.write("### flow\n")
    f.write("flowStartTime:\n")
    f.write("  - {:.5e}\n".format(flowStartTime))
    f.write("flowEndTime:\n")
    f.write("  - {:.5e}\n".format(flowEndTime))
    f.write("flowWriteStartTime:\n")
    f.write("  - {:.5e}\n".format(flowWriteStartTime))
    f.write("flowWriteEndTime:\n")
    f.write("  - {:.5e}\n".format(flowWriteEndTime))
    f.write("flowSurfaceWriteStartTime:\n")
    f.write("  - {:.5e}\n".format(flowSurfaceWriteStartTime))
    f.write("flowSurfaceWriteEndTime:\n")
    f.write("  - {:.5e}\n".format(flowSurfaceWriteEndTime))
    f.write("CFL:\n")
    f.write("  - {:.5e}\n".format(CFL))
    f.write("flowWriteDeltaT:\n")
    f.write("  - {:.5e}\n".format(flowWriteDeltaT))
    f.write("\n")

    f.write("### structure\n")
    f.write("structureStartTime:\n")
    f.write("  - {:.5e}\n".format(structureStartTime))
    f.write("structureEndTime:\n")
    f.write("  - {:.5e}\n".format(structureEndTime))
    f.write("structureRampTime:\n")
    f.write("  - {:.5e}\n".format(structureRampTime))
    f.write("structureDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureDeltaT))
    f.write("structureWriteStartTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteStartTime))
    f.write("structureWriteEndTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteEndTime))
    f.write("structureWriteDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureWriteDeltaT))
    f.write("\n")

    f.write("### output\n")
    f.write("writeDeltaT:\n")
    f.write("  - {:.5e}\n".format(writeDeltaT))
    f.write("\n")

    f.write("### wave characteristics\n")
    f.write("waterdepth:\n")
    f.write("  - {:.5e}\n".format(h))
    f.write("waveperiod:\n")
    f.write("  - {:.5e}\n".format(T))
    f.write("wavelength:\n")
    f.write("  - {:.9e}\n".format(L))
    f.write("waveheight:\n")
    f.write("  - {:.5e}\n".format(H))
    f.write("\n")

    f.write("### grid resolutions\n")
    f.write("Nx:\n")
    f.write("  - {:d}\n".format(Nx))
    f.write("Nz:\n")
    f.write("  - {:d}\n".format(Nz))
    f.write("\n")

    f.write("### computational domain\n")
    f.write("xmin:\n")
    f.write("  - {:.5e}\n".format(xmin))
    f.write("xmax:\n")
    f.write("  - {:.5e}\n".format(xmax))

    f.write("### wave generation\n")
    f.write("gamma:\n")
    f.write("  - {:.5e}\n".format(gamma))
    f.write("nRamp:\n")
    f.write("  - {:.5e}\n".format(nRamp))
    f.write("nexp:\n")
    f.write("  - {:.5e}\n".format(nexp))
    f.write("nIn:\n")
    f.write("  - {:.5e}\n".format(nIn))
    f.write("nOut:\n")
    f.write("  - {:.5e}\n".format(nOut))

# check if the local trusssystem_setup.py exits, which if so should be executed
if os.path.isfile(setup_path + "/setup/trusssystem_setup.py"):
    cmd_str = "python3 {}/setup/trusssystem_setup.py".format(setup_path)
else:
    cmd_str = "cat {}/setup/trusssystem_config | python3 {}/trusssystem_fast_setup.py {}".format(setup_path, script_path, setup_path)
subprocess.run(cmd_str, shell=True)