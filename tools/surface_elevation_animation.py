import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.animation as animation
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx

file = sys.argv[1]

# check if the file given exists
if not os.path.exists(file):
    print("The file " + file + " does not exist!\nPlease check again!\n")
    sys.exit()

t     = read(file, "Variables", "time_elevation")
x     = read(file, "Variables", "coordinates")[-1, :, 0]
z     = read(file, "Variables", "coordinates")[:, -1, 1]
eta   = read(file, "Variables", "elevation")
steps = t.size
t     = np.reshape(t, (steps, 1))

# wave
H     = read(file, "Wave", "height")[0]
h     = read(file, "Wave", "depth" )[0]
L     = read(file, "Wave", "length")[0]
T     = read(file, "Wave", "period")[0]

print("wave height: {:f}, wave period: {:f}, wave length: {:f}, water depth: {:f}".format(H, T, L, h))

# grid
Nx_L  = read(file, "Grid", "nodes_in_x")[0]
Nz_h  = read(file, "Grid", "nodes_in_z")[0]
Nx_L  = int(Nx_L)
Nz_h  = int(Nz_h)

Nx = x.size
dx = x[1] - x[0]
x = x + dx / 2.0
Nx_per_L = round(Nx_L / round((x[-1] - x[0] + dx) / L))
Nz_per_h = int(Nz_h)

fig, ax = plt.subplots(figsize=(20, 5))
xdata, ydata = [], []
ln1, = plt.plot([], [], 'b-', label="numerical")
ln2, = plt.plot([], [], 'k-', alpha=0.5, label="analytical")
time_template = "$N_x/L={:d}, N_z/h={:d}, t/T={:0.2f}$"
time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes, fontsize=14)

xmax = x[-1] / L
xmin = x[ 0] / L

def init():
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(-1.5, 2.5)
    # helping lines
    ax.plot(np.array([xmin, xmax], dtype=float), np.array([ 1,  1], dtype=float), 'k--', lw=0.5, alpha=0.5)
    ax.plot(np.array([xmin, xmax], dtype=float), np.array([-1, -1], dtype=float), 'k--', lw=0.5, alpha=0.5)

    ax.legend()
    ax.set_xlabel("$x/L$", fontsize=14)
    ax.set_ylabel("$\eta/a$", fontsize=14)
    return ln1, ln2, time_text

def update(frame):
    xdata = x / L
    ydata = eta[frame, :] / H * 2.0
    ln1.set_data(xdata, ydata)
    tt = t[frame][0]
    ydata = np.cos(2.0 * np.pi * tt / T - 2.0 * np.pi / L * x)
    ln2.set_data(xdata, ydata)
    time_text.set_text(time_template.format(Nx_per_L, Nz_per_h, tt / T))
    return ln1, ln2, time_text

ani = animation.FuncAnimation(fig, update, steps - 1,
        interval=50.0, init_func=init, blit=True)

if len(sys.argv) > 2:
    # specify output figure name
    fname_fig = sys.argv[2]

    # specify frame rate
    npers = 3
    if len(sys.argv) > 3:
        npers = int(sys.argv[3])

    writergif = animation.PillowWriter(fps=npers)
    ani.save(fname_fig, writer=writergif)
else:
    plt.show()