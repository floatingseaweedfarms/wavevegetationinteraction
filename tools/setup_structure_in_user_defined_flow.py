import numpy as np
import os
import sys
import subprocess

if len(sys.argv) < 2:
    print("You should give the folder to be set up!\n")
    sys.exit()

# script path
script_path = os.path.abspath(os.path.dirname(__file__))

setup_path  = sys.argv[1]
input_path  = setup_path + "/input"
output_path = setup_path + "/output"

if not os.path.exists(setup_path):
    print("The setup path should be given!")
    sys.exit()

# check whether the input and output exist
# if not, create them
if not os.path.exists(input_path):
    os.makedirs(input_path)
if not os.path.exists(output_path):
    os.makedirs(output_path)

control_file = input_path + "/control.yaml"

# reference time
T = float(input("reference time (s), usually 1 second for non-oscillatory flow and the period for oscillatory flow: "))
assert T >  0, "The reference time should be greater than zero"

# read and write structure control info
# read control information for the structure
structureStartTime_by_reference_time = float(input("start time of the structure simulation (by period): "))
structureEndTime_by_reference_time = float(input("end time of the structure simulation (by period): "))
nStructureRamp = float(input("warmup time, usually 1 (by period): "))
structureDeltaT = float(input("update time step size for the structure (s): "))
structureWriteStartTime_by_reference_time = float(input("start time to record the structure motion (by period): "))
structureWriteEndTime_by_reference_time = float(input("end time to record the structure motion (by period): "))
structureWriteFPS_by_reference_time = int(input("frequency of the record of the structure in one wave period: "))

# checkout inputs
assert structureStartTime_by_reference_time >= 0, "The start time should be greater than or equal to zero"
assert structureEndTime_by_reference_time   >= structureStartTime_by_reference_time, "The end time should be greater than or equal to the start time"
assert nStructureRamp               >= 0, "The ramp time should be greater than or equal to zero"
assert structureStartTime_by_reference_time >= structureStartTime_by_reference_time and structureStartTime_by_reference_time <= structureEndTime_by_reference_time, "The record should start during the simulation"
assert structureEndTime_by_reference_time   >= structureStartTime_by_reference_time, "The record should end not before it starts"
assert structureDeltaT              >  0, "The update time step size should be greater than 0"
assert structureWriteFPS_by_reference_time >= 0, "The record rate should be greater than or equal to zero"

structureStartTime          = structureStartTime_by_reference_time * T
structureEndTime            = structureEndTime_by_reference_time * T
structureRampTime           = nStructureRamp * T
structureWriteStartTime     = structureWriteStartTime_by_reference_time * T
structureWriteEndTime       = structureWriteEndTime_by_reference_time * T
structureWriteDeltaT        = T / structureWriteFPS_by_reference_time

control_file = input_path + "/control.yaml"

with open(control_file, "w") as f:
    f.write("### structure\n")
    f.write("structureStartTime:\n")
    f.write("  - {:.5e}\n".format(structureStartTime))
    f.write("structureEndTime:\n")
    f.write("  - {:.5e}\n".format(structureEndTime))
    f.write("structureRampTime:\n")
    f.write("  - {:.5e}\n".format(structureRampTime))
    f.write("structureDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureDeltaT))
    f.write("structureWriteStartTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteStartTime))
    f.write("structureWriteEndTime:\n")
    f.write("  - {:.5e}\n".format(structureWriteEndTime))
    f.write("structureWriteDeltaT:\n")
    f.write("  - {:.5e}\n".format(structureWriteDeltaT))
    f.write("\n")

    f.write("referenceTime:\n")
    f.write("  - {:.5e}\n".format(T))
    f.write("\n")

# check if the local trusssystem_setup.py exits, which if so should be executed
if os.path.isfile(setup_path + "/setup/trusssystem_setup.py"):
    cmd_str = "python3 {}/setup/trusssystem_setup.py".format(setup_path)
else:
    cmd_str = "cat {}/setup/trusssystem_config | python3 {}/trusssystem_fast_setup.py {}".format(setup_path, script_path, setup_path)
subprocess.run(cmd_str, shell=True)