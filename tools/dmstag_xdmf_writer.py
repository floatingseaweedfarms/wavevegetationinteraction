import numpy as np
import h5py
import sys
import os

def read(hdf5file, group, dataset):
    with h5py.File(hdf5file, "r") as f:
        return np.array(f[group][dataset][()])

if len(sys.argv) < 2:
    print("You should give the input HDF5 file!\n")
    sys.exit()

input_file = sys.argv[1]

# check if the file given exists
if not os.path.exists(input_file):
    print("The file " + input_file + " does not exist!\nPlease check again!\n")
    sys.exit()

# The generated xdmf file is stored in the same directory
output_file = input_file.replace(".h5", ".xdmf")

# coordinates
with h5py.File(input_file, "r") as f:
    if "Variables/coordinates" in f:
        x = read(input_file, "Variables", "coordinates")[0, :, 0]
        z = read(input_file, "Variables", "coordinates")[:, 0, 1]
        x = x + (x[1] - x[0]) / 2.0
        z = z + (z[1] - z[0]) / 2.0

# write into the hdf5 file
with h5py.File(input_file, "a") as f:
    # if "Variables/coordinates" in f:
    #     del f["Variables"]["coordinates"]
    if "XZ" not in f:
        grpXZ = f.create_group("XZ")
        grpXZ.create_dataset("X", data=x)
        grpXZ.create_dataset("Z", data=z)

steps = int(read(input_file, "Variables", "solution_time_steps"))
x = read(input_file, "XZ", "X")
z = read(input_file, "XZ", "Z")

# time
t = np.zeros(steps)
for i in range(steps):
    t[i] = read(input_file, "Variables", "time_solution_{:d}".format(i))

Nx    = x.size
Nz    = z.size

with open(output_file, 'w') as f:
    f.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
    f.write("<Xdmf Version=\"3.0\">\n")
    f.write("\t<Domain>\n")
    f.write("\t\t<Grid Name=\"LinearWaveTank\" GridType=\"Collection\" CollectionType=\"Temporal\">\n")
    
    file = input_file.rsplit('/', 1)[-1]
    for i in range(steps):
        f.write("\n")
        f.write("\t\t\t<Grid Name=\"Wave Tank\" GridType=\"Uniform\">\n")
        f.write("\t\t\t\t<Time Value=\"{:f}\" />\n".format(t[i]))
        f.write("\t\t\t\t\t<Topology TopologyType=\"2DRECTMesh\" NumberOfElements=\"{:d} {:d}\" />\n".format(Nz, Nx))
        f.write("\t\t\t\t\t<Geometry GeometryType=\"VXVY\">\n")
        f.write("\t\t\t\t\t\t<DataItem Name=\"X\" Dimensions=\"{:d}\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n".format(Nx))
        f.write("\t\t\t\t\t\t\t{:s}:/XZ/X\n".format(file))
        f.write("\t\t\t\t\t\t</DataItem>\n")
        f.write("\t\t\t\t\t\t<DataItem Name=\"Z\" Dimensions=\"{:d}\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n".format(Nz))
        f.write("\t\t\t\t\t\t\t{:s}:/XZ/Z\n".format(file))
        f.write("\t\t\t\t\t\t</DataItem>\n")
        f.write("\t\t\t\t\t</Geometry>\n")
        f.write("\t\t\t\t\t<Attribute Name=\"U\" AttributeType=\"Scalar\" Center=\"Node\">\n")
        f.write("\t\t\t\t\t\t<DataItem Dimensions=\"{:d} {:d}\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n".format(Nz, Nx))
        f.write("\t\t\t\t\t\t\t{:s}:/Variables/horizontal_velocity_{:d}\n".format(file, i))
        f.write("\t\t\t\t\t\t</DataItem>\n")
        f.write("\t\t\t\t\t</Attribute>\n")
        f.write("\t\t\t\t\t<Attribute Name=\"V\" AttributeType=\"Scalar\" Center=\"Node\">\n")
        f.write("\t\t\t\t\t\t<DataItem Dimensions=\"{:d} {:d}\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n".format(Nz, Nx))
        f.write("\t\t\t\t\t\t\t{:s}:/Variables/vertical_velocity_{:d}\n".format(file, i))
        f.write("\t\t\t\t\t\t</DataItem>\n")
        f.write("\t\t\t\t\t</Attribute>\n")
        f.write("\t\t\t\t\t<Attribute Name=\"P\" AttributeType=\"Scalar\" Center=\"Node\">\n")
        f.write("\t\t\t\t\t\t<DataItem Dimensions=\"{:d} {:d}\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n".format(Nz, Nx))
        f.write("\t\t\t\t\t\t\t{:s}:/Variables/pressure_{:d}\n".format(file, i))
        f.write("\t\t\t\t\t\t</DataItem>\n")
        f.write("\t\t\t\t\t</Attribute>\n")
        f.write("\t\t\t</Grid>\n")
    f.write("\t\t</Grid>\n")
    f.write("\t</Domain>\n")
    f.write("</Xdmf>\n")
