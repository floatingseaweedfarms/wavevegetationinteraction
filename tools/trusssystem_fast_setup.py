import numpy as np
import os
import sys

tol = 1.0e-12

if len(sys.argv) < 2:
    print("You should give the folder to be set up!\n")
    sys.exit()

setup_path = sys.argv[1]
input_path = setup_path + "/input"

sys.path.insert(0, setup_path + "/setup")
from init_velo import *

class section:
    def __init__(self, a, b, kind):
        kind_list = ["circle", "rectangle"]
        assert kind in kind_list, "The shape should either CIRCLE or RECTANGLE!"
        assert a > 0 and b > 0, "The dimensions should be positive!"
        
        if (kind == "circle"):
            assert a == b, "For circle, please give two identical values!"
            self.a = a
            self.b = b
        if (kind == "rectangle"):
            self.a = a
            self.b = b
        self.kind = kind

class line:
    def __init__(self, section, starting_point, ending_point, N, tol=5):
        x1, y1, z1 = starting_point
        x2, y2, z2 = ending_point
        assert x1 == x2 or y1 == y2 or z1 == z2, "The line should be parallel to one coordinate plane!"
        
        start   = np.array([x1, y1, z1])
        end     = np.array([x2, y2, z2])
        l       = np.linalg.norm(end - start)
        self.dl = l / N
        
        if section.kind == "circle":
            self.b = section.a
            self.d = section.b
            self.A = np.pi / 4 * self.b * self.d
            self.I = np.pi / 4 * self.b * self.b * self.b * self.b
        if section.kind == "rectangle":
            self.b = section.a
            self.d = section.b
            self.A = self.b * self.d
            self.I = 1 / 12 * self.b * self.d * self.d * self.d
        self.N = N
        x = np.linspace(x1, x2, N + 1)
        y = np.linspace(y1, y2, N + 1)
        z = np.linspace(z1, z2, N + 1)

        self.xyz   = []
        self.fixed = []
        self.load  = np.zeros((N + 1, 3), dtype=float)
        for i in range(N + 1):
            self.xyz.append((round(x[i], tol), round(y[i], tol), round(z[i], tol)))

    def set_properties(self, stiff, E, density, sym):
        self.stiff = stiff
        self.EA    = E * self.A
        self.EI    = E * self.I
        self.mass  = density * self.A
        self.sym   = sym
    
    def set_init_velocity(self, func):
        self.uvw = []
        for i in range(self.N + 1):
            self.uvw.append(func(self.xyz[i]))
    
    def set_fixed_vertices(self, vertices):
        for i in range(self.N + 1):
            if i in vertices:
                self.fixed.append(1)
            else:
                self.fixed.append(0)
    
    def set_pre_applied_load(self, pairs):
        for i, load in pairs:
            self.load[i] = load
    
    def set_in_fluid(self, fluid_density):
        self.in_fluid = True
        self.fluid_density = fluid_density
    
    def set_properties_in_fluid(self, Cd, Cm):
        self.displacedmass = self.fluid_density * self.A
        self.Cd = Cd
        self.Cm = Cm

arrangement_options = ['single', 'patch']
section_options     = ['rectangle', 'circle']
stiff_options       = ['True', 'False']
isinfluid_options   = ['True', 'False']

def read_input(options, prompt):
    while True:
        user_input = input(prompt)
        if not user_input in options:
            print("The input can only be ", end="")
            print(*options, sep=' or ')
            continue
        else:
            return user_input

# overall properties
damping = float(input("damping (kg/s): "))

# cross-section
cross_section = input("cross-section (circle or rectangle): ")
if cross_section == 'circle':
    b = float(input("cylinder diameter (m): "))
    d = float(input("cylinder diameter (m): "))
    assert b > 0, "The diameter should be greater than zero"
    d = b

if cross_section == 'rectangle':
    b = float(input("blade width (m): "))
    d = float(input("blade thickness (m): "))
    assert b > 0 and d > 0, "The width and thickness should be greater than zero"
    assert b > d, "Generally the width should to greater than the thickness"

crosssection = section(b, d, cross_section)

# blade length
l = float(input("structure length (m): "))

# number of truss elements on one stem
trusses_in_one_stem = int(input("number of truss elements on one stem: "))

# structure collections
lines = []

# arrangement
arrangement = read_input(arrangement_options, "structure arragement (single or patch): ")
print(arrangement)
if arrangement == 'single':
    # start
    x0 = float(input("x of start point (m): "))
    y0 = float(input("y of start point (m): "))
    z0 = float(input("z of start point (m): "))
    # end
    x1 = float(input("x of end point (m): "))
    y1 = float(input("y of end point (m): "))
    z1 = float(input("z of end point (m): "))

    ll = np.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0) + (z1 - z0) * (z1 - z0))
    assert np.abs(ll - l) < tol, "Generally there should be no prestress"

    structure = line(crosssection, (x0, y0, z0), (x1, y1, z1), trusses_in_one_stem)
    lines.append(structure)

    stems_per_meter = 1

if arrangement == 'patch':
    '''
    ## if the canopy is uniformly distributed,
    stems_per_meter_in_x = sqrt(stems_per_meter_square)
    stems_per_meter_in_y = sqrt(stems_per_meter_square)
    ## if the canopy is not uniformly distributed,
    stems_per_meter_in_x and stems_per_meter_in_y are not equal
    '''

    stems_per_meter_in_x = float(input("the stem density in x (1/m): "))
    stems_per_meter_in_y = float(input("the stem density in y (1/m): "))

    interval_in_x = 1 / stems_per_meter_in_x

    ## x-direction
    ### start position (m)
    x0 = float(input("start of the patch (m): "))
    ### end position (m)
    x1 = float(input("end of the patch (m): "))
    ### rows
    rows_in_x = int(np.ceil((x1 - x0) / interval_in_x + 0.5))
    ### modify x1
    x1 = x0 + interval_in_x * rows_in_x
    ### locations for each stem
    xx = np.linspace(x0, x1, rows_in_x + 1)

    ## z-direction
    ### start position (m)
    z0 = float(input("root of the stem (m): "))
    ### end position (m)
    z1 = float(input("tip of the stem (m): "))

    assert np.abs(np.abs(z0 - z1) - l) < tol, "Generally there should be no prestress"

    for i in range(rows_in_x):
        lines.append(line(crosssection, (xx[i], 0, z0), (xx[i], 0, z1), trusses_in_one_stem))

# set properties
## whether the structure is stiff, 0 for not and 1 for yes
stiff = read_input(stiff_options, "the structure is stiff (True or False): ")
## Young's modulus (Pa)
E = float(input("Young's modulus (Pa): "))
## density (kg/m^3)
density = float(input("density (kg/m^3): "))
## symmetry of the ghost truss
sysx = int(input("symmetry of the ghost truss in x-direction: (-1 or 1): "))
sysy = int(input("symmetry of the ghost truss in y-direction: (-1 or 1): "))
sysz = int(input("symmetry of the ghost truss in z-direction: (-1 or 1): "))
sys = (sysx, sysy, sysz)

for subline in lines:
    # the root is usually fixed
    subline.set_fixed_vertices([0])
    subline.set_properties(stiff, E, density, sys)
    subline.set_init_velocity(velo_func)
    subline.set_pre_applied_load([])

Nsubnet = len(lines)
sharedvertices = []
nedges = []
for i in range(Nsubnet):
    nedges.append(lines[i].N)
    for j in range(i+1, Nsubnet):
        duplicates = set(lines[i].xyz).intersection(lines[j].xyz)
        for duplicate in duplicates:
            index1 = lines[i].xyz.index(duplicate)
            index2 = lines[j].xyz.index(duplicate)
        if (len(duplicates)):
            sharedvertices.append((i, j, index1, index2))
nsharedvertices = len(sharedvertices)

for i in range(nsharedvertices):
    info = sharedvertices[i]
    v1 = lines[info[0]].fixed[info[2]]
    v2 = lines[info[1]].fixed[info[3]]
    if (v1 == 1 or v2 == 1):
        lines[info[0]].fixed[info[2]] = 1
        lines[info[1]].fixed[info[3]] = 1

with open(setup_path + "/input/trusssystem.yaml", 'w') as f:
    f.write("### topology\n")
    f.write("Nsubnet:      # number of subnetworks\n")
    f.write("  - {:d}\n".format(len(lines)))
    f.write("nedge:        # number of edges in each subnetwork\n")
    for subline in lines:
        f.write("  - {:d}\n".format(subline.N))
    f.write("nsharedvtx:   # number of shared vertices\n")
    f.write("  - {:d}\n".format(nsharedvertices))
    f.write("sharedvtx:    # info of shared vertices\n")
    for svtx in sharedvertices:
        f.write(" - {:d},{:d},{:d},{:d}\n".format(svtx[0], svtx[1], svtx[2], svtx[3]))
    f.write("\n")
    
    f.write("### node status (fixed or not) and node positions\n")
    f.write("nodeinit:\n")
    for b in range(Nsubnet):
        subline = lines[b]
        for v in range(subline.N + 1):
            f.write("  - {:d},{:d},{:d},{:.5e},{:.5e},{:.5e},{:.5e},{:.5e},{:.5e}\n".format(b, v, subline.fixed[v], subline.xyz[v][0], subline.xyz[v][1], subline.xyz[v][2], subline.uvw[v][0], subline.uvw[v][1], subline.uvw[v][2]))
        f.write("\n")
    f.write("### system global properties\n")
    f.write("damping:\n")
    f.write("  - {:.5e}\n".format(damping))
    if arrangement == 'single':
        stems_per_meter_in_y = 1
    f.write("linedensity:\n")
    f.write("  - {:.5e}\n".format(stems_per_meter_in_y))
    f.write("\n")   

    f.write("### pre-applied load on the node\n")
    f.write("# branch id,node id,fx,fy,fz\n")
    f.write("preappliedloadonnode:\n")
    for b in range(Nsubnet):
        subline =  lines[b]
        for v in range(subline.N + 1):
            f.write("  - {:d},{:d},{:.5e},{:.5e},{:.5e}\n".format(b, v, subline.load[v][0], subline.load[v][1], subline.load[v][2]))
        f.write("\n")
    
    f.write("### branch global properties\n")
    f.write("# branch id, stiff(1) or not(0), and its symmetry about the three planes\n")
    f.write("branchgps:\n")
    for b in range(Nsubnet):
        subline =  lines[b]
        if subline.stiff == 'True':
            stiff = 1
        if subline.stiff == 'False':
            stiff = 0
        f.write("  - {:d},{:d},{:d},{:d},{:d}\n\n".format(b, stiff, subline.sym[0], subline.sym[1], subline.sym[2]))
    f.write("### truss properties\n")
    f.write("trusspts:\n")
    f.write("# branch id,truss id,width,thickness,mass per unit length,EA,EI\n")
    for b in range(Nsubnet):
        subline =  lines[b]
        for e in range(subline.N):
            f.write("  - {:d},{:d},{:.5e},{:.5e},{:.5e},{:.5e},{:.5e}\n".format(b, e, subline.b, subline.d, subline.mass, subline.EA, subline.EI))
        f.write("\n")

# whether the structure is in fluid
isinfluid = read_input(isinfluid_options, "the structure is submerged in fluid (True or False): ")
if isinfluid == 'True':
    fluiddensity = float(input("fluid density (kg/m^3): "))
    Cd = float(input("Cd: "))
    Cm = float(input("Cm: "))
    assert fluiddensity > 0, "The fluid density should be greater than zero"
    assert Cd >= 0, "The drag coefficient should be greater than or equal to zero"
    assert Cm >= 0, "The added mass coefficient should be greater than or equal to zero"

    for subline in lines:
        subline.set_in_fluid(fluiddensity)
        subline.set_properties_in_fluid(Cd, Cm)
    
    with open(setup_path + "/input/trusssysteminfluid.yaml", 'w') as f:
        f.write("### fluid properties\n")
        f.write("fluidensity:      # kg/m^3\n")
        f.write("  - {:.2f}\n".format(fluiddensity))
        f.write("### truss properties in fluid\n")
        f.write("trussptsinfluid:\n")
        for b in range(Nsubnet):
            subline =  lines[b]
            for e in range(subline.N):
                displaced_mass_per_unit_length = subline.fluid_density * subline.A
                addedmass_per_unit_length = np.pi / 4 * subline.fluid_density * subline.Cm * subline.b * subline.b
                f.write("  - {:d},{:d},{:.5e},{:.5e},{:.2e},{:.2e}\n".format(b, e, displaced_mass_per_unit_length, addedmass_per_unit_length, subline.Cd, subline.Cm))
            f.write("\n")