# Wave-Vegetation Interaction

This coupled wave-vegetation model aims to tackle the interaction between linear waves and flexible vegetation with large deflections. Note that here only linear wave is considered. The vegetation model is based on the explicit truss spring model, which is capable of resolving the motion of flexible vegetation. The coupling between flow and vegetation is implemented using a diffused immersed boundary method.

## Project Structure

```shell
project
├── examples
│   ├── BladeInOscillatoryFlow
│   ├── WaveInducedMotion
│   ├── WavesOverFlexibleCanopy
│   └── WavesOverRigidCanopy
├── include
│   ├── *.h
├── LICENSE
├── makefile
├── README.md
├── src
│   ├── AiryWave.cc
│   ├── apps
│   │   ├── *.cc
│   ├── *.cc
└── tools
    ├── *.py
```

**Notes**:
- The `build` directory is generated automatically when `make` is executed for the first time.
- Only the following files and directories are tracked: `README.md`, `makefile`, `.gitignore`, `include`, `src`, `examples`, and `tools`.
  
## Makefile Instructions

```shell
make {build|clean|cleanall|wavetankperiodicbc|wavetank|bladeincurrent|bladeinoscillatoryflow|bladeinwaves|bladeinuserdefinedflow|waveoverrigidcanopy|waveoverflexiblecanopy}
```

### Common Commands
- `make`: Builds all applications.
- `make build`: Creates the `build/apps` and `build/objects` directories.
- `make clean`: Removes `build/objects` and `build/apps`.
- `make cleanall`: Removes the entire `build` directory.

### Application Commands

* `make wavetankperiodicbc`: Builds the appplication for a wave tank with periodic boundary conditions at two ends

* `make wavetank`: Builds the appplication for a wave tank with relaxation forcing zones at two ends

* `make bladeincurrent`: Builds the appplication for an elastic blade in current (one-way model)

* `make bladeinoscillatoryflow`: Builds the appplication for an elastic blade in sinusoidal oscillatory flow (one-way model)

* `make bladeinwaves`: Builds the appplication for an elastic blade in waves (one-way model), now only supports Airy waves and second-order Stokes waves

* `make bladeinuserdefinedflow`: Builds the appplication for an elastic blade in any user-defined flow (one-way model)

* `make waveoverrigidcanopy`: Builds the appplication for linear waves over rigid canopies (one-way model)

* `make waveoverflexiblecanopy`: Builds the appplication for linear waves over flexible canopies (two-way model)

## Usage

Run the application using: `app -input_path <target_path>`, where `<target_path>` contains the input and output files.

## Tools
Several Python scripts are included for utility purposes:
- `surface_elevation_animation.py`: Animates free surface elevation in a wave tank.
- `dmnetwork_xdmf_writer.py`: Generates an XDMF file from an HDF5 file for the truss-spring model, viewable in ParaView.
- `dmstag_xdmf_writer.py`: Generates an XDMF file from an HDF5 file for the linearized flow model, viewable in ParaView.

### Example: 
```shell
python dmnetwork_xdmf_writer.py vegetation.h5
```
This creates an XDMF file (`vegetation.xdmf`) in the same directory as `vegetation.h5`.

Several other Python scripts are provided to generate input files for different applications, such as:
- `setup_structure_in_current.py`: Prepares input files for the `bladeincurrent` app. For instance:
```shell
python setup_structure_in_current.py target_path
```

Other scripts include:
- `setup_structure_in_sinusoidal_oscillatory_flow.py`
- `setup_structure_in_user_defined_flow.py`
- `setup_structure_in_waves.py`
- `setup_wave_over_flexible_canopy.py`
- `setup_wave_over_rigid_canopy.py`

These follow a similar structure and provide prompts for input.

## Examples

Example applications are available in the `examples` directory. Each includes setup files that can either be provided or generated via bash scripts. Feel free to modify parameters and explore the results. More details can be found in the respective example directories.

## Dependencies

- **[PETSc](https://petsc.org/release/)** (3.16.4)
  
  For [DTU gbar](https://www.gbar.dtu.dk/) users, load the following module: 
  ```shell
  module load petsc/3.16.4-mar-2022-gcc-10.3.0-openblas-0.3.19-non-complex-slepc-elemental-superlu
  ```
  This loads `PETSc`, as well as other required libraries like [HDF5](https://www.hdfgroup.org/solutions/hdf5/). You can then compile all apps.

  For other users, refer to the [PETSc installation guide](https://petsc.org/release/install/install/). Ensure `superlu` and `HDF5` are installed.

- **[Python](https://www.python.org/)** (3.9.10)
  Ensure packages like `NumPy`, `SciPy`, and `Matplotlib` are installed.

## Citation

The code is also preserved on [DTU Data](https://doi.org/10.11583/DTU.24533098.v2), where citation styles can be generated.

## Publications

1. [Wei, Z., Shao, Y., Kristiansen, T., & Kristiansen, D. A Fully Explicit Wave-Vegetation Interaction Model and Its Application in Waves over a Floating Seaweed Farm. In Proceedings of 39th International Workshop on Water Waves and Floating Bodies IWWWFB.](https://orbit.dtu.dk/en/publications/a-fully-explicit-wave-vegetation-interaction-model-and-its-applic)

2. [Wei, Z., Shao, Y., Kristiansen, T., & Kristiansen, D. (2024). An efficient numerical solver for highly compliant slender structures in waves: Application to marine vegetation. Journal of Fluids and Structures, 129, Article 104170.](https://doi.org/10.1016/j.jfluidstructs.2024.104170)

3. [Wei, Z., Weiss, M., Kristiansen, T., Kristiansen, D., & Shao, Y. (2024). Wave attenuation by cultivated seaweeds: A linearized analytical model. Coastal Engineering, 104642.](https://doi.org/10.1016/j.coastaleng.2024.104642)

## References

1. [Kristiansen, T. & Faltinsen, O. M. Experimental and numerical study of an aquaculture net cage with floater in waves and current. Journal of Fluids and Structures 54, 1–26 (2015).](https://doi.org/10.1016/j.jfluidstructs.2014.08.015)

2. [Chen, H. & Zou, Q.-P. Eulerian–Lagrangian flow-vegetation interaction model using immersed boundary method and OpenFOAM. Advances in Water Resources 126, 176–192 (2019).](https://doi.org/10.1016/j.advwatres.2019.02.006)

3. [Jacobsen, N. G., Fuhrman, D. R. & Fredsøe, J. A wave generation toolbox for the open-source CFD library: OpenFoam®. International Journal for Numerical Methods in Fluids 70, 1073–1088 (2012).](https://doi.org/10.1002/fld.2726)