#include "DMNetworkHDF5Writer.h"
#include "Truss.h"

constexpr PetscInt DIMS = 3;

DMNetworkHDF5Writer::DMNetworkHDF5Writer(const PetscViewer &viewer,
        const PetscInt num_node,
        const PetscInt num_truss,
        const PetscInt num_subnet,
        const TrussSystem &ts,
        const DM &dm) : ptrviewer(&viewer), 
        ptrTS(&ts),
        nnode(num_node), ntruss(num_truss),
        nsubnet(num_subnet), 
        index(0),
        flag(PETSC_FALSE)
{
    /* topology */
    Vec vecTopol;
    PetscScalar *arrTopol;
    PetscCalloc1(num_truss * 2, &arrTopol);

    Truss *truss;
    Node  *node;
    PetscInt eStart, eEnd;
    const PetscInt *cone;

    DMNetworkGetEdgeRange(dm, &eStart, &eEnd);
    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(dm, e, 0, NULL, (void**)&truss, NULL);
        DMNetworkGetConnectedVertices(dm, e, &cone);
        
        DMNetworkGetComponent(dm, cone[0], 0, NULL, (void**)&node, NULL);
        arrTopol[(e - eStart) * 2    ] = node->id;
        DMNetworkGetComponent(dm, cone[1], 0, NULL, (void**)&node, NULL);
        arrTopol[(e - eStart) * 2 + 1] = node->id;
    }
    VecCreateSeqWithArray(PETSC_COMM_SELF, 1, 2 * num_truss, arrTopol, &vecTopol);
    VecAssemblyBegin(vecTopol);
    VecAssemblyEnd(vecTopol);

    /* number of nodes and trusses */
    Vec val;
    PetscScalar tmp = 1.0;
    VecCreateSeqWithArray(PETSC_COMM_SELF, 1, 1, &tmp, &val);
    VecAssemblyBegin(val);
    VecAssemblyEnd(val);

    PetscViewerHDF5PushGroup(*ptrviewer, groupname);

    PetscObjectSetName((PetscObject) vecTopol, "topology");
    VecView(vecTopol, *ptrviewer);

    VecSet(val, num_node);
    PetscObjectSetName((PetscObject) val, "number_node");
    VecView(val, *ptrviewer);

    VecSet(val, num_truss);
    PetscObjectSetName((PetscObject) val, "number_truss");
    VecView(val, *ptrviewer);

    VecSet(val, num_subnet);
    PetscObjectSetName((PetscObject) val, "number_subnet");
    VecView(val, *ptrviewer);

    PetscViewerHDF5PopGroup(*ptrviewer);

    PetscFree(arrTopol);
    VecDestroy(&vecTopol);
    VecDestroy(&val);
}

PetscErrorCode DMNetworkHDF5Writer::pushGroup()
{
    PetscViewerHDF5PushGroup(*ptrviewer, groupname);
    flag = PETSC_TRUE;

    return 0;
}

PetscErrorCode DMNetworkHDF5Writer::popGroup()
{
    if (flag)
    {
        PetscViewerHDF5PopGroup(*ptrviewer);
        flag = PETSC_FALSE;

        return 0;
    }
    else
    {
        PetscPrintf(PETSC_COMM_WORLD, "The group TrussSystemSystem has not been pushed yet!");
        exit(EXIT_FAILURE);
    }
}

PetscErrorCode DMNetworkHDF5Writer::writeToHDF5(const DM &dm, const PetscScalar T)
{
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "The group TrussSystemSystem has not been pushed yet!");
        exit(EXIT_FAILURE);
    }

    Vec vecPosition, vecTension, vecLoad;
    PetscScalar *arrPosition, *arrTension, *arrLoad;
    PetscCalloc1(DIMS * nnode, &arrPosition);
    PetscCalloc1(ntruss, &arrTension);
    PetscCalloc1(DIMS * nsubnet, &arrLoad);

    Node  *node;
    Truss *truss;
    PetscInt vStart, vEnd, eStart, eEnd;
    DMNetworkGetVertexRange(dm, &vStart, &vEnd);
    DMNetworkGetEdgeRange(dm, &eStart, &eEnd);

    for (PetscInt v = vStart; v != vEnd; ++v)
    {
        DMNetworkGetComponent(dm, v, 0, NULL, (void**)&node, NULL);
        arrPosition[(v - vStart) * DIMS    ] = node->x(0);
        arrPosition[(v - vStart) * DIMS + 1] = node->x(2);
        arrPosition[(v - vStart) * DIMS + 2] = node->x(1);
    }

    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(dm, e, 0, NULL, (void**)&truss, NULL);
        arrTension[e - eStart] = truss->T;
    }

    for (PetscInt b = 0; b != nsubnet; ++b)
    {
        arrLoad[b * DIMS    ] = (ptrTS->subnets() + b)->load(0);
        arrLoad[b * DIMS + 1] = (ptrTS->subnets() + b)->load(1);
        arrLoad[b * DIMS + 2] = (ptrTS->subnets() + b)->load(2);
    }

    VecCreateSeqWithArray(PETSC_COMM_SELF, 1, DIMS * nnode, arrPosition, &vecPosition);
    VecAssemblyBegin(vecPosition);
    VecAssemblyEnd(vecPosition);

    VecCreateSeqWithArray(PETSC_COMM_SELF, 1, ntruss, arrTension, &vecTension);
    VecAssemblyBegin(vecTension);
    VecAssemblyEnd(vecTension);

    VecCreateSeqWithArray(PETSC_COMM_SELF, 1, DIMS * nsubnet, arrLoad, &vecLoad);
    VecAssemblyBegin(vecLoad);
    VecAssemblyEnd(vecLoad);

    Vec val;
    VecCreateSeqWithArray(PETSC_COMM_SELF, 1, 1, &T, &val);
    VecAssemblyBegin(val);
    VecAssemblyEnd(val);

    PetscObjectSetName((PetscObject) val, "time");
    PetscViewerHDF5PushTimestepping(*ptrviewer);
    VecView(val, *ptrviewer);
    PetscViewerHDF5IncrementTimestep(*ptrviewer);
    PetscViewerHDF5PopTimestepping(*ptrviewer);

    std::string name = std::string("XYZ_") + std::to_string(index);
    PetscObjectSetName((PetscObject) vecPosition, name.c_str());
    VecView(vecPosition, *ptrviewer);

    name = std::string("Tension_") + std::to_string(index);
    PetscObjectSetName((PetscObject) vecTension, name.c_str());
    VecView(vecTension, *ptrviewer);

    name = std::string("Load_") + std::to_string(index);
    PetscObjectSetName((PetscObject) vecLoad, name.c_str());
    VecView(vecLoad, *ptrviewer);

    ++index;

    PetscFree(arrPosition);
    VecDestroy(&vecPosition);
    VecDestroy(&vecTension);
    VecDestroy(&vecLoad);
    VecDestroy(&val);

    return 0;
}