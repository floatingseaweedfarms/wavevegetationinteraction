#include <petscviewerhdf5.h>
#include "DMStagHDF5Writer.h"

DMStagHDF5Writer::DMStagHDF5Writer(const PetscViewer &viewer,
        const DM &dm)
{
    ptrViewer = &viewer;
    ptrDM     = &dm;
    index_ele = 0;
    index_sol = 0;
    flag      = PETSC_FALSE;

    DMCreateMatrix(*ptrDM, &pU);
    DMCreateMatrix(*ptrDM, &pW);
    MatSetOption(pU, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
    MatSetOption(pW, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);

    PetscInt N[2];
    PetscInt startx, starty, nx, ny, ex, ey;
    DMStagStencil row, col[2];
    PetscScalar val[2];

    DMStagGetGlobalSizes(dm, &N[0], &N[1], NULL);
    
    DMStagGetCorners(dm, &startx, &starty, NULL, &nx, &ny, NULL, NULL, NULL, NULL);
    for (ey = starty; ey != starty + ny; ++ey)
    {
        for (ex = startx; ex != startx + nx; ++ex)
        {
            // horizontal velocity at loc center
            row.i = ex; row.j = ey; row.loc = DMSTAG_ELEMENT; row.c = 0;
            col[0].i = ex; col[0].j = ey; col[0].loc = DMSTAG_LEFT ; col[0].c = 0; val[0] = 0.5;
            col[1].i = ex; col[1].j = ey; col[1].loc = DMSTAG_RIGHT; col[1].c = 0; val[1] = 0.5;
            DMStagMatSetValuesStencil(*ptrDM, pU, 1, &row, 2, col, val, INSERT_VALUES);
            // vertical velocity at loc center
            row.i = ex; row.j = ey; row.loc = DMSTAG_ELEMENT; row.c = 0;
            col[0].i = ex; col[0].j = ey; col[0].loc = DMSTAG_UP  ; col[0].c = 0; val[0] = 0.5;
            col[1].i = ex; col[1].j = ey; col[1].loc = DMSTAG_DOWN; col[1].c = 0; val[1] = 0.5;
            DMStagMatSetValuesStencil(*ptrDM, pW, 1, &row, 2, col, val, INSERT_VALUES);
        }
    }
    MatAssemblyBegin(pU, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(pU, MAT_FINAL_ASSEMBLY);
    MatAssemblyBegin(pW, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(pW, MAT_FINAL_ASSEMBLY);

    PetscInt *ix;
    PetscMalloc1(N[0], &ix);

    for (PetscInt i = 0; i != N[0]; ++i) ix[i] = (N[1] - 1) * N[0] + i;
    ISCreateGeneral(PETSC_COMM_WORLD, N[0], ix, PETSC_COPY_VALUES, &topEta);
    PetscFree(ix);
}

PetscErrorCode DMStagHDF5Writer::pushGroup()
{
    PetscViewerHDF5PushGroup(*ptrViewer, "Variables");
    flag      = PETSC_TRUE;

    // coordinates
    DM dmda;
    Vec vec, subvec, xy;
    DMCreateGlobalVector(*ptrDM, &vec);
    DMStagVecSplitToDMDA(*ptrDM, vec, DMSTAG_ELEMENT, 0, &dmda, &subvec);

    DMGetCoordinates(dmda, &xy);
    PetscObjectSetName((PetscObject) xy, "coordinates");
    VecView(xy, *ptrViewer);

    // dimensions
    PetscInt N[2];
    Vec val;

    DMStagGetGlobalSizes(*ptrDM, &N[0], &N[1], NULL);

    VecCreate(PETSC_COMM_WORLD, &val);
    VecSetSizes(val, PETSC_DECIDE, 1);
    VecSetFromOptions(val);

    PetscViewerHDF5PushGroup(*ptrViewer, "/Grid");

    VecSet(val, N[0]);
    PetscObjectSetName((PetscObject) val, "nodes_in_x");
    VecView(val, *ptrViewer);
    VecSet(val, N[1]);
    PetscObjectSetName((PetscObject) val, "nodes_in_z");
    VecView(val, *ptrViewer);

    PetscViewerHDF5PopGroup(*ptrViewer);

    VecDestroy(&val);

    VecDestroy(&vec);
    VecDestroy(&subvec);
    DMDestroy(&dmda);

    return 0;
}

PetscErrorCode DMStagHDF5Writer::popGroup()
{
    if (flag)
    {
        PetscViewerHDF5PopGroup(*ptrViewer);
        flag      = PETSC_FALSE;
    }
    else
    {
        PetscPrintf(PETSC_COMM_WORLD, "The group Variables has not been pushed yet!\n");
        exit(EXIT_FAILURE);
    }

    return 0;
}

PetscErrorCode DMStagHDF5Writer::writeSolutionToHDF5(const Vec &sol, const PetscScalar T)
{
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "The group Variables has not been pushed yet!\n");
        exit(EXIT_FAILURE);
    }

    DM dmda;
    Vec val, vec, subvec;
    std::string name;

    VecCreate(PETSC_COMM_WORLD, &val);
    VecSetSizes(val, PETSC_DECIDE, 1);
    VecSetFromOptions(val);

    // time
    name = std::string("time_solution_") + std::to_string(index_sol);
    VecSet(val, T);
    PetscObjectSetName((PetscObject) val, name.c_str());
    VecView(val, *ptrViewer);

    DMCreateGlobalVector(*ptrDM, &vec);

    // horizontal velocity
    name = std::string("horizontal_velocity_") + std::to_string(index_sol);
    MatMult(pU, sol, vec);
    DMStagVecSplitToDMDA(*ptrDM, vec, DMSTAG_ELEMENT, 0, &dmda, &subvec);
    PetscObjectSetName((PetscObject) subvec, name.c_str());
    VecView(subvec, *ptrViewer);
    VecDestroy(&subvec);
    DMDestroy(&dmda);

    // vertical velocity
    name = std::string("vertical_velocity_") + std::to_string(index_sol);
    MatMult(pW, sol, vec);
    DMStagVecSplitToDMDA(*ptrDM, vec, DMSTAG_ELEMENT, 0, &dmda, &subvec);
    PetscObjectSetName((PetscObject) subvec, name.c_str());
    VecView(subvec, *ptrViewer);
    VecDestroy(&subvec);
    DMDestroy(&dmda);
    
    // pressure
    name = std::string("pressure_") + std::to_string(index_sol);
    DMStagVecSplitToDMDA(*ptrDM, sol, DMSTAG_ELEMENT, 0, &dmda, &subvec);
    PetscObjectSetName((PetscObject) subvec, name.c_str());
    VecView(subvec, *ptrViewer);
    VecDestroy(&subvec);
    DMDestroy(&dmda);
    
    ++index_sol;

    // solution time steps
    VecSet(val, index_sol);
    PetscObjectSetName((PetscObject) val, "solution_time_steps");
    VecView(val, *ptrViewer);

    VecDestroy(&val);
    VecDestroy(&vec);
    VecDestroy(&subvec);
    DMDestroy(&dmda);

    return 0;
}

PetscErrorCode DMStagHDF5Writer::writeElevationToHDF5(const Vec &eta, const PetscScalar T)
{
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "The group Variables has not been pushed yet!\n");
        exit(EXIT_FAILURE);
    }

    DM dmda;
    Vec val, vec, subvec;
    std::string name;

    VecCreate(PETSC_COMM_WORLD, &val);
    VecSetSizes(val, PETSC_DECIDE, 1);
    VecSetFromOptions(val);

    PetscViewerHDF5PushTimestepping(*ptrViewer);
    VecSet(val, T);
    PetscObjectSetName((PetscObject) val, "time_elevation");
    VecView(val, *ptrViewer);

    DMStagVecSplitToDMDA(*ptrDM, eta, DMSTAG_ELEMENT, 0, &dmda, &vec);
    VecGetSubVector(vec, topEta, &subvec);
    PetscObjectSetName((PetscObject) subvec, "elevation");
    VecView(subvec, *ptrViewer);
    VecRestoreSubVector(subvec, topEta, &subvec);

    PetscViewerHDF5IncrementTimestep(*ptrViewer);
    PetscViewerHDF5PopTimestepping(*ptrViewer);

    VecDestroy(&val);
    VecDestroy(&vec);
    VecDestroy(&subvec);
    DMDestroy(&dmda);

    return 0;
}

PetscErrorCode DMStagHDF5Writer::writeWaveToHDF5(const PotentialWaves &wave)
{
    Vec val;
    VecCreate(PETSC_COMM_WORLD, &val);
    VecSetSizes(val, PETSC_DECIDE, 1);
    VecSetFromOptions(val);

    PetscViewerHDF5PushGroup(*ptrViewer, "/Wave");

    PetscScalar T, L, h, H;
    T = wave.period();
    L = wave.length();
    h = wave.depth();
    H = wave.height();

    VecSet(val, T);
    PetscObjectSetName((PetscObject) val, "period");
    VecView(val, *ptrViewer);
    VecSet(val, L);
    PetscObjectSetName((PetscObject) val, "length");
    VecView(val, *ptrViewer);
    VecSet(val, h);
    PetscObjectSetName((PetscObject) val, "depth");
    VecView(val, *ptrViewer);
    VecSet(val, H);
    PetscObjectSetName((PetscObject) val, "height");
    VecView(val, *ptrViewer);

    PetscViewerHDF5PopGroup(*ptrViewer);

    VecDestroy(&val);

    return 0;
}

PetscErrorCode DMStagHDF5Writer::destroy()
{
    if (flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "The group Variables has not been popped yet!\nPlease call popGroup before destroying it!\n");
        exit(EXIT_FAILURE);
    }

    ptrViewer = NULL;
    ptrDM     = NULL;
    MatDestroy(&pU);
    MatDestroy(&pW);
    ISDestroy(&topEta);

    return 0;
}