#include <petscdmnetwork.h>
#include "Truss.h"
#include "HydroLoad.h"
#include "HydroLoadOnTrussSystem.h"

PetscErrorCode HydroLoadOnTrussSystem::load(const PetscScalar T,
    const PetscScalar factor)
{
    HydroLoad loadcomputer = HydroLoad();

    PetscInt eStart, eEnd;
    Truss *truss;
    Vector3s X, U, A, F;
    Matrix3s du;

    DMNetworkGetEdgeRange(*ptrDM, &eStart, &eEnd);

    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(*ptrDM, e, 0, NULL, (void**)&truss, NULL);

        X  =  truss->x;
        U  << ptrFlow->u   (X(0), X(2), T), 0.0, ptrFlow->w (X(0), X(2), T);
        A  << ptrFlow->ax  (X(0), X(2), T), 0.0, ptrFlow->az(X(0), X(2), T);
        du << ptrFlow->dudx(X(0), X(2), T), 0.0, ptrFlow->dudz(X(0), X(2), T),
              0.0, 0.0, 0.0,
              ptrFlow->dwdx(X(0), X(2), T), 0.0, ptrFlow->dwdz(X(0), X(2), T);
        
        loadcomputer.load(F, *truss, X, U, A, du);
        truss->F = factor * F;
    }

    return 0;
}