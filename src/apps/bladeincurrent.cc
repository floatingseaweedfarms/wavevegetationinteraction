#include <petsc.h>
#include <petscdmnetwork.h>
#include <petscviewerhdf5.h>
#include <string>
#include "TrussSystem.h"
#include "DMNetworkHDF5Writer.h"
#include "UniformCurrent.h"
#include "HydroLoadOnTrussSystem.h"

const char help[] = "Flexible blade in uniform and constant current\n\n";

int main(int argc, char **argv)
{
    PetscErrorCode  ierr;
    char            input_path[PETSC_MAX_PATH_LEN] = "";
    PetscOptions    options;
    PetscBool       flag;

    ierr = PetscInitialize(&argc, &argv, (char*)0, help); if (ierr) return ierr;

    // read path of input database from command line
    flag = PETSC_FALSE;
    PetscOptionsGetString(NULL, NULL, "-input_path", input_path, sizeof(input_path), &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "Please specify the INPUT path with option [-input_path]!\n");
        PetscEnd();
    }
    std::string path = input_path;

    /* -------------------------- variables -------------------------- */
    // control info
    PetscScalar structureStartTime, structureEndTime;
    PetscScalar structureWarmupTime;
    PetscScalar structureDeltaT;
    PetscScalar structureWriteStartTime, structureWriteEndTime;

    // truss system
    DM                  nw;
    TrussSystem         *ts = NULL;
    PetscViewer         hdf5viewer_struct;
    DMNetworkHDF5Writer *struct_writer = NULL;

    // flow
    PetscScalar UU;             // current speed
    Flows       *flow = NULL;

    // hydro load
    HydroLoadOnTrussSystem *hydroloadonts = NULL;

    // output
    PetscScalar writeDeltaT;
    PetscInt    recordLeap = 1;

    /* -------------------------- configurations -------------------------- */
    // read inputs for the model setup
    PetscOptionsCreate(&options);
    flag = PETSC_FALSE;
    std::string input_control = path + "/input/control.yaml";
    PetscOptionsInsertFileYAML(PETSC_COMM_WORLD, options, input_control.c_str(), flag);
    if (flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "\nPlease make sure the input yaml file exists and in correct format!\n\n");
        PetscEnd();
    }

    // control
    PetscOptionsGetScalar(options, NULL, "-structureStartTime", &structureStartTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-structureEndTime", &structureEndTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-structureRampTime", &structureWarmupTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-structureWriteStartTime", &structureWriteStartTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-structureWriteEndTime", &structureWriteEndTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-structureDeltaT", &structureDeltaT, NULL);

    // output
    PetscOptionsGetScalar(options, NULL, "-structureWriteDeltaT", &writeDeltaT, NULL);

    // current
    PetscOptionsGetScalar(options, NULL, "-U", &UU, NULL);

    PetscOptionsDestroy(&options);

    // flow model
    flow = new UniformCurrent(UU);
    hydroloadonts = new HydroLoadOnTrussSystem(nw, *flow);

    // read input to setup the truss system
    DMNetworkCreate(PETSC_COMM_WORLD, &nw);
    ts = new TrussSystem(nw);

    PetscOptionsCreate(&options);
    flag = PETSC_FALSE;
    std::string input_ts = path + "/input/trusssystem.yaml";
    PetscOptionsInsertFileYAML(PETSC_COMM_WORLD, options, input_ts.c_str(), flag);
    if (flag) 
    {
        PetscPrintf(PETSC_COMM_WORLD, "\nPlease make sure the input yaml file exists and in correct format!\n\n");
        PetscEnd();
    }
    ts->setupFromOptions(options);
    ts->initialize();

    PetscOptionsDestroy(&options);

    // read inputs for structures in fluid
    PetscOptionsCreate(&options);
    flag = PETSC_FALSE;
    std::string input_tsinfluid = path + "/input/trusssysteminfluid.yaml";
    PetscOptionsInsertFileYAML(PETSC_COMM_WORLD, options, input_tsinfluid.c_str(), flag);
    if (flag) 
    {
        PetscPrintf(PETSC_COMM_WORLD, "\nPlease make sure the input yaml file exists and in correct format!\n\n");
        PetscEnd();
    }

    ts->setupInFluidFromOptions(options);
    PetscOptionsDestroy(&options);

    ts->initialize();

    // output
    PetscInt nnode   = ts->node_num();
    PetscInt nedge   = ts->truss_num();
    PetscInt nsubnet = ts->subnet_num();

    std::string output_struct = path + "/output/structure.h5";
    PetscViewerHDF5Open(PETSC_COMM_WORLD, output_struct.c_str(), FILE_MODE_WRITE, &hdf5viewer_struct);
    struct_writer = new DMNetworkHDF5Writer(hdf5viewer_struct, nnode, nedge, nsubnet, *ts, nw);

    // update time step size
    recordLeap = PetscInt(writeDeltaT / structureDeltaT + 0.5);
    structureDeltaT = writeDeltaT / recordLeap;

    PetscPrintf(PETSC_COMM_WORLD, "The structure simulation starts from %.3f s, ends at %.3f s, and is updated with an interval of %.2e s.\n", structureStartTime, structureEndTime, structureDeltaT);
    PetscPrintf(PETSC_COMM_WORLD, "The record of structure motion starts from %.3f s, ends at %.3f s,\n", structureWriteStartTime, structureWriteEndTime);
    PetscPrintf(PETSC_COMM_WORLD, "and is done with an interval of %.2e s or %d times per second, or every %d structure update steps\n\n", writeDeltaT, PetscInt(1.0 / writeDeltaT + 0.5), recordLeap);

    /* ------------------------------------- solver ------------------------------------- */
    // basic variables
    PetscScalar T0      = structureStartTime;
    PetscScalar endTime = structureEndTime;

    PetscScalar T = T0;
    PetscInt index = 0;
    PetscScalar strain_max = 0.0; // no pre-tension
    PetscScalar factor = 1.0;

    struct_writer->pushGroup();
    struct_writer->writeToHDF5(nw, T);
    

    while(T <= endTime)
    {
        if (index % recordLeap == 0)
        {
            strain_max = ts->maxstrain();
            PetscPrintf(PETSC_COMM_WORLD, "i: %10d; T (s): %6.3f; max strain: %6.5e\n", index, T, strain_max);
            if (T >= structureWriteStartTime && T <= structureWriteEndTime) struct_writer->writeToHDF5(nw, T);
        }

        factor = ((T - structureStartTime) > structureWarmupTime ? 1.0 : (T - structureStartTime) / structureWarmupTime);
        hydroloadonts->load(T, factor);
        ts->update_RK2nd(structureDeltaT, 1);
        hydroloadonts->load(T, factor);
        ts->update_RK2nd(structureDeltaT, 2);
        T += structureDeltaT;
        ++index;
    }
    struct_writer->popGroup();

    ts->destroy();
    delete flow;
    delete hydroloadonts;
    delete ts;
    delete struct_writer;
    DMDestroy(&nw);
    PetscViewerDestroy(&hdf5viewer_struct);

    ierr = PetscFinalize();
    return ierr;
}