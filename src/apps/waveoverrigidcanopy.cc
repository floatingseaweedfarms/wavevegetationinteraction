#include <petsc.h>
#include <petscdmnetwork.h>
#include <petscviewerhdf5.h>
#include <string>
#include "Constants.h"
#include "TrussSystem.h"
#include "AiryWave.h"
#include "HydroLoadOnTrussSystem.h"
#include "ImmersedBoundaryMethod.h"
#include "Projection.h"
#include "WaveMaker.h"
#include "WaveTank.h"
#include "DMStagHDF5Writer.h"
#include "DMNetworkHDF5Writer.h"

const char help[] = "Wave over rigid canopy\n\n";

int main(int argc, char **argv)
{
    PetscErrorCode  ierr;
    char            input_path[PETSC_MAX_PATH_LEN] = "";
    PetscOptions    options;
    PetscBool       flag;

    ierr = PetscInitialize(&argc, &argv, (char*)0, help); if (ierr) return ierr;

    // read path of input database from command line
    flag = PETSC_FALSE;
    PetscOptionsGetString(NULL, NULL, "-input_path", input_path, sizeof(input_path), &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "Please specify the INPUT path with option [-input_path]!\n");
        PetscEnd();
    }
    std::string path = input_path;

    /* -------------------------- variables -------------------------- */
    // control info
    PetscScalar flowStartTime, flowEndTime;
    PetscScalar flowWriteStartTime, flowWriteEndTime;
    PetscScalar flowSurfaceWriteStartTime, flowSurfaceWriteEndTime;
    PetscScalar CFL;

    // wave
    PetscScalar hh; // water depth
    PetscScalar TT; // wave period
    PetscScalar LL; // wave length
    PetscScalar HH; // wave height

    PotentialWaves *wave = NULL;

    // wave tank
    DM dm;
    PetscViewer hdf5viewer_flow;
    Projection       *pro         = NULL;
    WaveMaker        *maker       = NULL;
    WaveTank         *tank        = NULL;
    DMStagHDF5Writer *flow_writer = NULL;
    ImmersedBoundaryMethod *ibm        = NULL;
    PetscInt    tankbc     = 0;
    PetscScalar flowDeltaT = 0;

    // grid
    PetscInt    Nx;         // number of cells in x
    PetscInt    Nz;         // number to cells in z
    PetscInt    Nx_in_L;    // number of cells in one wavelength
    PetscInt    Nz_in_h;    // number of cells in z
    PetscScalar xmin, xmax; // limits in x
    PetscScalar zmin, zmax; // limits in z
    PetscScalar dx, dz;     // grid cell sizes

    // output
    PetscScalar writeDeltaT;
    PetscInt    recordLeap = 1;

    // wave making
    PetscScalar gamma;      // wave reflection control parameter
    PetscScalar nexp;       // exponent
    PetscScalar nRamp;      // periods of warming up
    PetscScalar nIn;        // wave lengths of wave making zone
    PetscScalar nOut;       // wave lengths of wave absorbing zone

    // vegetation
    // truss system
    DM                  nw;
    TrussSystem         *ts = NULL;
    PetscViewer         hdf5viewer_struct;
    DMNetworkHDF5Writer *struct_writer = NULL;
    PetscScalar         linedensity;

    /* -------------------------- configurations -------------------------- */
    // read inputs for the model setup
    PetscOptionsCreate(&options);
    flag = PETSC_FALSE;
    std::string input_control = path + "/input/control.yaml";
    PetscOptionsInsertFileYAML(PETSC_COMM_WORLD, options, input_control.c_str(), flag);
    if (flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "\nPlease make sure the input yaml file exists and in correct format!\n\n");
        PetscEnd();
    }

    PetscOptionsGetScalar(options, NULL, "-flowStartTime", &flowStartTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-flowEndTime", &flowEndTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-CFL", &CFL, NULL);
    PetscOptionsGetScalar(options, NULL, "-flowWriteStartTime", &flowWriteStartTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-flowWriteEndTime", &flowWriteEndTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-flowSurfaceWriteStartTime", &flowSurfaceWriteStartTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-flowSurfaceWriteEndTime", &flowSurfaceWriteEndTime, NULL);
    PetscOptionsGetScalar(options, NULL, "-writeDeltaT", &writeDeltaT, NULL);

    // read wave characteristics from options
    PetscOptionsGetScalar(options, NULL, "-waterdepth", &hh, NULL);
    PetscOptionsGetScalar(options, NULL, "-waveperiod", &TT, NULL);
    PetscOptionsGetScalar(options, NULL, "-wavelength", &LL, NULL);
    PetscOptionsGetScalar(options, NULL, "-waveheight", &HH, NULL);
    PetscPrintf(PETSC_COMM_WORLD, "wave characteristics: h=%.5f, T=%.5f, L=%.5f, H=%.5f\n\n", hh, TT, LL, HH);
    
    // read resolution from options
    PetscOptionsGetInt(options, NULL, "-Nx", &Nx, NULL);
    PetscOptionsGetInt(options, NULL, "-Nz", &Nz, NULL);

    // computational domain
    PetscOptionsGetScalar(options, NULL, "-xmin", &xmin, NULL);
    PetscOptionsGetScalar(options, NULL, "-xmax", &xmax, NULL);

    // read wave maker settings from options
    PetscOptionsGetScalar(options, NULL, "-gamma", &gamma, NULL);
    PetscOptionsGetScalar(options, NULL, "-nexp", &nexp, NULL);
    PetscOptionsGetScalar(options, NULL, "-nRamp", &nRamp, NULL);
    PetscOptionsGetScalar(options, NULL, "-nIn", &nIn, NULL);
    PetscOptionsGetScalar(options, NULL, "-nOut", &nOut, NULL);

    PetscOptionsDestroy(&options);

    // setup truss system
    DMNetworkCreate(PETSC_COMM_WORLD, &nw);
    ts = new TrussSystem(nw);

    PetscOptionsCreate(&options);
    flag = PETSC_FALSE;
    std::string input_ts = path + "/input/trusssystem.yaml";
    PetscOptionsInsertFileYAML(PETSC_COMM_WORLD, options, input_ts.c_str(), flag);
    if (flag) 
    {
        PetscPrintf(PETSC_COMM_WORLD, "\nPlease make sure the input yaml file exists and in correct format!\n\n");
        PetscEnd();
    }
    ts->setupFromOptions(options);
    PetscOptionsGetScalar(options, NULL, "-linedensity", &linedensity, NULL);

    PetscOptionsDestroy(&options);

    // read inputs for structures in fluid
    PetscOptionsCreate(&options);
    flag = PETSC_FALSE;
    std::string input_tsinfluid = path + "/input/trusssysteminfluid.yaml";
    PetscOptionsInsertFileYAML(PETSC_COMM_WORLD, options, input_tsinfluid.c_str(), flag);
    if (flag) 
    {
        PetscPrintf(PETSC_COMM_WORLD, "\nPlease make sure the input yaml file exists and in correct format!\n\n");
        PetscEnd();
    }

    ts->setupInFluidFromOptions(options);
    PetscOptionsDestroy(&options);

    ts->initialize();

    // output
    PetscInt nnode   = ts->node_num();
    PetscInt nedge   = ts->truss_num();
    PetscInt nsubnet = ts->subnet_num();

    std::string output_struct = path + "/output/structure.h5";
    PetscViewerHDF5Open(PETSC_COMM_WORLD, output_struct.c_str(), FILE_MODE_WRITE, &hdf5viewer_struct);
    struct_writer = new DMNetworkHDF5Writer(hdf5viewer_struct, nnode, nedge, nsubnet, *ts, nw);
    struct_writer->pushGroup();
    struct_writer->writeToHDF5(nw, 0.0);
    struct_writer->popGroup();


    /* -------------------------- setup -------------------------- */
    wave = new AiryWave(TT, LL, hh, HH);

    const PetscInt dof0 = 0, dof1 = 1, dof2 = 1;
    const PetscInt stencilWidth = 1;
    DMStagCreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, Nx, Nz, PETSC_DECIDE, PETSC_DECIDE, dof0, dof1, dof2, DMSTAG_STENCIL_STAR, stencilWidth, NULL, NULL, &dm);
    DMSetFromOptions(dm);
    DMSetUp(dm);

    zmin = -hh;
    zmax = 0.0;
    DMStagSetUniformCoordinatesProduct(dm, xmin, xmax, zmin, zmax, 0.0, 0.0);

    DMStagGetGlobalSizes(dm, &Nx, &Nz, NULL);
    dx = (xmax - xmin) / Nx;
    dz = (zmax - zmin) / Nz;
    Nx_in_L = PetscInt(Nx / ((xmax - xmin) / LL) + 0.5);
    Nz_in_h = Nz;
    PetscPrintf(PETSC_COMM_WORLD, "The computational domain is (%.5f, %.5f)x(%.5f, %.5f)\n", xmin, xmax, zmin, zmax);
    PetscPrintf(PETSC_COMM_WORLD, "The overall grid resolution is %d x %d; the resolution within one wave length is %d x %d\n", Nx, Nz, Nx_in_L, Nz_in_h);
    PetscPrintf(PETSC_COMM_WORLD, "The grid size is (%.5fx%.5f); the grid aspect ratio is %.2f\n\n", dx, dz, dx / dz);

    maker = new WaveMaker(dm, *wave, xmin, xmax, gamma, nexp, nRamp, nIn, nOut);
    pro   = new Projection(dm, dx, dz, tankbc);
    tank  = new WaveTank(dm, *pro, *maker, tankbc);
    ibm   = new ImmersedBoundaryMethod(dm, dx, dz, xmin, xmax, zmin, zmax, 2);

    // output
    // flow output in hdf5
    std::string output_flow = path + "/output/flow.h5";
    PetscViewerHDF5Open(PETSC_COMM_WORLD, output_flow.c_str(), FILE_MODE_WRITE, &hdf5viewer_flow);
        
    flow_writer = new DMStagHDF5Writer(hdf5viewer_flow, dm);
    flow_writer->writeWaveToHDF5(*wave);

    // update time step size
    flowDeltaT = PetscMin(dx, dz) * CFL / wave->celerity();
    recordLeap = PetscInt(writeDeltaT / flowDeltaT + 0.5);
    flowDeltaT = writeDeltaT / recordLeap;

    PetscPrintf(PETSC_COMM_WORLD, "The flow simulation starts from %.3f s (%.1f period), ends at %.3f s (%.1f period), and is updated with an interval of %.2e s or %d times over one period.\n", 
        flowStartTime, flowStartTime / TT, flowEndTime,  flowEndTime / TT, flowDeltaT, PetscInt(TT / flowDeltaT + 0.5));
    PetscPrintf(PETSC_COMM_WORLD, "The record of the flow kinematics (velocities, pressure) starts from %.3f s (%.1f period), ends at %.3f s (%.1f period);\n", 
        flowWriteStartTime, flowWriteStartTime / TT, flowWriteEndTime, flowWriteEndTime / TT);
    PetscPrintf(PETSC_COMM_WORLD, "The record of the surface elevation (velocities, pressure) starts from %.3f s (%.1f period), ends at %.3f s (%.1f period);\n", 
        flowSurfaceWriteStartTime, flowSurfaceWriteStartTime / TT, flowSurfaceWriteEndTime, flowSurfaceWriteEndTime / TT);
    PetscPrintf(PETSC_COMM_WORLD, "and is done with an interval of %.2e s or %d times over one period, or every %d flow update steps.\n\n", writeDeltaT, PetscInt(TT / writeDeltaT + 0.5), PetscInt(writeDeltaT / flowDeltaT + 0.5));

    /* ------------------------------------- solver ------------------------------------- */
    // basic variables
    PetscScalar T0      = flowStartTime;
    PetscScalar endTime = flowEndTime;

    Vec sol;    // velocities and pressure
    Vec eta;    // surface elevation
    DMCreateGlobalVector(dm, &sol);
    DMCreateGlobalVector(dm, &eta);

    // set initial condition
    VecZeroEntries(sol);
    VecZeroEntries(eta);

    PetscScalar T = T0;
    PetscInt index = 0;
    PetscScalar eta_max = 0.0;

    // auxiliary vector
    Vec dU, dU0;
    Vec velo1, velo2;
    Vec acel;
    DMCreateGlobalVector(dm, &dU);
    DMCreateGlobalVector(dm, &dU0);
    DMCreateGlobalVector(dm, &velo1);
    DMCreateGlobalVector(dm, &velo2);
    DMCreateGlobalVector(dm, &acel);

    flow_writer->pushGroup();
    while(T <= endTime)
    {
        if (index % recordLeap == 0)
        {
            VecNorm(eta, NORM_INFINITY, &eta_max);
            if (T >= flowWriteStartTime && T <= flowWriteEndTime) flow_writer->writeSolutionToHDF5(sol, T);
            if (T >= flowSurfaceWriteStartTime && T <= flowSurfaceWriteEndTime) flow_writer->writeElevationToHDF5(eta, T);
            PetscPrintf(PETSC_COMM_WORLD, "i: %10d; T (s): %6.2f or T/TT: %6.2f; ", index, T, T/TT);
            PetscPrintf(PETSC_COMM_WORLD, "max surface elevation: %6.5f (m) or %6.5f by incident wave height\n", eta_max, eta_max * 2.0 / HH);
        }

        VecCopy(sol, velo1);
        tank->update(sol, eta, dU, T, flowDeltaT);
        VecCopy(sol, velo2);

        // flow acceleration
        VecWAXPY(acel, -1.0, velo1, velo2);
        VecScale(acel, 1.0 / flowDeltaT);

        VecZeroEntries(dU );
        VecZeroEntries(dU0);
        ibm->load(dU0, nw, sol, acel);
        VecScale(dU0, flowDeltaT);

        VecScale(dU0, linedensity);
        VecAXPY(dU, 1.0 / rhow, dU0);

        T += flowDeltaT;
        ++index;
    }
    flow_writer->popGroup();

    maker->destroy();
    pro->destroy();
    tank->destroy();
    flow_writer->destroy();
    ts->destroy();
    ibm->destroy();
    delete maker;
    delete pro;
    delete tank;
    delete flow_writer;
    delete wave;
    delete ts;
    delete struct_writer;
    DMDestroy(&nw);
    DMDestroy(&dm);
    VecDestroy(&sol);
    VecDestroy(&eta);
    VecDestroy(&velo1);
    VecDestroy(&velo2);
    VecDestroy(&acel);
    VecDestroy(&dU);
    VecDestroy(&dU0);
    PetscViewerDestroy(&hdf5viewer_flow);
    PetscViewerDestroy(&hdf5viewer_struct);

    ierr = PetscFinalize();
    return ierr;
}