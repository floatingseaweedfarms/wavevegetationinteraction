#include "RigidBar.h"
#include "Constants.h"


RigidBar::RigidBar() :
        x0(0.0), z0(0.0),
        l(0.0), b(0.0),
        mu(0.0), ma(0.0), 
        dm(0.0),
        I(0.0),
        N(1), ds(0.0),
        CD(0.0)
{
    Theta     = 0.0;
    dTheta    = 0.0;
    Theta_0   = 0.0;
    dTheta_0  = 0.0;
    ddTheta_0 = 0.0;
    PetscCalloc1(N, &F);
}

RigidBar::RigidBar(const PetscScalar x,
        const PetscScalar z,
        const PetscScalar length,
        const PetscScalar width,
        const PetscScalar bodymass,
        const PetscScalar addedmass,
        const PetscScalar massdiff,
        const PetscInt segments,
        const PetscScalar Cd) :
            x0(x), z0(z),
            l(length), b(width),
            mu(bodymass), ma(addedmass), 
            dm(massdiff),
            I(1.0 / 3.0 * bodymass * length * length * length),
            N(segments), ds(length / segments),
            CD(Cd)
{
    Theta     = 0.0;
    dTheta    = 0.0;
    Theta_0   = 0.0;
    dTheta_0  = 0.0;
    ddTheta_0 = 0.0;
    PetscCalloc1(N, &F);
}

PetscErrorCode RigidBar::hydroload(Vector3s &load, PetscScalar &f,
        const PetscScalar s,
        const Vector3s &xf, 
        const Vector3s &uf,
        const Vector3s &af) const
{
    // normal component of the relative velocity
    PetscScalar urn = uf(0) * PetscCosScalar(Theta) + uf(2) * PetscSinScalar(Theta) - s * dTheta;
    // normal component of the flow acceleration
    PetscScalar afn = af(0) * PetscCosScalar(Theta) + af(2) * PetscSinScalar(Theta);

    f = ma * afn + 1.0 / 2.0 * rhow * CD * b * PetscAbsScalar(urn) * urn;

    // unit normal vector
    Vector3s nn = Vector3s{PetscCosScalar(Theta), 0.0, PetscSinScalar(Theta)};

    load = (f - ma * s * ddTheta_0) * nn;

    return 0;
}

PetscErrorCode RigidBar::update_RK2nd(const PetscScalar dt, const PetscInt step)
{
    PetscScalar s;
    Vector3s load;

    PetscScalar M = 0.0;
    for (PetscInt i = 0; i != N; ++i)
    {
        s = 0.5 * ds + ds * i;
        M += F[i] * ds * s;
    }

    PetscScalar ddTheta = (M - 0.5 * dm * l * g * l * Theta) / (I + 1.0 / 3.0 * ma * l * l * l);

    if (step == 1)
    {
        ddTheta_0 = ddTheta;
        dTheta_0  = dTheta ;
        Theta_0   = Theta;

        dTheta += ddTheta * dt;
        Theta  += dTheta  * dt;
    }

    if (step == 2)
    {
        dTheta = dTheta_0 + ddTheta * dt;
        Theta  = Theta_0  + dTheta_0 * dt + ddTheta_0 * dt * dt / 2.0;
    }

    return 0;
}