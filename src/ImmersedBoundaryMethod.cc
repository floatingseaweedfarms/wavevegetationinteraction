#include <petscdmnetwork.h>
#include "ImmersedBoundaryMethod.h"
#include "HydroLoad.h"

ImmersedBoundaryMethod::ImmersedBoundaryMethod(const DM &dm,
        const PetscScalar Dx, const PetscScalar Dz,
        const PetscScalar xMin, const PetscScalar xMax,
        const PetscScalar zMin, const PetscScalar zMax,
        const PetscInt N) : ptrDM(&dm),
        dx(Dx), dz(Dz),
        xmin(xMin), xmax(xMax), 
        zmin(zMin), zmax(zMax),
        span(N)
{
    DMStagGetGlobalSizes(dm, &Nx, &Nz, NULL);

    DMCreateMatrix(*ptrDM, &ele2lr);
    DMCreateMatrix(*ptrDM, &ele2ud);
    MatSetOption(ele2lr, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
    MatSetOption(ele2ud, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);

    PetscInt dims[2];
    PetscInt startx, starty, nx, ny, nExtra[2], ex, ey;
    DMStagStencil row, col[2];
    PetscScalar val[2];
    PetscInt nCol;
    PetscBool isFirstRank1, isLastRank1;
    PetscBool isFirstRank2, isLastRank2;

    DMStagGetIsFirstRank(*ptrDM, &isFirstRank1,&isFirstRank2, NULL);
    DMStagGetIsLastRank(*ptrDM, &isLastRank1, &isLastRank2, NULL);
    DMStagGetGlobalSizes(dm, &dims[0], &dims[1], NULL);
    DMStagGetCorners(*ptrDM, &startx, &starty, NULL, &nx, &ny, NULL, &nExtra[0], &nExtra[1], NULL);

    val[0] = 0.5;
    val[1] = 0.5;

    for (ey = starty; ey != starty + ny + nExtra[1]; ++ey)
    {
        for (ex = startx; ex != startx + nx + nExtra[0]; ++ex)
        {
            nCol = 0;

            row.i = ex; row.j = ey; row.loc = DMSTAG_LEFT; row.c = 0;
            if (ex == 0)
            {
                col[nCol].i = ex; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                ++nCol;
            }
            else if (ex == dims[0])
            {
                col[nCol].i = ex - 1; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                ++nCol;
            }
            else
            {
                col[nCol].i = ex - 1; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                ++nCol;
                col[nCol].i = ex    ; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                ++nCol;
            }

            DMStagMatSetValuesStencil(*ptrDM, ele2lr, 1, &row, nCol, col, val, INSERT_VALUES);

            nCol = 0;
            row.i = ex; row.j = ey; row.loc = DMSTAG_DOWN; row.c = 0;
            if (ey == 0) continue;
            else if (ey == dims[1])
            {
                col[nCol].i = ex; col[nCol].j = ey - 1; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                ++nCol;
            }
            else
            {
                col[nCol].i = ex; col[nCol].j = ey    ; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                ++nCol;
                col[nCol].i = ex; col[nCol].j = ey - 1; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                ++nCol;
            }
            DMStagMatSetValuesStencil(*ptrDM, ele2ud, 1, &row, nCol, col, val, INSERT_VALUES);
        }
    }

    MatAssemblyBegin(ele2lr, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(ele2lr, MAT_FINAL_ASSEMBLY);
    MatAssemblyBegin(ele2ud, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(ele2ud, MAT_FINAL_ASSEMBLY);
}

PetscErrorCode ImmersedBoundaryMethod::destroy()
{
    ptrDM = NULL;

    MatDestroy(&ele2lr);
    MatDestroy(&ele2ud);

    return 0;
}

PetscErrorCode ImmersedBoundaryMethod::load(Vec &dU, DM &ts,
        const Vec &Uf, const Vec &Af,
        const PetscScalar factor) const
{
    Vec veloLocal, acelLocal;
    DMGetLocalVector(*ptrDM, &veloLocal);
    DMGetLocalVector(*ptrDM, &acelLocal);
    VecZeroEntries(veloLocal);
    VecZeroEntries(acelLocal);
    DMGlobalToLocal(*ptrDM, Uf, INSERT_VALUES, veloLocal);
    DMGlobalToLocal(*ptrDM, Af, INSERT_VALUES, acelLocal);

    Vec du, dw;
    DMCreateGlobalVector(*ptrDM, &du);
    DMCreateGlobalVector(*ptrDM, &dw);
    Vec duLocal, dwLocal;
    DMGetLocalVector(*ptrDM, &duLocal);
    DMGetLocalVector(*ptrDM, &dwLocal);
    VecZeroEntries(duLocal);
    VecZeroEntries(dwLocal);

    PetscInt iul, iur, ivu, ivd, ip;
    PetscInt iprev, icenter;
    PetscReal ***arrVelo, ***arrAcel, ***arrDu, ***arrDw;
    PetscScalar **arrX, **arrY;
    PetscInt startx, starty, nx, ny, nExtra[2], ex, ey;
    DMStagGetCorners(*ptrDM, &startx, &starty, NULL, &nx, &ny, NULL, &nExtra[0], &nExtra[1], NULL);

    DMStagGetLocationSlot(*ptrDM, DMSTAG_LEFT   , 0, &iul);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_RIGHT  , 0, &iur);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_UP     , 0, &ivu);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_DOWN   , 0, &ivd);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_ELEMENT, 0, &ip );

    DMStagGetProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_LEFT   , &iprev  );
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_ELEMENT, &icenter);

    DMStagVecGetArrayRead(*ptrDM, veloLocal, &arrVelo);
    DMStagVecGetArrayRead(*ptrDM, acelLocal, &arrAcel);
    DMStagVecGetArray(*ptrDM, duLocal, &arrDu);
    DMStagVecGetArray(*ptrDM, dwLocal, &arrDw);

    Vector3s xf, uf, af;    // position, velocity and acceleration at grid node
    Matrix3s gu;            // velocity gradient tensor 
    PetscInt imin, imax;    // delta region index
    PetscInt jmin, jmax;    // delta region index

    HydroLoad loadcomputer = HydroLoad();

    /* iterate over trusses */
    Truss *truss;
    PetscInt eStart, eEnd;
    Vector3s f;
    Vector3s F;
    DMNetworkGetEdgeRange(ts, &eStart, &eEnd);
    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(ts, e, 0, NULL, (void**)&truss, NULL);
        // bound indices of the delta region
        whichCells(imin, imax, jmin, jmax, truss->x(0), truss->x(2), dx, dz);

        F.setZero();
        for (ey = starty + jmin; ey != starty + jmax + 1; ++ey)
        {
            for (ex = startx + imin; ex != startx + imax + 1; ++ex)
            {
                // position, velocity, and acceleration
                xf(0) = arrX[ex][icenter];
                xf(1) = 0.0;
                xf(2) = arrY[ey][icenter];
                // velocity
                uf(0) = 0.5 * (arrVelo[ey][ex][iul] + arrVelo[ey][ex][iur]);
                uf(1) = 0.0;
                uf(2) = 0.5 * (arrVelo[ey][ex][ivu] + arrVelo[ey][ex][ivd]);
                // acceleration
                af(0) = 0.5 * (arrAcel[ey][ex][iul] + arrAcel[ey][ex][iur]);
                af(1) = 0.0;
                af(2) = 0.5 * (arrAcel[ey][ex][ivu] + arrAcel[ey][ex][ivd]);
                // velocity gradient
                gu(0, 0) = (arrVelo[ey][ex][iur] - arrVelo[ey][ex][iul]) / dx;
                gu(0, 1) = 0.0;
                if (ey == starty) gu(0, 2) = (arrVelo[ey+1][ex][iur] + arrVelo[ey+1][ex][iul] - arrVelo[ey  ][ex][iur] - arrVelo[ey  ][ex][iul]) / 4.0 / dz;
                else              gu(0, 2) = (arrVelo[ey+1][ex][iur] + arrVelo[ey+1][ex][iul] - arrVelo[ey-1][ex][iur] - arrVelo[ey-1][ex][iul]) / 4.0 / dz;
                gu(1, 0) = 0.0;
                gu(1, 1) = 0.0;
                gu(1, 2) = 0.0;
                gu(2, 0) = (arrVelo[ey][ex+1][ivu] + arrVelo[ey][ex+1][ivd] - arrVelo[ey][ex-1][ivu] - arrVelo[ey][ex-1][ivd]) / 4.0 / dx;
                gu(2, 1) = 0.0;
                gu(2, 2) = (arrVelo[ey][ex][ivu] - arrVelo[ey][ex][ivd]) / dz;
                // gu.setZero();
                loadcomputer.load(f, *truss, xf, uf, af, gu);
                f *= delta(truss->x - xf, dx, 1.0, dz);
                f *= factor;
                F += f;
                arrDu[ey][ex][ip] -= f(0) / dx / dz;
                arrDw[ey][ex][ip] -= f(2) / dx / dz;
            }
        }
        truss->F = F;
    }

    DMStagRestoreProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);

    DMStagVecRestoreArrayRead(*ptrDM, veloLocal, &arrVelo);
    DMStagVecRestoreArrayRead(*ptrDM, acelLocal, &arrAcel);
    DMStagVecRestoreArray(*ptrDM, duLocal, &arrDu);
    DMStagVecRestoreArray(*ptrDM, dwLocal, &arrDw);
    DMRestoreLocalVector(*ptrDM, &veloLocal);
    DMRestoreLocalVector(*ptrDM, &acelLocal);
    DMLocalToGlobal(*ptrDM, duLocal, INSERT_VALUES, du);
    DMLocalToGlobal(*ptrDM, dwLocal, INSERT_VALUES, dw);
    DMRestoreLocalVector(*ptrDM, &duLocal);
    DMRestoreLocalVector(*ptrDM, &dwLocal);

    VecZeroEntries(dU);
    MatMultAdd(ele2lr, du, dU, dU);
    MatMultAdd(ele2ud, dw, dU, dU);

    VecDestroy(&du);
    VecDestroy(&dw);

    return 0;
}

PetscErrorCode ImmersedBoundaryMethod::load(Vec &dU, RigidBar **bars,
        const PetscScalar nbars,
        const Vec &Uf, const Vec &Af,
        const PetscScalar factor) const
{
    Vec veloLocal, acelLocal;
    DMGetLocalVector(*ptrDM, &veloLocal);
    DMGetLocalVector(*ptrDM, &acelLocal);
    VecZeroEntries(veloLocal);
    VecZeroEntries(acelLocal);
    DMGlobalToLocal(*ptrDM, Uf, INSERT_VALUES, veloLocal);
    DMGlobalToLocal(*ptrDM, Af, INSERT_VALUES, acelLocal);

    Vec du, dw;
    DMCreateGlobalVector(*ptrDM, &du);
    DMCreateGlobalVector(*ptrDM, &dw);
    Vec duLocal, dwLocal;
    DMGetLocalVector(*ptrDM, &duLocal);
    DMGetLocalVector(*ptrDM, &dwLocal);
    VecZeroEntries(duLocal);
    VecZeroEntries(dwLocal);

    PetscInt iul, iur, ivu, ivd, ip;
    PetscInt iprev, icenter;
    PetscReal ***arrVelo, ***arrAcel, ***arrDu, ***arrDw;
    PetscScalar **arrX, **arrY;
    PetscInt startx, starty, nx, ny, nExtra[2], ex, ey;
    DMStagGetCorners(*ptrDM, &startx, &starty, NULL, &nx, &ny, NULL, &nExtra[0], &nExtra[1], NULL);

    DMStagGetLocationSlot(*ptrDM, DMSTAG_LEFT   , 0, &iul);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_RIGHT  , 0, &iur);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_UP     , 0, &ivu);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_DOWN   , 0, &ivd);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_ELEMENT, 0, &ip );

    DMStagGetProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_LEFT   , &iprev  );
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_ELEMENT, &icenter);

    DMStagVecGetArrayRead(*ptrDM, veloLocal, &arrVelo);
    DMStagVecGetArrayRead(*ptrDM, acelLocal, &arrAcel);
    DMStagVecGetArray(*ptrDM, duLocal, &arrDu);
    DMStagVecGetArray(*ptrDM, dwLocal, &arrDw);

    Vector3s xf, uf, af;    // position, velocity and acceleration at grid node
    PetscInt imin, imax;    // delta region index
    PetscInt jmin, jmax;    // delta region index

    for (PetscInt ibar = 0; ibar != nbars; ++ibar)
    {
        Vector3s load = Vector3s::Zero();
        PetscScalar f = 0.0, F = 0.0;

        // length of the bar
        const PetscScalar bar_length = bars[ibar]->length();
        // number of segments
        const PetscInt bar_N = bars[ibar]->segments();
        // segment size
        const PetscScalar ds = bar_length / bar_N;

        PetscScalar bar_x, bar_z;
        bars[ibar]->hangingPoint(bar_x, bar_z);
        // theta
        PetscScalar theta = bars[ibar]->theta();

        PetscScalar s, x, z;
        for (PetscInt i = 0; i != bar_N; ++i)
        {
            s = ds / 2.0 + ds * i;
            x = bar_x + s * PetscSinScalar(theta);
            z = bar_z - s * PetscCosScalar(theta);
            Vector3s X = Vector3s{x, 0, z};
            // bound indices of the delta region
            whichCells(imin, imax, jmin, jmax, x, z, dx, dz);

            F = 0.0;
            for (ey = starty + jmin; ey != starty + jmax + 1; ++ey)
            {
                for (ex = startx + imin; ex != startx + imax + 1; ++ex)
                {
                    // position, velocity, and acceleration
                    xf(0) = arrX[ex][icenter];
                    xf(1) = 0.0;
                    xf(2) = arrY[ey][icenter];
                    // velocity
                    uf(0) = 0.5 * (arrVelo[ey][ex][iul] + arrVelo[ey][ex][iur]);
                    uf(1) = 0.0;
                    uf(2) = 0.5 * (arrVelo[ey][ex][ivu] + arrVelo[ey][ex][ivd]);
                    // acceleration
                    af(0) = 0.5 * (arrAcel[ey][ex][iul] + arrAcel[ey][ex][iur]);
                    af(1) = 0.0;
                    af(2) = 0.5 * (arrAcel[ey][ex][ivu] + arrAcel[ey][ex][ivd]);

                    bars[ibar]->hydroload(load, f, s, xf, uf, af);
                    load *= delta(X - xf, dx, 1.0, dz);
                    load *= factor;
                    f *= delta(X - xf, dx, 1.0, dz);
                    f *= factor;
                    F += f;
                    arrDu[ey][ex][ip] -= load(0) * ds / dx / dz;
                    arrDw[ey][ex][ip] -= load(2) * ds / dx / dz;
                }
            }
            bars[ibar]->loads()[i] = F;
        }
    }
    DMStagRestoreProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);

    DMStagVecRestoreArrayRead(*ptrDM, veloLocal, &arrVelo);
    DMStagVecRestoreArrayRead(*ptrDM, acelLocal, &arrAcel);
    DMStagVecRestoreArray(*ptrDM, duLocal, &arrDu);
    DMStagVecRestoreArray(*ptrDM, dwLocal, &arrDw);
    DMRestoreLocalVector(*ptrDM, &veloLocal);
    DMRestoreLocalVector(*ptrDM, &acelLocal);
    DMLocalToGlobal(*ptrDM, duLocal, INSERT_VALUES, du);
    DMLocalToGlobal(*ptrDM, dwLocal, INSERT_VALUES, dw);
    DMRestoreLocalVector(*ptrDM, &duLocal);
    DMRestoreLocalVector(*ptrDM, &dwLocal);

    VecZeroEntries(dU);
    MatMultAdd(ele2lr, du, dU, dU);
    MatMultAdd(ele2ud, dw, dU, dU);

    VecDestroy(&du);
    VecDestroy(&dw);

    return 0;
}

PetscErrorCode ImmersedBoundaryMethod::whichCells(PetscInt &imin, PetscInt &imax,
        PetscInt &jmin, PetscInt &jmax,
        const PetscScalar x, const PetscScalar z,
        const PetscScalar dx, const PetscScalar dz) const
{
    // make sure the point is within computational area
    if (x < xmin || x > xmax || z < zmin || z > zmax)
    {
        PetscPrintf(PETSC_COMM_WORLD, "The point given (x, z) = (%.3f, %.3f) is not within the computational domain [%.3f, %.3f]x[%.3f, %.3f]!\n", x, z, xmin, xmax, zmin, zmax);
        PetscEnd();
    }

    PetscInt i = PetscInt(std::round((x - xmin) / dx));
    PetscInt j = PetscInt(std::round((z - zmin) / dz));
    imin = PetscMax(i - span, 0     );
    imax = PetscMin(i + span, Nx - 1);
    jmin = PetscMax(j - span, 0     );
    jmax = PetscMin(j + span, Nz - 1);

    return 0;
}

PetscScalar ImmersedBoundaryMethod::delta(const Vector3s &x,
        const PetscScalar dx,
        const PetscScalar dy,
        const PetscScalar dz) const
{
    // return delta(x(0) / dx)  * delta(x(1) / dy) * delta(x(2) / dz);
    return delta(x(0) / dx) * delta(x(2) / dz);
}

PetscScalar ImmersedBoundaryMethod::delta(const PetscScalar x) const
{
    return PetscAbsScalar(x) <= 2.0 ? 0.25 * (1.0 + PetscCosScalar(PETSC_PI * x / 2.0)) : 0.0;
}