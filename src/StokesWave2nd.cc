#include "StokesWave2nd.h"
#include "Constants.h"

PetscScalar StokesWave2nd::u(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return c * k * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x)
        + 3.0 / 16.0 * c * k * k * H * H * PetscCoshScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscCosScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::w(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -c * k * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x)
        - 3.0 / 16.0 * c * k * k * H * H * PetscSinhScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscSinScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::ax(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -omega * c * k * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x)
        - omega * 3.0 / 8.0 * c * k * k * H * H * PetscCoshScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscSinScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::az(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -omega * c * k * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x)
        - omega * 3.0 / 8.0 * c * k * k * H * H * PetscSinhScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscCosScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return c * k * k * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x)
        + 3.0 / 8.0 * c * k * k * k * H * H * PetscCoshScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscSinScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return c * k * k * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x)
        + 3.0 / 8.0 * c * k * k * k * H * H * PetscSinhScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscCosScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return c * k * k * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x)
        + 3.0 / 8.0 * c * k * k * k * H * H * PetscSinhScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscCosScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -c * k * k * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x)
        - 3.0 / 8.0 * c * k * k * k * H * H * PetscCoshScalar(2.0 * k * (z + h)) / PetscPowScalar(PetscSinhScalar(k * h), 4.0) * PetscSinScalar(2.0 * (omega * t - k * x));
}

PetscScalar StokesWave2nd::p(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return rhow * g * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscCoshScalar(k * h) * PetscCosScalar(omega * t - k * x)
        + 1.0 / 8.0 * rhow * g * k * H * H / PetscSinhScalar(2.0 * k * h) 
        * ((3.0 * PetscCoshScalar(2.0 * k * (z + h)) / PetscSinhScalar(k * h) / PetscSinhScalar(k * h)) * PetscCosScalar(2.0 * (omega * t - k * x)) 
        + 1.0 - PetscCoshScalar(2.0 * k * (z + h)));
}

PetscScalar StokesWave2nd::elevation(const PetscScalar x, const PetscScalar t) const
{
    return H / 2.0 * PetscCosScalar(omega * t - k * x)
        + 1.0 / 16.0 * k * H * H * (3.0 / PetscPowScalar(PetscTanhScalar(k * h), 3.0) - 1.0 / PetscTanhScalar(k * h)) * PetscCosScalar(2.0 * (omega * t - k * x));
}