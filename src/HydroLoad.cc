#include "HydroLoad.h"
#include "Constants.h"

const Vector3s kk {0, -1, 0};

PetscErrorCode HydroLoad::load(Vector3s &F, Truss &t,
    const Vector3s &X,
    const Vector3s &U,
    const Vector3s &A,
    const Matrix3s &du) const
{
    Vector3s tau = t.dX / t.l;          // unit tangential vector
    Vector3s n = tau.cross(kk);         // unit normal vector

    Vector3s du_dtau = du * tau;         // directional derivative

    Vector3s ur  = t.u - U;             // relative velocity
    Vector3s urt = ur.dot(tau) * tau;   // tangent relative velocity
    Vector3s urn = ur.dot(n) * n;       // normal relative velocity

    F = - 0.5 * rhow * t.Cd * t.b * urn.norm() * urn * t.l
        + t.addedmass * A.dot(n) * n
        - t.addedmass * (urt.dot(urt) - 0.5 * urn.dot(urn)) * t.Kappa.cross(tau)
        - t.addedmass * (du_dtau.dot(tau) * ur.dot(n) + du_dtau.dot(n) * ur.dot(tau)) * n
        + t.addedmass * ur.dot(n) * du_dtau.dot(n) * tau;
    
    t.Ut = ur.dot(tau);
    
    return 0;
}