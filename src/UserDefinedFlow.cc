#include "UserDefinedFlow.h"
#include "Constants.h"

const PetscScalar a1 =  1.66391382e-01;
const PetscScalar a2 = -5.01996782e-02;
const PetscScalar a3 = -9.36103865e-03;
const PetscScalar a4 = -8.21786426e-04;
const PetscScalar p1 =  1.17770178e-01;
const PetscScalar p2 =  2.60414666e+00;
const PetscScalar p3 =  2.15505299e+00;
const PetscScalar p4 =  1.06414591e+00;

const PetscScalar  h = 0.30;
const PetscScalar  d = 0.04;
const PetscScalar  k = 0.88365177;
const PetscScalar  T = 2.0;
const PetscScalar  omega = 2.0 * PETSC_PI / T;

PetscScalar UserDefinedFlow::u(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return PetscCoshScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
        a1 * PetscSinScalar(      omega * t - k * x + p1) + 
        a2 * PetscSinScalar(2.0 * omega * t - k * x + p2) + 
        a3 * PetscSinScalar(3.0 * omega * t - k * x + p3) + 
        a4 * PetscSinScalar(4.0 * omega * t - k * x + p4));
}

PetscScalar UserDefinedFlow::w(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return PetscSinhScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
        a1 * PetscCosScalar(      omega * t - k * x + p1) + 
        a2 * PetscCosScalar(2.0 * omega * t - k * x + p2) + 
        a3 * PetscCosScalar(3.0 * omega * t - k * x + p3) + 
        a4 * PetscCosScalar(4.0 * omega * t - k * x + p4));
}

PetscScalar UserDefinedFlow::ax(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return PetscCoshScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
              omega * a1 * PetscCosScalar(      omega * t - k * x + p1) + 
        2.0 * omega * a2 * PetscCosScalar(2.0 * omega * t - k * x + p2) + 
        3.0 * omega * a3 * PetscCosScalar(3.0 * omega * t - k * x + p3) + 
        4.0 * omega * a4 * PetscCosScalar(4.0 * omega * t - k * x + p4));
}

PetscScalar UserDefinedFlow::az(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -PetscSinhScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
              omega * a1 * PetscSinScalar(      omega * t - k * x + p1) + 
        2.0 * omega * a2 * PetscSinScalar(2.0 * omega * t - k * x + p2) + 
        3.0 * omega * a3 * PetscSinScalar(3.0 * omega * t - k * x + p3) + 
        4.0 * omega * a4 * PetscSinScalar(4.0 * omega * t - k * x + p4));
}

PetscScalar UserDefinedFlow::dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -k * PetscCoshScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
        a1 * PetscCosScalar(      omega * t - k * x + p1) + 
        a2 * PetscCosScalar(2.0 * omega * t - k * x + p2) + 
        a3 * PetscCosScalar(3.0 * omega * t - k * x + p3) + 
        a4 * PetscCosScalar(4.0 * omega * t - k * x + p4));
}

PetscScalar UserDefinedFlow::dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return k * PetscSinhScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
        a1 * PetscSinScalar(      omega * t - k * x + p1) + 
        a2 * PetscSinScalar(2.0 * omega * t - k * x + p2) + 
        a3 * PetscSinScalar(3.0 * omega * t - k * x + p3) + 
        a4 * PetscSinScalar(4.0 * omega * t - k * x + p4));
}

PetscScalar UserDefinedFlow::dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return k * PetscSinhScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
        a1 * PetscSinScalar(      omega * t - k * x + p1) + 
        a2 * PetscSinScalar(2.0 * omega * t - k * x + p2) + 
        a3 * PetscSinScalar(3.0 * omega * t - k * x + p3) + 
        a4 * PetscSinScalar(4.0 * omega * t - k * x + p4));
}

PetscScalar UserDefinedFlow::dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return k * PetscCoshScalar(k * (z + h)) / PetscCoshScalar(k * d) * (
        a1 * PetscCosScalar(      omega * t - k * x + p1) + 
        a2 * PetscCosScalar(2.0 * omega * t - k * x + p2) + 
        a3 * PetscCosScalar(3.0 * omega * t - k * x + p3) + 
        a4 * PetscCosScalar(4.0 * omega * t - k * x + p4));
}