#include "Projection.h"
#include "Constants.h"

Projection::Projection(const DM &dm, 
        const PetscScalar Dx, 
        const PetscScalar Dz,
        const PetscInt    bc) : ptrDM(&dm),
        dx(Dx), dz(Dz)
{
    const PetscScalar X = 1.0 / dx / dx;
    const PetscScalar Z = 1.0 / dz / dz;

    if (bc != 0 && bc != 1)
    {
        PetscPrintf(PETSC_COMM_WORLD, "WRONG boundary condition for the x direction, either 0 or 1!\n");
        PetscEnd();
    }
    xbc = bc;

    DMStagCreateCompatibleDMStag(*ptrDM, 0, 0, 1, 0, &pDM);

    DMCreateMatrix(pDM, &A);
    DMCreateMatrix(*ptrDM, &dU);
    DMCreateMatrix(*ptrDM, &dP);
    DMCreateMatrix(*ptrDM, &U );
    DMCreateMatrix(*ptrDM, &ddP1stBC);
    DMCreateMatrix(*ptrDM, &dP1stBC );
    MatSetOption(A, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
    MatSetOption(A, MAT_SPD, PETSC_TRUE);
    MatSetOption(dU, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
    MatSetOption(dP, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
    MatSetOption(U , MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
    MatSetOption(ddP1stBC, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
    MatSetOption(dP1stBC , MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);

    PetscInt N[2];
    PetscInt startx, starty, nx, ny, nExtra[2], ex, ey;
    DMStagStencil row, col[5];
    PetscScalar val[5];
    PetscInt nCol;

    DMStagGetGlobalSizes(dm, &N[0], &N[1], NULL);
    DMStagGetCorners(dm, &startx, &starty, NULL, &nx, &ny, NULL, &nExtra[0], &nExtra[1], NULL);

    for (ey = starty; ey != starty + ny; ++ey)
    {
        for (ex = startx; ex != startx + nx; ++ex)
        {
            // set up coefficient matrix
            if (xbc == 0)
            {
                nCol = 0;

                row.i = ex; row.j = ey; row.loc = DMSTAG_ELEMENT; row.c = 0;
                col[nCol].i = ex; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0; 
                val[nCol] = -2.0 * X - 2.0 * Z; ++nCol;
                if (ex == 0) val[0] += X;
                else
                {
                    col[nCol].i = ex - 1; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                    val[nCol] = X; ++nCol;
                }
                if (ex == N[0] - 1) val[0] += X;
                else
                {
                    col[nCol].i = ex + 1; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                    val[nCol] = X; ++nCol;
                }
                if (ey == 0) val[0] += Z;
                else
                {
                    col[nCol].i = ex; col[nCol].j = ey - 1; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                    val[nCol] = Z; ++nCol;
                }
                if (ey == N[1] - 1) val[0] -= Z;
                else
                {
                    col[nCol].i = ex; col[nCol].j = ey + 1; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                    val[nCol] = Z; ++nCol;
                }
                DMStagMatSetValuesStencil(pDM, A, 1, &row, nCol, col, val, INSERT_VALUES);
            }

            if (xbc == 1)
            {
                nCol = 0;

                row.i = ex; row.j = ey; row.loc = DMSTAG_ELEMENT; row.c = 0;
                col[nCol].i = ex; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0; 
                val[nCol] = -2.0 * X - 2.0 * Z; ++nCol;

                col[nCol].i = ex - 1; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                val[nCol] = X; ++nCol;

                col[nCol].i = ex + 1; col[nCol].j = ey; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                val[nCol] = X; ++nCol;

                if (ey == 0) val[0] += Z;
                else
                {
                    col[nCol].i = ex; col[nCol].j = ey - 1; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                    val[nCol] = Z; ++nCol;
                }
                if (ey == N[1] - 1) val[0] -= Z;
                else
                {
                    col[nCol].i = ex; col[nCol].j = ey + 1; col[nCol].loc = DMSTAG_ELEMENT; col[nCol].c = 0;
                    val[nCol] = Z; ++nCol;
                }
                DMStagMatSetValuesStencil(pDM, A, 1, &row, nCol, col, val, INSERT_VALUES);
            }

            // set divergence matrix
            row.i = ex; row.j = ey; row.loc = DMSTAG_ELEMENT; row.c = 0;
            col[0].i = ex    ; col[0].j = ey    ; col[0].loc = DMSTAG_LEFT; col[0].c = 0; val[0] = -1.0 / dx;
            col[1].i = ex + 1; col[1].j = ey    ; col[1].loc = DMSTAG_LEFT; col[1].c = 0; val[1] =  1.0 / dx;
            col[2].i = ex    ; col[2].j = ey    ; col[2].loc = DMSTAG_DOWN; col[2].c = 0; val[2] = -1.0 / dz;
            col[3].i = ex    ; col[3].j = ey + 1; col[3].loc = DMSTAG_DOWN; col[3].c = 0; val[3] =  1.0 / dz;
            DMStagMatSetValuesStencil(dm, dU, 1, &row, 4, col, val, INSERT_VALUES);

            if (xbc == 0)
            {
                // set gradient matrix
                if (ex != 0)
                {
                    row.i = ex; row.j = ey; row.loc = DMSTAG_LEFT; row.c = 0;
                    col[0].i = ex - 1; col[0].j = ey; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] = -1.0 / dx;
                    col[1].i = ex    ; col[1].j = ey; col[1].loc = DMSTAG_ELEMENT; col[1].c = 0; val[1] =  1.0 / dx;
                    DMStagMatSetValuesStencil(dm, dP, 1, &row, 2, col, val, INSERT_VALUES);
                }
                if (ey != 0)
                {
                    row.i = ex; row.j = ey; row.loc = DMSTAG_DOWN; row.c = 0;
                    col[0].i = ex; col[0].j = ey - 1; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] = -1.0 / dz;
                    col[1].i = ex; col[1].j = ey    ; col[1].loc = DMSTAG_ELEMENT; col[1].c = 0; val[1] =  1.0 / dz;
                    DMStagMatSetValuesStencil(dm, dP, 1, &row, 2, col, val, INSERT_VALUES);
                    if (ey == N[1] - 1)
                    {
                        row.i = ex; row.j = ey; row.loc = DMSTAG_UP; row.c = 0;
                        col[0].i = ex; col[0].j = ey; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] = -2.0 / dz;
                        DMStagMatSetValuesStencil(dm, dP, 1, &row, 1, col, val, INSERT_VALUES);
                    }
                }
            }

            // set up matrix for second derivative of pressure on the Dirichlet boundarys
            if (ey == N[1] - 1)
            {
                row.i = ex; row.j = ey; row.loc = DMSTAG_ELEMENT; row.c = 0;
                col[0].i = ex    ; col[0].j = ey; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] = -1.0 / 2.0 / dx / dx - 2.0 / dz / dz;
                col[1].i = ex + 1; col[1].j = ey; col[1].loc = DMSTAG_ELEMENT; col[1].c = 0; val[1] =  1.0 / 4.0 / dx / dx;
                col[2].i = ex - 1; col[2].j = ey; col[2].loc = DMSTAG_ELEMENT; col[2].c = 0; val[2] =  1.0 / 4.0 / dx / dx;
                DMStagMatSetValuesStencil(dm, ddP1stBC, 1, &row, 3, col, val, INSERT_VALUES);
            }
        }
    }
    MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyBegin(dU, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(dU, MAT_FINAL_ASSEMBLY);
    MatAssemblyBegin(ddP1stBC, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(ddP1stBC, MAT_FINAL_ASSEMBLY);

    for (ey = starty; ey != starty + ny + nExtra[1]; ++ey)
    {
        for (ex = startx; ex != startx + nx + nExtra[0]; ++ex)
        {
            // set up matrix to pick velocity from sol
            val[0] = 1.0;
            row.i = ex; row.j = ey; row.loc = DMSTAG_DOWN; row.c = 0;
            DMStagMatSetValuesStencil(dm, U, 1, &row, 1, &row, val, INSERT_VALUES);
            row.i = ex; row.j = ey; row.loc = DMSTAG_LEFT; row.c = 0;
            DMStagMatSetValuesStencil(dm, U, 1, &row, 1, &row, val, INSERT_VALUES);

            if (xbc == 1)
            {
                row.i = ex; row.j = ey; row.loc = DMSTAG_LEFT; row.c = 0;
                col[0].i = ex - 1; col[0].j = ey; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] = -1.0 / dx;
                col[1].i = ex    ; col[1].j = ey; col[1].loc = DMSTAG_ELEMENT; col[1].c = 0; val[1] =  1.0 / dx;
                DMStagMatSetValuesStencil(dm, dP, 1, &row, 2, col, val, INSERT_VALUES);
                if (ey != 0)
                {
                    row.i = ex; row.j = ey; row.loc = DMSTAG_DOWN; row.c = 0;
                    col[0].i = ex; col[0].j = ey - 1; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] = -1.0 / dz;
                    col[1].i = ex; col[1].j = ey    ; col[1].loc = DMSTAG_ELEMENT; col[1].c = 0; val[1] =  1.0 / dz;
                    DMStagMatSetValuesStencil(dm, dP, 1, &row, 2, col, val, INSERT_VALUES);
                    if (ey == N[1])
                    {
                        row.i = ex; row.j = ey; row.loc = DMSTAG_DOWN; row.c = 0;
                        col[0].i = ex; col[0].j = ey - 1; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] = -2.0 / dz;
                        DMStagMatSetValuesStencil(dm, dP, 1, &row, 1, col, val, INSERT_VALUES);
                    }
                }
            }

            // set up matrix for gradient of pressure on the Dirichlet boundarys
            if (ey == N[1] - 1)
            {
                row.i = ex; row.j = ey; row.loc = DMSTAG_UP; row.c = 0;
                col[0].i = ex    ; col[0].j = ey; col[0].loc = DMSTAG_ELEMENT; col[0].c = 0; val[0] =  1.0 / 2.0 * dz / dx / dx + 2.0 / dz;
                col[1].i = ex + 1; col[1].j = ey; col[1].loc = DMSTAG_ELEMENT; col[1].c = 0; val[1] = -1.0 / 4.0 * dz / dx / dx;
                col[2].i = ex - 1; col[2].j = ey; col[2].loc = DMSTAG_ELEMENT; col[2].c = 0; val[2] = -1.0 / 4.0 * dz / dx / dx;
                DMStagMatSetValuesStencil(dm, dP1stBC, 1, &row, 3, col, val, INSERT_VALUES);
            }
        }
    }
    MatAssemblyBegin(U, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(U, MAT_FINAL_ASSEMBLY);
    MatAssemblyBegin(dP, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(dP, MAT_FINAL_ASSEMBLY);
    MatAssemblyBegin(dP1stBC, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(dP1stBC, MAT_FINAL_ASSEMBLY);

    KSPCreate(PETSC_COMM_WORLD, &ksp);
    KSPSetOperators(ksp, A, A);
    
    KSPSetType(ksp, KSPPREONLY);
    KSPGetPC(ksp, &prec);
    PCSetType(prec, PCLU);
    KSPSetFromOptions(ksp);
}

PetscErrorCode Projection::solve(Vec &sol, const Vec &pbc,
        const PetscScalar dt) const
{
    // set up source term
    Vec rhs;
    DMCreateGlobalVector(*ptrDM, &rhs);

    // divergence of intermediate velocity
    MatMult(dU, sol, rhs);
    VecScale(rhs, rhow / dt);
    // top boundary condition
    MatMultAdd(ddP1stBC, pbc, rhs, rhs);

    // migrate source term
    Vec pRhs, pSol;
    DMCreateGlobalVector(pDM, &pRhs);
    DMCreateGlobalVector(pDM, &pSol);
    DMStagMigrateVec(*ptrDM, rhs, pDM, pRhs);

    // solve
    KSPSolve(ksp, pRhs, pSol);

    // migrate back
    DMStagMigrateVec(pDM, pSol, *ptrDM, rhs);

    Vec tmp;    // store velocity only
    DMCreateGlobalVector(*ptrDM, &tmp);
    MatMult(U, sol, tmp);
    VecWAXPY(sol, 1.0, tmp, rhs);

    MatMult(dP, sol, tmp);
    MatMultAdd(dP1stBC, pbc, tmp, tmp);
    VecAXPY(sol, -dt / rhow, tmp);

    VecDestroy(&rhs );
    VecDestroy(&pRhs);
    VecDestroy(&pSol);
    VecDestroy(&tmp );

    return 0;
}

PetscErrorCode Projection::divergence(Vec &div, const Vec &sol) const
{
    MatMult(dU, sol, div);

    return 0;
}

PetscErrorCode Projection::destroy()
{
    KSPDestroy(&ksp);
    MatDestroy(&A );
    MatDestroy(&dU);
    MatDestroy(&dP);
    MatDestroy(&U );
    MatDestroy(&ddP1stBC);
    MatDestroy(&dP1stBC );
    DMDestroy(&pDM);
    ptrDM = NULL;

    return 0;
}