#include "PotentialWaves.h"
#include "Constants.h"

PotentialWaves::PotentialWaves(const PetscScalar period, const PetscScalar length,
        const PetscScalar depth, const PetscScalar height,
        const PetscScalar tol) : T(period), L(length), 
        h(depth), H(height),
        k(2.0 * PETSC_PI / L), omega(2.0 * PETSC_PI / T),
        c(L / T)
{
    // check dispersion relation
    if (PetscAbsScalar(omega * omega - g * k * tanh(k * h)) > tol)
    {
        PetscPrintf(PETSC_COMM_WORLD, "It seems that the dispersion relation is not satisfied!\nPlease check your inputs!\n");
        PetscEnd();
    }
}