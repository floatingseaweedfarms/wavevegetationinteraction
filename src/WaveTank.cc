#include "WaveTank.h"
#include "Constants.h"

WaveTank::WaveTank(const DM &dm,
        const Projection &proj,
        const WaveMaker  &maker,
        const PetscInt   &bc) :
        ptrProj(&proj), ptrMaker(&maker)
{
    if (bc != 0 && bc != 1)
    {
        PetscPrintf(PETSC_COMM_WORLD, "WRONG boundary condition for the x direction, either 0 or 1!\n");
        PetscEnd();
    }
    xbc = bc;

    DMCreateMatrix(dm, &topWMat);
    MatSetOption(topWMat, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);

    PetscInt startx, starty, nx, ny, ex, ey;
    DMStagStencil row, col;
    PetscScalar val;
    PetscBool isLastRankDim2;

    DMStagGetCorners(dm, &startx, &starty, NULL, &nx, &ny, NULL, NULL, NULL, NULL);
    DMStagGetIsLastRank(dm, NULL, &isLastRankDim2, NULL);

    for (ey = starty; ey != starty + ny; ++ey)
    {
        for (ex = startx; ex != startx + nx; ++ex)
        {
            if (isLastRankDim2 && ey == starty + ny - 1)
            {
                row.i = ex; row.j = ey; row.loc = DMSTAG_ELEMENT; row.c = 0;
                col.i = ex; col.j = ey; col.loc = DMSTAG_UP     ; col.c = 0;
                val = 1.0;
                DMStagMatSetValuesStencil(dm, topWMat, 1, &row, 1, &col, &val, INSERT_VALUES);
            }
        }
    }
    MatAssemblyBegin(topWMat, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(topWMat, MAT_FINAL_ASSEMBLY);

    DMCreateGlobalVector(dm, &topp);
    DMCreateGlobalVector(dm, &topw);
}

PetscErrorCode WaveTank::update(Vec &sol, Vec &eta,
        const PetscScalar t, const PetscScalar dt)
{
    if (xbc == 0) ptrMaker->force(sol, eta, t, dt);

    // update surface elevation
    MatMult(topWMat, sol, topw);
    VecAXPY(eta, dt, topw);

    // pressure at still water level
    VecZeroEntries(topp);
    VecAXPY(topp, rhow * g, eta);

    ptrProj->solve(sol, topp, dt);

    return 0;
}

PetscErrorCode WaveTank::update(Vec &sol, Vec &eta,
        const Vec &source,
        const PetscScalar t, const PetscScalar dt)
{
    if (xbc == 0) ptrMaker->force(sol, eta, t, dt);

    // update surface elevation
    MatMult(topWMat, sol, topw);
    VecAXPY(eta, dt, topw);

    VecAXPY(sol, 1.0, source);

    // pressure at still water level
    VecZeroEntries(topp);
    VecAXPY(topp, rhow * g, eta);

    ptrProj->solve(sol, topp, dt);

    return 0;
}

PetscErrorCode WaveTank::destroy()
{
    ptrProj = NULL;
    ptrMaker = NULL;
    VecDestroy(&topw);
    VecDestroy(&topp);
    MatDestroy(&topWMat);

    return 0;
}