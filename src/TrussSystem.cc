#include <petscdmnetwork.h>
#include "TrussSystem.h"
#include "Constants.h"

const Vector3s gg = Vector3s{0.0, 0.0, -g};
const Vector3s kk = Vector3s{0, -1, 0};

PetscErrorCode TrussSystem::update_RK2nd(const PetscScalar dt, const PetscInt step)
{
    if (step !=1 && step != 2)
    {
        PetscPrintf(PETSC_COMM_WORLD, "WRONG Step! either 1 or 2\n");
        PetscEnd();
    }

    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "The system has NOT been initialized!\n");
        PetscEnd();
    }

    Truss *truss;
    Node  *node, *node_fr, *node_to;
    PetscInt eStart, eEnd, vStart, vEnd;
    PetscInt nedges;
    const PetscInt *cone;
    const PetscInt *cone_next;
    const PetscInt *edges;
    PetscInt nv;
    const PetscInt *vtx;

    DMNetworkGetEdgeRange(*ptrDM, &eStart, &eEnd);
    DMNetworkGetVertexRange(*ptrDM, &vStart, &vEnd);

    for (PetscInt v = vStart; v != vEnd; ++v)
    {
        DMNetworkGetComponent(*ptrDM, v, 0, NULL, (void**)&node, NULL);
        node->F.setZero();
        node->Ut = 0.0;
        node->Fe.setZero();
    }

    // direction, strain, tension, and external force
    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(*ptrDM, e, 0, NULL, (void**)&truss, NULL);
        DMNetworkGetConnectedVertices(*ptrDM, e, &cone);

        DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
        DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);

        truss->dX = (node_to->x - node_fr->x);
        truss->l  = truss->dX.norm();
        truss->dl = (truss->l - truss->l0) / truss->l0;
        truss->T  = truss->EA * truss->dl;

        node_fr->F += 0.5 * truss->F;
        node_to->F += 0.5 * truss->F;

        node_fr->Fe += 0.5 * truss->F;
        node_to->Fe += 0.5 * truss->F;

        node_fr->Ut += 0.5 * truss->Ut;
        node_to->Ut += 0.5 * truss->Ut;
    }

    // shear on the node
    bending();
    // advection term
    advection();

    // load on the node
    for (PetscInt v = vStart; v != vEnd; ++v)
    {
        DMNetworkGetComponent(*ptrDM, v, 0, NULL, (void**)&node, NULL);
        // shear, pre-applied load, net buoyance, damping
        node->F  += (node->shear + node->Fp + node->NB + node->damping * node->u);
        node->Fe += (node->NB + node->Fp);

        DMNetworkGetSupportingEdges(*ptrDM, v, &nedges, &edges);
        for (PetscInt i = 0; i != nedges; ++i)
        {
            DMNetworkGetComponent(*ptrDM, edges[i], 0, NULL, (void**)&truss, NULL);
            DMNetworkGetConnectedVertices(*ptrDM, edges[i], &cone_next);
            if (cone_next[0] == v)
                node->F += truss->T * truss->dX / truss->l;
            else
                node->F -= truss->T * truss->dX / truss->l;
        }
    }

    // update
    for (PetscInt v = vStart; v != vEnd; ++v)
    {
        DMNetworkGetComponent(*ptrDM, v, 0, NULL, (void**)&node, NULL);
        DMNetworkGetSupportingEdges(*ptrDM, v, &nedges, &edges);

        Eigen::Matrix3d M = Eigen::Matrix3d::Zero();

        for (PetscInt e = 0; e != nedges; ++e)
        {
            DMNetworkGetComponent(*ptrDM, edges[e], 0, NULL, (void**)&truss, NULL);
            Vector3s tau = truss->dX / truss->l;
            Vector3s n = tau.cross(kk);
            M += 0.5 * (truss->mass * Eigen::Matrix3d::Identity() + truss->addedmass * n * n.transpose());
        }

        if (node->fixed) node->a.setZero();
        else node->a = M.llt().solve(node->F);

        if (step == 1)
        {
            node->a_0_2 = node->a;
            node->u_0_2 = node->u;
            node->x_0_2 = node->x;

            node->u += node->a * dt / 2;
            node->x += node->u * dt / 2;
        }

        if (step == 2)
        {
            node->u = node->u_0_2 + node->a * dt;
            node->x = node->x_0_2 + node->u_0_2 * dt + node->a_0_2 * dt * dt / 2;
        }
        node->Fe -= M * node->a;
    }

    // loads at the branch roots
    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        branches[b].load.setZero();
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], &nv, NULL, &vtx, NULL);
        for (PetscInt v = 0; v != nv; ++v)
        {
            DMNetworkGetComponent(*ptrDM, vtx[v], 0, NULL, (void**)&node, NULL);
            branches[b].load += node->Fe;
        }
    }

    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(*ptrDM, e, 0, NULL, (void**)&truss, NULL);
        DMNetworkGetConnectedVertices(*ptrDM, e, &cone);

        DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
        DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);

        truss->x = (node_fr->x + node_to->x) / 2.0;
        truss->u = (node_fr->u + node_to->u) / 2.0;
        truss->a = (node_fr->a + node_to->a) / 2.0;
    }

    return 0;
}

PetscScalar TrussSystem::maxstrain() const
{
    Truss *truss;
    PetscInt eStart, eEnd;
    PetscScalar strainmax = 0.0;

    DMNetworkGetEdgeRange(*ptrDM, &eStart, &eEnd);
    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(*ptrDM, e, 0, NULL, (void**)&truss, NULL);
        strainmax = PetscMax(PetscAbsScalar(truss->dl), strainmax);
    }

    if (isnan(strainmax))
    {
        PetscPrintf(PETSC_COMM_WORLD, "Non-physical strain!\n");
        PetscEnd();
    }
    else return strainmax;

    return 0;
}

PetscErrorCode TrussSystem::setupFromOptions(PetscOptions &options)
{
    setupTopologyFromOptions(options);
    setupPositionFromOptions(options);
    setupPropertyFromOptions(options);

    return 0;
}

PetscErrorCode TrussSystem::setupInFluidFromOptions(PetscOptions &options)
{
    PetscBool       flag;                   // flag to indicate whether reading from options fails
    PetscInt        ntoread;                // total number of values to be read
    PetscInt        nvalue;                 // number of values to be read for one object

    PetscScalar     *trussptsinfluid;       // truss properties

    // read truss properties
    nvalue = 6;
    ntoread = nvalue * Nedge;
    PetscCalloc1(ntoread, &trussptsinfluid);
    PetscOptionsGetScalarArray(options, NULL, "-trussptsinfluid", trussptsinfluid, &ntoread, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read trussptsinfluid!\n");
        PetscEnd();
    }
    if (ntoread != nvalue * Nedge)
    {
        PetscPrintf(PETSC_COMM_WORLD, "NOT enough trussptsinfluid inputs!\n");
        PetscEnd();
    }

    PetscInt ne;
    PetscInt count;
    const PetscInt *edges;
    Truss *truss;

    count = 0;
    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], NULL, &ne, NULL, &edges);
        for (PetscInt e = 0; e != ne; ++e)
        {
            PetscInt idbranch = trussptsinfluid[count * nvalue    ];
            PetscInt idtruss  = trussptsinfluid[count * nvalue + 1];

            if (b != idbranch || e != idtruss)
            {
                PetscPrintf(PETSC_COMM_WORLD, "INCOMPATABILITY between input data and the object to be set!\n");
                PetscEnd();
            }

            DMNetworkGetComponent(*ptrDM, edges[e], 0, NULL, (void**)&truss, NULL);
            PetscScalar displacedmass   = trussptsinfluid[count * nvalue + 2] * truss->l0;
            truss->addedmass            = trussptsinfluid[count * nvalue + 3] * truss->l0;
            truss->Cd                   = trussptsinfluid[count * nvalue + 4];
            truss->Cm                   = trussptsinfluid[count * nvalue + 5];

            truss->NB                   = (truss->mass - displacedmass) * gg;
            
            ++count;
        }
    }

    return 0;
}

PetscErrorCode TrussSystem::initialize()
{
    PetscInt eStart, eEnd, vStart, vEnd;
    DMNetworkGetEdgeRange(*ptrDM, &eStart, &eEnd);
    DMNetworkGetVertexRange(*ptrDM, &vStart, &vEnd);
    Truss *truss;
    Node *node, *node_to, *node_fr;
    const PetscInt *cone;

    for (PetscInt v = vStart; v != vEnd; ++v)
    {
        DMNetworkGetComponent(*ptrDM, v, 0, NULL, (void**)&node, NULL);
        node->id = v - vStart;
        node->NB.setZero();

        node->mass      = 0.0;
        node->addedmass = 0.0;

        node->a.setZero();
    }

    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(*ptrDM, e, 0, NULL, (void**)&truss, NULL);

        DMNetworkGetConnectedVertices(*ptrDM, e, &cone);
        DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
        DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);
        truss->x = 0.5 * (node_fr->x + node_to->x);
        truss->u = 0.5 * (node_fr->u + node_to->u);
        truss->a = 0.5 * (node_fr->a + node_to->a);
        truss->l = truss->l0;

        // set mass of starting node
        DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node, NULL);
        node->mass      += 0.5 * truss->mass;
        node->addedmass += 0.5 * truss->addedmass;
        node->NB        += 0.5 * truss->NB;

         // set mass of ending node
        DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node, NULL);
        node->mass      += 0.5 * truss->mass;
        node->addedmass += 0.5 * truss->addedmass;
        node->NB        += 0.5 * truss->NB;

        truss->dX   = (node_to->x - node_fr->x);
        truss->l    = truss->dX.norm();
    }

    for (PetscInt b = 0; b != Nsubnet; ++b)
        branches[b].load.setZero();

    flag = PETSC_TRUE;

    return 0;
}

PetscErrorCode TrussSystem::setupTopologyFromOptions(PetscOptions &options)
{
    PetscBool       flag;                   // flag to indicate whether reading from options fails
    PetscInt        ntoread;                // total number of values to be read
    PetscInt        nvalue;                 // number of values to be read for one object

    // inputs
    PetscInt        nsharedvtx;             // number of shared vertices
    PetscInt        *sharedvtx;             // list of shared vertices

    // other variables
    PetscInt       **edgelist;              // edgelist for all subnetworks
    PetscInt       asubnet, bsubnet;        // id of the subnetworks
    PetscInt       asvtx, bsvtx;            // id of the shared vertex

    // read Nsubnet
    PetscOptionsGetInt(options, NULL, "-Nsubnet", &Nsubnet, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read Nsubnet!\n");
        PetscEnd();
    }

    PetscCalloc1(Nsubnet, &nedge);
    PetscCalloc1(Nsubnet, &netnum);
    PetscCalloc1(Nsubnet, &edgelist);

    // read nedge
    ntoread = Nsubnet;
    PetscOptionsGetIntArray(options, NULL, "-nedge", nedge, &ntoread, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read nedge!\n");
        PetscEnd();
    }
    if (ntoread != Nsubnet)
    {
        PetscPrintf(PETSC_COMM_WORLD, "NOT enough nedge inputs!\n");
        PetscEnd();
    }

    Nedge = 0;
    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        Nedge += nedge[b];
        PetscCalloc1(2 * nedge[b], &edgelist[b]);
        for (PetscInt e = 0; e != nedge[b]; ++e)
        {
            edgelist[b][2 * e    ] = e    ;
            edgelist[b][2 * e + 1] = e + 1;
        }
    }

    // add subnetworks
    DMNetworkSetNumSubNetworks(*ptrDM, PETSC_DECIDE, Nsubnet);
    for (PetscInt b = 0; b != Nsubnet; ++b)
        DMNetworkAddSubnetwork(*ptrDM, "", nedge[b], edgelist[b], netnum + b);
    
    // read nsharedvtx, sharedvtx
    PetscOptionsGetInt(options, NULL, "-nsharedvtx", &nsharedvtx, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read nsharedvtx!\n");
        PetscEnd();
    }
    Nnode = Nedge + Nsubnet - nsharedvtx;

    if (nsharedvtx != 0)
    {
        nvalue = 4;
        ntoread = nvalue * nsharedvtx;
        PetscCalloc1(ntoread, &sharedvtx);
        PetscOptionsGetIntArray(options, NULL, "-sharedvtx", sharedvtx, &ntoread, &flag);
        if (!flag)
        {
            PetscPrintf(PETSC_COMM_WORLD, "FAILED to read sharedvtx!\n");
            PetscEnd();
        }
        if (ntoread != nvalue * nsharedvtx)
        {
            PetscPrintf(PETSC_COMM_WORLD, "NOT enough sharedvtx inputs!\n");
            PetscEnd();
        }

        // set shared vertices
        for (PetscInt sv = 0; sv != nsharedvtx; ++sv)
        {
            asubnet = sharedvtx[sv * nvalue    ];
            bsubnet = sharedvtx[sv * nvalue + 1];
            asvtx   = sharedvtx[sv * nvalue + 2];
            bsvtx   = sharedvtx[sv * nvalue + 3];
            DMNetworkAddSharedVertices(*ptrDM, netnum[asubnet], netnum[bsubnet], 1, &asvtx, &bsvtx);
        }
    }

    DMNetworkLayoutSetUp(*ptrDM);

    PetscFree(edgelist);
    if (nsharedvtx != 0) PetscFree(sharedvtx);

    return 0;
}

PetscErrorCode TrussSystem::setupPositionFromOptions(PetscOptions &options)
{
    PetscBool       flag;                   // flag to indicate whether reading from options fails
    PetscInt        ntoread;                // total number of values to be read
    PetscInt        nvalue;                 // number of values to be read for one object

    Node        *nodes;
    Truss       *trusses;
    PetscScalar *nodeinit;
    PetscCalloc1(Nnode, &nodes);
    PetscCalloc1(Nedge, &trusses);

    // register components
    PetscInt    key_node, key_truss;
    DMNetworkRegisterComponent(*ptrDM, "node", sizeof(Node), &key_node);
    DMNetworkRegisterComponent(*ptrDM, "truss", sizeof(Truss), &key_truss);

    // add components
    PetscInt eStart, eEnd, vStart, vEnd;
    DMNetworkGetEdgeRange(*ptrDM, &eStart, &eEnd);
    DMNetworkGetVertexRange(*ptrDM, &vStart, &vEnd);
    for (PetscInt e = eStart; e != eEnd; ++e)
        DMNetworkAddComponent(*ptrDM, e, key_truss, &trusses[e - eStart], 0);
    for (PetscInt v = vStart; v != vEnd; ++v)
        DMNetworkAddComponent(*ptrDM, v, key_node, &nodes[v - vStart], 0);
    DMSetUp(*ptrDM);
    DMNetworkDistribute(ptrDM, 0);
    DMNetworkAssembleGraphStructures(*ptrDM);

    // initialization
    for (PetscInt v = 0; v != Nnode; ++v)
    {
        nodes[v].x.setZero();
        nodes[v].set = PETSC_FALSE;
    }
    for (PetscInt e = 0; e != Nedge; ++e) trusses[e].l0 = 0;

    // read locations
    nvalue = 9;
    ntoread = nvalue * (Nedge + Nsubnet);
    PetscCalloc1(ntoread, &nodeinit);
    PetscOptionsGetScalarArray(options, NULL, "-nodeinit", nodeinit, &ntoread, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read nodeinit!\n");
        PetscEnd();
    }
    if (ntoread != nvalue * (Nedge + Nsubnet))
    {
        PetscPrintf(PETSC_COMM_WORLD, "NOT enough nodeinit inputs!\n");
        PetscEnd();
    }

    PetscInt nv;
    const PetscInt *vtx;
    Truss *truss;
    Node  *node, *node_fr, *node_to;
    const PetscInt *cone;

    PetscInt count = 0;
    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], &nv, NULL, &vtx, NULL);
        for (PetscInt v = 0; v != nv; ++v)
        {
            PetscInt idbranch = nodeinit[count * nvalue    ];
            PetscInt idnode   = nodeinit[count * nvalue + 1];
            
            if (b != idbranch || v != idnode)
            {
                PetscPrintf(PETSC_COMM_WORLD, "INCOMPATABILITY between input data and the object to be set!\n");
                PetscEnd();
            }
            
            DMNetworkGetComponent(*ptrDM, vtx[v], 0, NULL, (void**)&node, NULL);
            if (!node->set)
            {
                node->fixed = int(nodeinit[count * nvalue + 2]) == 0 ? PETSC_FALSE : PETSC_TRUE;
                node->x << nodeinit[count * nvalue + 3], nodeinit[count * nvalue + 4], nodeinit[count * nvalue + 5];
                node->u << nodeinit[count * nvalue + 6], nodeinit[count * nvalue + 7], nodeinit[count * nvalue + 8];
                node->set = PETSC_TRUE;
            }
            ++count;
        }
    }

    // make sure that all nodes are set
    for (PetscInt v = vStart; v != vEnd; ++v)
    {
        DMNetworkGetComponent(*ptrDM, v, 0, NULL, (void**)&node, NULL);
        if (!node->set)
        {
            PetscPrintf(PETSC_COMM_WORLD, "Some nodes have NOT been set!\n");
            PetscEnd();
        }
    }

    // set initial truss length
    for (PetscInt e = eStart; e != eEnd; ++e)
    {
        DMNetworkGetComponent(*ptrDM, e, 0, NULL, (void**)&truss, NULL);
        DMNetworkGetConnectedVertices(*ptrDM, e, &cone);

        DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
        DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);

        truss->l0 = (node_to->x - node_fr->x).norm();
    }

    PetscFree(nodes);
    PetscFree(trusses);
    PetscFree(nodeinit);

    return 0;
}

PetscErrorCode TrussSystem::setupPropertyFromOptions(PetscOptions &options)
{
    PetscBool       flag;                   // flag to indicate whether reading from options fails
    PetscInt        ntoread;                // total number of values to be read
    PetscInt        nvalue;                 // number of values to be read for one object

    PetscInt        *branchgps;             // branch global properties
    PetscScalar     *trusspts;              // truss properties

    // read branch global properties
    nvalue = 5;
    ntoread = nvalue * Nsubnet;
    PetscCalloc1(ntoread, &branchgps);
    PetscOptionsGetIntArray(options, NULL, "-branchgps", branchgps, &ntoread, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read branchgps!\n");
        PetscEnd();
    }
    if (ntoread != nvalue * Nsubnet)
    {
        PetscPrintf(PETSC_COMM_WORLD, "NOT enough branchgps inputs!\n");
        PetscEnd();
    }

    PetscCalloc1(Nsubnet, &branches);
    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        PetscInt id = branchgps[b * nvalue];
        if (id != b)
        {
            PetscPrintf(PETSC_COMM_WORLD, "INCOMPATABILITY between input data and the object to be set!\n");
            PetscEnd();
        }

        if (abs(branchgps[b * nvalue + 2]) != 1 || abs(branchgps[b * nvalue + 3]) != 1 || abs(branchgps[b * nvalue + 4]) != 1)
        {
            PetscPrintf(PETSC_COMM_WORLD, "WRONG input for the branch symmetry!\n");
            PetscEnd();
        }
        branches[id].stiff = branchgps[b * nvalue + 1] == 0 ? PETSC_FALSE : PETSC_TRUE;
        branches[id].sym << branchgps[b * nvalue + 2], branchgps[b * nvalue + 3], branchgps[b * nvalue + 4];
    }

    // read truss properties
    nvalue = 7;
    ntoread = nvalue * Nedge;
    PetscCalloc1(ntoread, &trusspts);
    PetscOptionsGetScalarArray(options, NULL, "-trusspts", trusspts, &ntoread, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read trusspts!\n");
        PetscEnd();
    }
    if (ntoread != nvalue * Nedge)
    {
        PetscPrintf(PETSC_COMM_WORLD, "NOT enough trusspts inputs!\n");
        PetscEnd();
    }

    PetscInt ne;
    PetscInt count;
    const PetscInt *edges;
    Truss *truss;
    Node  *node;

    count = 0;
    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], NULL, &ne, NULL, &edges);
        for (PetscInt e = 0; e != ne; ++e)
        {
            PetscInt idbranch = trusspts[count * nvalue    ];
            PetscInt idtruss  = trusspts[count * nvalue + 1];

            if (b != idbranch || e != idtruss)
            {
                PetscPrintf(PETSC_COMM_WORLD, "INCOMPATABILITY between input data and the object to be set!\n");
                PetscEnd();
            }

            DMNetworkGetComponent(*ptrDM, edges[e], 0, NULL, (void**)&truss, NULL);
            truss->id        = idtruss;
            truss->b         = trusspts[count * nvalue + 2];
            truss->d         = trusspts[count * nvalue + 3];
            truss->mass      = trusspts[count * nvalue + 4] * truss->l0;
            truss->EA        = trusspts[count * nvalue + 5];
            truss->EI        = trusspts[count * nvalue + 6];
            truss->NB        = truss->mass * gg;
            
            ++count;
        }
    }

    PetscFree(branchgps);
    PetscFree(trusspts);
    
    PetscInt eStart, eEnd, vStart, vEnd;
    DMNetworkGetEdgeRange(*ptrDM, &eStart, &eEnd);
    DMNetworkGetVertexRange(*ptrDM, &vStart, &vEnd);

    PetscScalar damping;
    // read Nsubnet
    PetscOptionsGetScalar(options, NULL, "-damping", &damping, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read damping!\n");
        PetscEnd();
    }

    for (PetscInt v = vStart; v != vEnd; ++v)
    {
        DMNetworkGetComponent(*ptrDM, v, 0, NULL, (void**)&node, NULL);
        node->damping = damping;
        node->Fp.setZero();
    }

    // set preapplied load on the node
    PetscScalar *preappliedloadonnode;

    nvalue = 5;
    ntoread = nvalue * (Nedge + Nsubnet);
    PetscCalloc1(ntoread, &preappliedloadonnode);
    PetscOptionsGetScalarArray(options, NULL, "-preappliedloadonnode", preappliedloadonnode, &ntoread, &flag);
    if (!flag)
    {
        PetscPrintf(PETSC_COMM_WORLD, "FAILED to read preappliedloadonnode!\n");
        PetscEnd();
    }
    if (ntoread != nvalue * (Nedge + Nsubnet))
    {
        PetscPrintf(PETSC_COMM_WORLD, "NOT enough preappliedloadonnode inputs!\n");
        PetscEnd();
    }

    PetscInt nv;
    const PetscInt *vtx;

    count = 0;
    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], &nv, NULL, &vtx, NULL);
        for (PetscInt v = 0; v != nv; ++v)
        {
            PetscInt idbranch = preappliedloadonnode[count * nvalue    ];
            PetscInt idnode   = preappliedloadonnode[count * nvalue + 1];
            
            if (b != idbranch || v != idnode)
            {
                PetscPrintf(PETSC_COMM_WORLD, "INCOMPATABILITY between input data and the object to be set!\n");
                PetscEnd();
            }
            
            DMNetworkGetComponent(*ptrDM, vtx[v], 0, NULL, (void**)&node, NULL);
            Vector3s fp(preappliedloadonnode[count * nvalue + 2], preappliedloadonnode[count * nvalue + 3], preappliedloadonnode[count * nvalue + 4]);
            node->Fp += fp;

            ++count;
        }
    }

    return 0;
}

PetscErrorCode TrussSystem::bending()
{
    PetscInt ne, nv;
    const PetscInt *vtx, *edges;
    Truss *truss;
    Node  *node, *node_fr, *node_to;
    PetscInt nsides;
    const PetscInt *cone;
    const PetscInt *side;
    Vector3s x1, x2;
    PetscScalar EI_1 = 0.0, EI_2 = 0.0;
    PetscScalar l0_1 = 0.0, l0_2 = 0.0;

    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], &nv, &ne, &vtx, &edges);
        for (PetscInt v = 0; v != nv; ++v)
        {
            DMNetworkGetComponent(*ptrDM, vtx[v], 0, NULL, (void**)&node, NULL);
            DMNetworkGetSupportingEdges(*ptrDM, vtx[v], &nsides, &side);

            // free tip
            if (nsides == 1 && !node->fixed) node->Kappa.setZero();

            if (nsides == 2)
            {
                DMNetworkGetComponent(*ptrDM, side[0], 0, NULL, (void**)&truss, NULL);
                x1 = truss->dX; l0_1 = truss->l;
                DMNetworkGetComponent(*ptrDM, side[1], 0, NULL, (void**)&truss, NULL);
                x2 = truss->dX; l0_2 = truss->l;
                node->Kappa = 2.0 / l0_1 / l0_2 * x1.cross(x2) / (x2 + x1).norm();
            }

            else if (nsides == 1 && node->fixed)
            {
                DMNetworkGetComponent(*ptrDM, side[0], 0, NULL, (void**)&truss, NULL);
                x2 = truss->dX; l0_2 = truss->l;
                x1 = (x2.array() * branches[b].sym.array()).matrix();
                l0_1 = l0_2;
                
                DMNetworkGetConnectedVertices(*ptrDM, side[0], &cone);
                DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
                DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);
                if (cone[0] == vtx[v]) node_fr->Kappa = node_to->Kappa;
                else PetscPrintf(PETSC_COMM_WORLD, "The two nodes are not the same!\n");
            }
        }
    }

    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        if (!branches[b].stiff) continue;
        // PetscPrintf(PETSC_COMM_WORLD, "Here!\n");
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], &nv, &ne, &vtx, &edges);
        for (PetscInt v = 0; v != nv; ++v)
        {
            DMNetworkGetComponent(*ptrDM, vtx[v], 0, NULL, (void**)&node, NULL);
            node->shear.setZero();

            DMNetworkGetSupportingEdges(*ptrDM, vtx[v], &nsides, &side);

            // free tip
            if (nsides == 1 && !node->fixed) continue;

            if (nsides == 2)
            {
                DMNetworkGetComponent(*ptrDM, side[0], 0, NULL, (void**)&truss, NULL);
                x1 = truss->dX; EI_1 = truss->EI; l0_1 = truss->l;
                DMNetworkGetComponent(*ptrDM, side[1], 0, NULL, (void**)&truss, NULL);
                x2 = truss->dX; EI_2 = truss->EI; l0_2 = truss->l;
            }

            else if (nsides == 1 && node->fixed)
            {
                DMNetworkGetComponent(*ptrDM, side[0], 0, NULL, (void**)&truss, NULL);
                x2 = truss->dX; EI_2 = truss->EI; l0_2 = truss->l;
                x1 = (x2.array() * branches[b].sym.array()).matrix();
                EI_1 = EI_2; l0_1 = l0_2;
            }

            node->M = (EI_1 + EI_2) / l0_1 / l0_2 * x1.cross(x2) / (x2 + x1).norm();
        }

        for (PetscInt e = 0; e != ne; ++e)
        {
            DMNetworkGetComponent(*ptrDM, edges[e], 0, NULL, (void**)&truss, NULL);
            DMNetworkGetConnectedVertices(*ptrDM, edges[e], &cone);

            DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
            DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);

            Vector3s couple = (node_to->M - node_fr->M).cross(truss->dX) / truss->l0 / truss->l;
            truss->Kappa = 0.5 * (node_to->Kappa + node_fr->Kappa);

            // from node
            node_fr->shear -= couple;
            // to node
            node_to->shear += couple;
        }
    }

    return 0;
}

PetscErrorCode TrussSystem::advection()
{
    PetscInt ne, nv;
    const PetscInt *vtx, *edges;
    Truss *truss;
    Node  *node, *node_fr, *node_to;
    PetscInt nsides;
    const PetscInt *cone;
    const PetscInt *sides;

    for (PetscInt b = 0; b != Nsubnet; ++b)
    {
        DMNetworkGetSubnetwork(*ptrDM, netnum[b], &nv, &ne, &vtx, &edges);
        for (PetscInt v = 0; v != nv; ++v)
        {
            DMNetworkGetComponent(*ptrDM, vtx[v], 0, NULL, (void**)&node, NULL);
            DMNetworkGetSupportingEdges(*ptrDM, vtx[v], &nsides, &sides);

            if (nsides == 1 && node->fixed) continue;
            if (nsides == 1 && !node->fixed) // free tip
            {
                DMNetworkGetComponent(*ptrDM, sides[0], 0, NULL, (void**)&truss, NULL);
                DMNetworkGetConnectedVertices(*ptrDM, sides[0], &cone);
                if (cone[1] == vtx[v])
                {
                    DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
                    DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);
                    node->F  += (2 * node->Ut * node->addedmass * (node_to->u - node_fr->u) / truss->l0);
                    node->Fe += (2 * node->Ut * node->addedmass * (node_to->u - node_fr->u) / truss->l0);
                }
            }
            if (nsides == 2)
            {
                if (node->Ut > 0)
                {
                    for (PetscInt i = 0; i != nsides; ++i)
                    {
                        DMNetworkGetComponent(*ptrDM, sides[i], 0, NULL, (void**)&truss, NULL);
                        DMNetworkGetConnectedVertices(*ptrDM, sides[i], &cone);
                        if (cone[0] == vtx[v])
                        {
                            DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
                            DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);
                            node->F  += (2 * node->Ut * node->addedmass * (node_to->u - node_fr->u) / truss->l0);
                            node->Fe += (2 * node->Ut * node->addedmass * (node_to->u - node_fr->u) / truss->l0);
                        }
                    }
                }
                else
                {
                    for (PetscInt i = 0; i != nsides; ++i)
                    {
                        DMNetworkGetComponent(*ptrDM, sides[i], 0, NULL, (void**)&truss, NULL);
                        DMNetworkGetConnectedVertices(*ptrDM, sides[i], &cone);
                        if (cone[1] == vtx[v])
                        {
                            DMNetworkGetComponent(*ptrDM, cone[0], 0, NULL, (void**)&node_fr, NULL);
                            DMNetworkGetComponent(*ptrDM, cone[1], 0, NULL, (void**)&node_to, NULL);
                            node->F  += (2 * node->Ut * node->addedmass * (node_to->u - node_fr->u) / truss->l0);
                            node->Fe += (2 * node->Ut * node->addedmass * (node_to->u - node_fr->u) / truss->l0);
                        }
                    }
                }
            }
        }
    }

    return 0;
}

PetscErrorCode TrussSystem::destroy()
{
    ptrDM = NULL;
    PetscFree(netnum);
    PetscFree(nedge);
    PetscFree(branches);

    return 0;
}
