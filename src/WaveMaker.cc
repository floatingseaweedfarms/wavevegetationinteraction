#include "WaveMaker.h"

WaveMaker::WaveMaker(const DM &dm, const PotentialWaves &wave,
        const PetscScalar xMin, const PetscScalar xMax,
        const PetscScalar Gamma,
        const PetscScalar n,
        const PetscScalar nRamp,
        const PetscScalar nIn,
        const PetscScalar nOut) : ptrDM(&dm), ptrWave(&wave),
        xmin(xMin), xmax(xMax), gamma(Gamma), 
        nramp(nRamp), nin(nIn), nout(nOut)
{
    DMCreateGlobalVector(*ptrDM, &blender);
    Vec blenderLocal;
    DMGetLocalVector(*ptrDM, &blenderLocal);
    VecZeroEntries(blenderLocal);

    PetscInt iu, iv, ip;
    PetscInt iprev, icenter;
    PetscScalar ***arrBlender, **arrX, **arrY;
    PetscInt N[2];
    PetscInt startx, starty, nx, ny, nExtra[2], ex, ey;
    PetscBool isLastRankDim2;

    DMStagGetLocationSlot(*ptrDM, DMSTAG_LEFT   , 0, &iu);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_DOWN   , 0, &iv);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_ELEMENT, 0, &ip);

    DMStagGetProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_LEFT   , &iprev  );
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_ELEMENT, &icenter);

    DMStagVecGetArray(*ptrDM, blenderLocal, &arrBlender);

    DMStagGetGlobalSizes(*ptrDM, &N[0], &N[1], NULL);
    DMStagGetCorners(*ptrDM, &startx, &starty, NULL, &nx, &ny, NULL, &nExtra[0], &nExtra[1], NULL);
    DMStagGetIsLastRank(*ptrDM, NULL, &isLastRankDim2, NULL);

    const PetscScalar L = ptrWave->length();

    for (ey = starty; ey != starty + ny + nExtra[1]; ++ey)
    {
        for (ex = startx; ex != startx + nx + nExtra[0]; ++ex)
        {
            arrBlender[ey][ex][iu] = blend(arrX[ex][iprev  ], xmin, xmax, n, nIn, nOut, L);
            arrBlender[ey][ex][iv] = blend(arrX[ex][icenter], xmin, xmax, n, nIn, nOut, L);

            // the top row
            if (isLastRankDim2 && ey == starty + ny - 1)
            {
                arrBlender[ey][ex][ip] = blend(arrX[ex][icenter], xmin, xmax, n, nIn, nOut, L);
            }
        }
    }
    DMStagRestoreProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);
    DMStagVecRestoreArray(*ptrDM, blenderLocal, &arrBlender);
    DMLocalToGlobal(*ptrDM, blenderLocal, INSERT_VALUES, blender);
    DMRestoreLocalVector(*ptrDM, &blenderLocal);
}

PetscErrorCode WaveMaker::force(Vec &sol, Vec &eta,
        const PetscScalar t,
        const PetscScalar dt) const
{
    Vec sourVelo, sourEta;
    Vec sourVeloLocal, sourEtaLocal, solLocal, etaLocal, blenderLocal;

    DMCreateGlobalVector(*ptrDM, &sourVelo);
    DMCreateGlobalVector(*ptrDM, &sourEta);

    DMGetLocalVector(*ptrDM, &sourVeloLocal);
    VecZeroEntries(sourVeloLocal);

    DMGetLocalVector(*ptrDM, &sourEtaLocal);
    VecZeroEntries(sourEtaLocal);

    DMGetLocalVector(*ptrDM, &blenderLocal);
    VecZeroEntries(blenderLocal);
    DMGlobalToLocal(*ptrDM, blender, INSERT_VALUES, blenderLocal);

    DMGetLocalVector(*ptrDM, &solLocal);
    VecZeroEntries(solLocal);
    DMGlobalToLocal(*ptrDM, sol, INSERT_VALUES, solLocal);

    DMGetLocalVector(*ptrDM, &etaLocal);
    VecZeroEntries(etaLocal);
    DMGlobalToLocal(*ptrDM, eta, INSERT_VALUES, etaLocal);

    PetscInt iu, iv, ip;
    PetscInt iprev, icenter;
    PetscScalar ***arrBlender, ***arrSol, ***arrEta, ***arrSourVelo, ***arrSourEta, **arrX, **arrY;
    PetscInt N[2];
    PetscInt startx, starty, nx, ny, nExtra[2], ex, ey;

    DMStagGetLocationSlot(*ptrDM, DMSTAG_LEFT   , 0, &iu);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_DOWN   , 0, &iv);
    DMStagGetLocationSlot(*ptrDM, DMSTAG_ELEMENT, 0, &ip);

    DMStagGetProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_LEFT   , &iprev  );
    DMStagGetProductCoordinateLocationSlot(*ptrDM, DMSTAG_ELEMENT, &icenter);

    DMStagVecGetArrayRead(*ptrDM, blenderLocal, &arrBlender);
    DMStagVecGetArrayRead(*ptrDM, solLocal    , &arrSol    );
    DMStagVecGetArrayRead(*ptrDM, etaLocal    , &arrEta    );
    DMStagVecGetArray    (*ptrDM, sourVeloLocal, &arrSourVelo);
    DMStagVecGetArray    (*ptrDM, sourEtaLocal, &arrSourEta);

    DMStagGetGlobalSizes(*ptrDM, &N[0], &N[1], NULL);
    DMStagGetCorners(*ptrDM, &startx, &starty, NULL, &nx, &ny, NULL, &nExtra[0], &nExtra[1], NULL);
    
    const PetscScalar T = ptrWave->period();
    const PetscScalar L = ptrWave->length();
    PetscScalar tRamp;

    for (ey = starty; ey != starty + ny + nExtra[1]; ++ey)
    {
        for (ex = startx; ex != startx + nx + nExtra[0]; ++ex)
        {
            tRamp = fmin(1.0 / (nramp * T) * t, 1.0);
            
            if (arrX[ex][iprev  ] <= xmin + nin * L)
            {
                arrSol[ey][ex][iu] *= (1.0 - arrBlender[ey][ex][iu]);
                arrSol[ey][ex][iu] += ptrWave->u(arrX[ex][iprev  ], arrY[ey][icenter], t) * tRamp * arrBlender[ey][ex][iu];
            }
            if (arrX[ex][icenter] <= xmin + nin * L)
            {
                arrSol[ey][ex][iv] *= (1.0 - arrBlender[ey][ex][iv]);
                arrSol[ey][ex][iv] += ptrWave->w(arrX[ex][icenter], arrY[ey][iprev  ], t) * tRamp * arrBlender[ey][ex][iv];
            }
            if (ey == N[1] - 1)
            {
                if (arrX[ex][icenter] <= xmin + nin * L)
                {
                    arrEta[ey][ex][ip] *= (1.0 - arrBlender[ey][ex][ip]);
                    arrEta[ey][ex][ip] += ptrWave->elevation(arrX[ex][icenter], t + dt) * tRamp * arrBlender[ey][ex][ip];
                }
            }
            
            if (arrX[ex][iprev  ] >= xmax - nout * L)
                arrSourVelo[ey][ex][iu] = gamma * arrBlender[ey][ex][iu] * (0.0 - arrSol[ey][ex][iu]);
            
            if (arrX[ex][icenter] >= xmax - nout * L)
                arrSourVelo[ey][ex][iv] = gamma * arrBlender[ey][ex][iv] * (0.0 - arrSol[ey][ex][iv]);
            
            if (ey == N[1] - 1)
            {
                if (arrX[ex][icenter] >= xmax - nout * L)
                    arrSourEta[ey][ex][ip] = gamma * arrBlender[ey][ex][ip] * (0.0 - arrEta[ey][ex][ip]);
            }
        }
    }
    DMStagRestoreProductCoordinateArraysRead(*ptrDM, &arrX, &arrY, NULL);

    DMStagVecRestoreArrayRead(*ptrDM, blenderLocal, &arrBlender);
    DMRestoreLocalVector(*ptrDM, &blenderLocal);

    DMStagVecRestoreArrayRead(*ptrDM, solLocal, &arrSol);
    DMLocalToGlobal(*ptrDM, solLocal, INSERT_VALUES, sol);
    DMRestoreLocalVector(*ptrDM, &solLocal);

    DMStagVecRestoreArrayRead(*ptrDM, etaLocal, &arrEta);
    DMLocalToGlobal(*ptrDM, etaLocal, INSERT_VALUES, eta);
    DMRestoreLocalVector(*ptrDM, &etaLocal);

    DMStagVecRestoreArray(*ptrDM, sourEtaLocal, &arrSourEta);
    DMLocalToGlobal(*ptrDM, sourEtaLocal, INSERT_VALUES, sourEta);
    DMRestoreLocalVector(*ptrDM, &sourEtaLocal);

    DMStagVecRestoreArray(*ptrDM, sourVeloLocal, &arrSourVelo);
    DMLocalToGlobal(*ptrDM, sourVeloLocal, INSERT_VALUES, sourVelo);
    DMRestoreLocalVector(*ptrDM, &sourVeloLocal);

    VecAXPY(eta, dt, sourEta);
    VecAXPY(sol, dt, sourVelo);

    VecDestroy(&sourEta);
    VecDestroy(&sourVelo);

    return 0;
}

PetscErrorCode WaveMaker::destroy()
{
    ptrDM   = NULL;
    ptrWave = NULL;
    VecDestroy(&blender);

    return 0;
}

PetscScalar WaveMaker::blend(const PetscScalar x,
        const PetscScalar xmin, const PetscScalar xmax,
        const PetscScalar n,
        const PetscScalar nin,
        const PetscScalar nout,
        const PetscScalar L) const
{
    const PetscScalar chi = fmax(-1.0 / (nin * L) * x + xmin / (nin * L), 1.0 / (nout * L) * x - xmax / (nout * L)) + 1.0;
    return (exp(pow(fmax(chi, 0.0), n)) - 1.0) / (exp(1.0) - 1.0);
}