#include "AiryWave.h"
#include "Constants.h"

PetscScalar AiryWave::u(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return omega * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x);
}

PetscScalar AiryWave::w(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -omega * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x);
}

PetscScalar AiryWave::ax(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -omega * omega * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x);
}

PetscScalar AiryWave::az(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -omega * omega * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x);
}

PetscScalar AiryWave::dudx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return k * omega * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x);
}

PetscScalar AiryWave::dudz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return k * omega * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x);
}

PetscScalar AiryWave::dwdx(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return k * omega * H / 2.0 * PetscSinhScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscCosScalar(omega * t - k * x);
}

PetscScalar AiryWave::dwdz(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return -k * omega * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscSinhScalar(k * h) * PetscSinScalar(omega * t - k * x);
}

PetscScalar AiryWave::p(const PetscScalar x, const PetscScalar z, const PetscScalar t) const
{
    return rhow * g * H / 2.0 * PetscCoshScalar(k * (z + h)) / PetscCoshScalar(k * h) * PetscCosScalar(omega * t - k * x);
}

PetscScalar AiryWave::elevation(const PetscScalar x, const PetscScalar t) const
{
    return H / 2.0 * PetscCosScalar(omega * t - k * x);
}